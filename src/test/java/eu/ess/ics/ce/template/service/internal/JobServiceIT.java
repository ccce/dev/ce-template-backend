/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal;

import static eu.ess.ics.ce.template.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import eu.ess.ics.ce.template.CeTemplatingBackendApplication;
import eu.ess.ics.ce.template.EntityFactory;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.rest.model.git.response.ReferenceType;
import eu.ess.ics.ce.template.rest.model.jobs.response.Job;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobDetails;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.rest.model.jobs.response.PagedJobResponse;
import eu.ess.ics.ce.template.rest.model.types.response.GitProjectInfo;
import eu.ess.ics.ce.template.service.internal.jobs.JobsService;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {JobsService.class, CeTemplatingBackendApplication.class})
@Tag("IntegrationTest")
@DirtiesContext
public class JobServiceIT {

  @Autowired private JobsService jobsService;

  @Autowired private IOperationRepository operationRepository;

  @Autowired private IIocInstanceRepository iocInstanceRepository;

  @Autowired private ITemplateRepository templateRepository;

  @Autowired private IInstanceLogRepository logRepository;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.hikari.username}")
  private String dataSourceUsername;

  @Value("${spring.datasource.hikari.password}")
  private String dataSourcePassword;

  @AfterEach
  void clean() throws SQLException {
    Connection conn =
        DriverManager.getConnection(dataSourceUrl, dataSourceUsername, dataSourcePassword);
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity false");
    st.executeUpdate("delete from ioc_instance");
    st.executeUpdate("alter table ioc_instance alter column id restart with 1");
    st.executeUpdate("delete from ioc_type");
    st.executeUpdate("alter table ioc_type alter column id restart with 1");
    st.executeUpdate("delete from operation");
    st.executeUpdate("alter table operation alter column id restart with 1");
    st.executeUpdate("set referential_integrity true");
    conn.close();
  }

  @Test
  public void findOperationSuccess() {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);

    final Job expected =
        new Job(
            ID_1,
            IOC_TYPE_NAME_1,
            USER_NAME,
            TAG_NAME,
            DATE_TIME,
            DATE_TIME,
            1L,
            IOC_ID,
            GenerationStatus.SUCCESSFUL,
            JobType.GENERATE_INSTANCE);
    PagedJobResponse actual =
        jobsService.findOperations(IOC_TYPE_NAME_1, ID_1, PAGE, LIMIT, USER_NAME);

    assertEquals(1, actual.getListSize());
    assertEquals(1L, actual.getTotalCount());
    assertEquals(LIMIT, actual.getLimit());
    assertEquals(PAGE, actual.getPageNumber());
    AssertionsForClassTypes.assertThat(actual.getJobs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(List.of(expected));
  }

  @Test
  public void getOperationByIdSuccess() {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);

    final JobDetails expected =
        new JobDetails(
            ID_1,
            JobType.GENERATE_INSTANCE,
            IOC_TYPE_NAME_1,
            ID_1,
            null,
            USER_NAME,
            TAG_NAME,
            new GitProjectInfo(null, null, ReferenceType.UNKNOWN),
            new GitProjectInfo(null, null, ReferenceType.UNKNOWN),
            1L,
            DATE_TIME,
            DATE_TIME);
    JobDetails actual = jobsService.getOperationById(ID_1);

    AssertionsForClassTypes.assertThat(actual)
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected);
  }
}
