/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template;

import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobInstance;
import java.time.ZonedDateTime;
import java.util.List;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Tag;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public final class TestConstants {

  public static final int PAGE = 0;
  public static final int LIMIT = 1;
  public static final Long ID_1 = 1L;
  public static final Long ID_2 = 2L;
  public static final Long ID_3 = 3L;
  public static final Long IOC_ID = 1L;
  public static final String IOC_TYPE_NAME_1 = "test_ioctype1";
  public static final String IOC_TYPE_NAME_2 = "test_ioctype2";
  public static final String TAG_NAME = "Version-1";
  public static final ZonedDateTime DATE_TIME = ZonedDateTime.now();
  public static final String USER_NAME = "testuser";
  public static final long CONFIG_GIT_PROJECT_ID = 9011L;
  public static final long CONFIG_GIT_PROJECT_ID_CHANGED = 11011L;
  public static final long TEMPLATE_GIT_PROJECT_ID_CHANGED = 11011L;
  public static final long TEMPLATE_GIT_PROJECT_ID = 9010L;
  public static final long GIT_PROJECT_ID = 1000L;
  public static final long VERSION = 555L;
  public static final String CONFIGURATION_FILE_NAME = "test-instance1.json";
  public static final String CONFIG_NAME = "test-instance1";
  public static final String INSTANCE_NAME = "e3-ioc-test_type1-test-instance1";
  public static final String CONFIG_SUFFIX = "-config";
  public static final String MAIN_BRANCH = "main";
  public static final IocTypeEntity IOC_TYPE_ENTITY = new IocTypeEntity();
  public static final IocTypeEntity IOC_TYPE_ENTITY_NEW = new IocTypeEntity();
  public static final OperationEntity OPERATION_ENTITY = new OperationEntity();
  public static final IocInstanceEntity IOC_INSTANCE_ENTITY = new IocInstanceEntity();
  public static final Tag TAG = new Tag();
  public static final Project TEMPLATE_PROJECT = new Project();
  public static final Project TEMPLATE_PROJECT_CHANGED = new Project();
  public static final Project CONFIG_PROJECT = new Project();
  public static final Project CONFIG_PROJECT_CHANGED = new Project();
  public static final JobInstance OPERATION_INSTANCE = new JobInstance(ID_1, INSTANCE_NAME, null);

  public static final String INVALID_TYPE_NAME_ERROR_MESSAGE =
      "Invalid request: Only lowercase alphanumeric chars and underscores are allowed in template name (max 20 chars)";

  public static final String INVALID_INSTANCE_NAME_ERROR_MESSAGE_TEMPLATE =
      "Invalid request: '%s': Only lowercase alphanumeric chars and hyphens are allowed in instance name (max 20 chars)";

  public static final Commit COMMIT = new Commit();

  static {
    IOC_TYPE_ENTITY_NEW.setName(IOC_TYPE_NAME_1);
    IOC_TYPE_ENTITY_NEW.setCreatedBy(USER_NAME);
    IOC_TYPE_ENTITY_NEW.setConfigGitProjectId(CONFIG_GIT_PROJECT_ID);
    IOC_TYPE_ENTITY_NEW.setTemplateGitProjectId(TEMPLATE_GIT_PROJECT_ID);

    IOC_TYPE_ENTITY.setId(ID_1);
    IOC_TYPE_ENTITY.setMaxVersion(VERSION);
    IOC_TYPE_ENTITY.setName(IOC_TYPE_NAME_1);
    IOC_TYPE_ENTITY.setCreatedBy(USER_NAME);
    IOC_TYPE_ENTITY.setConfigGitProjectId(CONFIG_GIT_PROJECT_ID);
    IOC_TYPE_ENTITY.setCreatedAt(DATE_TIME);
    IOC_TYPE_ENTITY.setTemplateGitProjectId(TEMPLATE_GIT_PROJECT_ID);

    OPERATION_ENTITY.setId(ID_1);
    OPERATION_ENTITY.setIocType(IOC_TYPE_ENTITY);
    OPERATION_ENTITY.setGitTag(TAG_NAME);
    OPERATION_ENTITY.setCreatedAt(DATE_TIME);
    OPERATION_ENTITY.setCreatedBy(USER_NAME);
    OPERATION_ENTITY.setFinishedAt(DATE_TIME);
    OPERATION_ENTITY.setTypeName(IOC_TYPE_ENTITY.getName());
    OPERATION_ENTITY.setConfigurationRevision(MAIN_BRANCH);
    OPERATION_ENTITY.setTemplateRevision(MAIN_BRANCH);

    IOC_INSTANCE_ENTITY.setId(ID_1);
    // IOC_INSTANCE_ENTITY.setOperation(OPERATION_ENTITY);
    IOC_INSTANCE_ENTITY.setIocTypeName(IOC_TYPE_NAME_1);
    IOC_INSTANCE_ENTITY.setCreatedAt(DATE_TIME);
    IOC_INSTANCE_ENTITY.setCreatedBy(USER_NAME);
    IOC_INSTANCE_ENTITY.setGitRevision(MAIN_BRANCH);
    // IOC_INSTANCE_ENTITY.setTemplateGitProjectId(TEMPLATE_GIT_PROJECT_ID);
    IOC_INSTANCE_ENTITY.setGitProjectId(GIT_PROJECT_ID);
    // IOC_INSTANCE_ENTITY.setConfigurationGitProjectId(CONFIG_GIT_PROJECT_ID);
    IOC_INSTANCE_ENTITY.setVersion(VERSION);
    IOC_INSTANCE_ENTITY.setGitTag(TAG_NAME);
    IOC_INSTANCE_ENTITY.setConfigurationName(CONFIGURATION_FILE_NAME);

    TEMPLATE_PROJECT.setId(TEMPLATE_GIT_PROJECT_ID);
    TEMPLATE_PROJECT.setTagList(List.of(TAG_NAME));
    TEMPLATE_PROJECT.setName(IOC_TYPE_NAME_1);

    TEMPLATE_PROJECT_CHANGED.setId(TEMPLATE_GIT_PROJECT_ID_CHANGED);
    TEMPLATE_PROJECT_CHANGED.setTagList(List.of(TAG_NAME));
    TEMPLATE_PROJECT_CHANGED.setName(IOC_TYPE_NAME_1);

    CONFIG_PROJECT.setId(CONFIG_GIT_PROJECT_ID);
    CONFIG_PROJECT.setTagList(List.of(TAG_NAME));
    CONFIG_PROJECT.setName(IOC_TYPE_NAME_1);
    CONFIG_PROJECT_CHANGED.setDefaultBranch(MAIN_BRANCH);

    CONFIG_PROJECT_CHANGED.setId(CONFIG_GIT_PROJECT_ID_CHANGED);
    CONFIG_PROJECT_CHANGED.setTagList(List.of(TAG_NAME));
    CONFIG_PROJECT_CHANGED.setName(IOC_TYPE_NAME_1);
    CONFIG_PROJECT_CHANGED.setDefaultBranch(MAIN_BRANCH);

    COMMIT.setId(MAIN_BRANCH);
    TAG.setCommit(COMMIT);
  }

  private TestConstants() {}
}
