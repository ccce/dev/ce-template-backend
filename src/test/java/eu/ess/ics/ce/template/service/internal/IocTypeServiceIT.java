/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal;

import static eu.ess.ics.ce.template.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ess.ics.ce.template.CeTemplatingBackendApplication;
import eu.ess.ics.ce.template.EntityFactory;
import eu.ess.ics.ce.template.EntityFactory.InstanceWithOperation;
import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.InputValidationException;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.PagedJobResponse;
import eu.ess.ics.ce.template.rest.model.template.request.CreateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.response.CreateTypeResponse;
import eu.ess.ics.ce.template.rest.model.template.response.PagedTypeResponse;
import eu.ess.ics.ce.template.rest.model.template.response.Type;
import eu.ess.ics.ce.template.rest.model.template.response.TypeBase;
import eu.ess.ics.ce.template.rest.model.types.request.InstancesToAttach;
import eu.ess.ics.ce.template.rest.model.types.response.PagedIocResponse;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.internal.iocinstance.IocInstanceService;
import eu.ess.ics.ce.template.service.internal.jobs.JobsService;
import eu.ess.ics.ce.template.service.internal.template.IocTypeService;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.AssertionsForClassTypes;
import org.gitlab4j.api.GitLabApiException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.util.ResourceUtils;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {IocTypeService.class, CeTemplatingBackendApplication.class})
@Tag("IntegrationTest")
@DirtiesContext
public class IocTypeServiceIT {

  @MockBean private GitLabService gitLabServiceMock;

  @Autowired private IocTypeService iocTypeService;

  @Autowired private IOperationRepository operationRepository;

  @Autowired private IIocInstanceRepository iocInstanceRepository;

  @Autowired private ITemplateRepository templateRepository;

  @Autowired private IInstanceLogRepository logRepository;
  @Autowired private JobsService jobsService;
  @Autowired private IocInstanceService iocInstanceService;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.hikari.username}")
  private String dataSourceUsername;

  @Value("${spring.datasource.hikari.password}")
  private String dataSourcePassword;

  @AfterEach
  void clean() throws SQLException {
    Connection conn =
        DriverManager.getConnection(dataSourceUrl, dataSourceUsername, dataSourcePassword);
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity false");
    st.executeUpdate("delete from ioc_instance");
    st.executeUpdate("alter table ioc_instance alter column id restart with 1");
    st.executeUpdate("delete from ioc_type");
    st.executeUpdate("alter table ioc_type alter column id restart with 1");
    st.executeUpdate("delete from operation");
    st.executeUpdate("alter table operation alter column id restart with 1");
    st.executeUpdate("set referential_integrity true");
    conn.close();
  }

  @Test
  public void findAllSuccess() {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);
    PagedTypeResponse actual =
        iocTypeService.findAll(USER_NAME, IOC_TYPE_NAME_1, false, PAGE, LIMIT, false);

    assertEquals(1, actual.getListSize());
    assertEquals(1L, actual.getTotalCount());
    assertEquals(PAGE, actual.getPageNumber());
    assertEquals(LIMIT, actual.getLimit());

    List<TypeBase> expectedTypes =
        List.of(
            new TypeBase(
                ID_1,
                IOC_TYPE_NAME_1,
                null,
                TEMPLATE_GIT_PROJECT_ID,
                CONFIG_GIT_PROJECT_ID,
                USER_NAME,
                DATE_TIME,
                1L,
                TAG_NAME,
                1L,
                false));
    AssertionsForClassTypes.assertThat(actual.getTypes())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .isEqualTo(expectedTypes);
  }

  @Test
  public void createIocTypeFailOnTypeNameValidation()
      throws JsonProcessingException, GitLabApiException {
    try {
      final CreateTypeRequest createRequest =
          new ObjectMapper()
              .readValue(
                  getFileContent("classpath:requests/create_type_request_invalid_type_name.json"),
                  CreateTypeRequest.class);

      iocTypeService.createTemplateAndConfiguration(createRequest, null, USER_NAME);
    } catch (InputValidationException e) {
      assertEquals(INVALID_TYPE_NAME_ERROR_MESSAGE, e.getMessage());
    }
  }

  @Test
  public void createIocTypeFailOnInstanceNameValidation()
      throws JsonProcessingException, GitLabApiException {
    try {
      final CreateTypeRequest createRequest =
          new ObjectMapper()
              .readValue(
                  getFileContent(
                      "classpath:requests/create_type_request_invalid_instance_name.json"),
                  CreateTypeRequest.class);

      iocTypeService.createTemplateAndConfiguration(createRequest, null, USER_NAME);
    } catch (InputValidationException e) {
      assertEquals(
          INVALID_INSTANCE_NAME_ERROR_MESSAGE_TEMPLATE.formatted("test_instance1"), e.getMessage());
    }
  }

  @Test
  public void createUpdateAndDeleteIocType() throws GitLabApiException, JsonProcessingException {
    // create
    final CreateTypeRequest createRequest =
        new ObjectMapper()
            .readValue(
                getFileContent("classpath:requests/create_type_request.json"),
                CreateTypeRequest.class);

    Mockito.when(gitLabServiceMock.createTemplateProject(createRequest.getTemplateName()))
        .thenReturn(TEMPLATE_PROJECT);
    Mockito.when(
            gitLabServiceMock.createConfigProject(
                IOC_TYPE_NAME_1 + CONFIG_SUFFIX, createRequest.getIocs()))
        .thenReturn(CONFIG_PROJECT);

    final CreateTypeResponse actualCreatedResponse =
        iocTypeService.createTemplateAndConfiguration(createRequest, null, USER_NAME);
    final CreateTypeResponse expectedCreatedResponse =
        new CreateTypeResponse(
            IOC_TYPE_NAME_1, ID_1, TEMPLATE_GIT_PROJECT_ID, CONFIG_GIT_PROJECT_ID);
    assertEquals(expectedCreatedResponse, actualCreatedResponse);

    // update

    OAuth2User user = EntityFactory.fetchOauthUser();
    Mockito.when(gitLabServiceMock.checkConfigExists(CONFIG_GIT_PROJECT_ID_CHANGED))
        .thenReturn(CONFIG_PROJECT_CHANGED);
    Mockito.when(gitLabServiceMock.checkTemplateExists(TEMPLATE_GIT_PROJECT_ID_CHANGED))
        .thenReturn(TEMPLATE_PROJECT_CHANGED);

    iocTypeService.updateTemplate(user, ID_1, IOC_TYPE_NAME_2, null);
    final Type actualAfterUpdate = iocTypeService.getIocTypeById(ID_1);

    final Type expectedAfterUpdate =
        new Type(
            ID_1,
            IOC_TYPE_NAME_2,
            null,
            TEMPLATE_GIT_PROJECT_ID,
            null,
            CONFIG_GIT_PROJECT_ID,
            null,
            USER_NAME,
            DATE_TIME,
            0L,
            null,
            null,
            false);
    AssertionsForClassTypes.assertThat(actualAfterUpdate)
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .isEqualTo(expectedAfterUpdate);

    // delete
    iocTypeService.deleteIocType(user, ID_1);
    assertThrows(EntityNotFoundException.class, () -> iocTypeService.getIocTypeById(ID_1));
  }

  @Test
  void failAttachOnDuplicateRepo() {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);

    assertThrows(
        InputValidationException.class,
        () -> {
          iocTypeService.attachInstances(
              1L,
              Collections.singletonList(new InstancesToAttach(GIT_PROJECT_ID, "new name")),
              null);
        });
  }

  @Test
  void attachInstance() {
    InstanceWithOperation dep =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    long typeId = dep.getOperation().getIocType().getId();

    PagedJobResponse operations = jobsService.findOperations(null, typeId, null, null, null);
    assertEquals(1, operations.getJobs().size());

    iocTypeService.attachInstances(
        typeId,
        Collections.singletonList(new InstancesToAttach(GIT_PROJECT_ID + 5, "new name")),
        null);

    operations = jobsService.findOperations(null, typeId, null, null, null);
    assertEquals(2, operations.getJobs().size());

    PagedIocResponse instances =
        iocInstanceService.findAll(typeId, null, null, null, null, null, true);
    assertEquals(2, instances.getIocs().size());
  }

  @Test
  void moveInstance() {
    InstanceWithOperation dep =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    long typeId1 = dep.getOperation().getIocType().getId();

    PagedJobResponse operations = jobsService.findOperations(null, typeId1, null, null, null);
    assertEquals(1, operations.getJobs().size());

    IocTypeEntity iocType2 = EntityFactory.createIocType(templateRepository, IOC_TYPE_NAME_2);

    operations = jobsService.findOperations(null, iocType2.getId(), null, null, null);
    assertEquals(0, operations.getJobs().size());

    iocInstanceService.moveInstance(iocType2.getId(), 1, "owner");

    operations = jobsService.findOperations(null, iocType2.getId(), null, null, null);
    assertEquals(1, operations.getJobs().size());

    PagedIocResponse instances =
        iocInstanceService.findAll(typeId1, null, null, null, null, null, true);
    assertEquals(0, instances.getIocs().size());

    instances = iocInstanceService.findAll(iocType2.getId(), null, null, null, null, null, true);
    assertEquals(1, instances.getIocs().size());
  }

  @Test
  void deleteTypeWhichHadMovedInstance() throws GitLabApiException {
    InstanceWithOperation dep =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    long typeId = dep.getOperation().getIocType().getId();

    IocTypeEntity iocType2 = EntityFactory.createIocType(templateRepository, IOC_TYPE_NAME_2);

    iocInstanceService.moveInstance(iocType2.getId(), dep.getInstance().getId(), null);

    Mockito.doNothing()
        .when(gitLabServiceMock)
        .deleteProjectById(dep.getInstance().getGitProjectId());

    OAuth2User user = EntityFactory.fetchOauthUser();
    iocTypeService.deleteIocType(user, typeId);
  }

  private static String getFileContent(final String resourceLocation) {
    try {
      return Files.readString(ResourceUtils.getFile(resourceLocation).toPath());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
