/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template;

import static eu.ess.ics.ce.template.TestConstants.CONFIGURATION_FILE_NAME;
import static eu.ess.ics.ce.template.TestConstants.CONFIG_GIT_PROJECT_ID;
import static eu.ess.ics.ce.template.TestConstants.DATE_TIME;
import static eu.ess.ics.ce.template.TestConstants.GIT_PROJECT_ID;
import static eu.ess.ics.ce.template.TestConstants.IOC_TYPE_NAME_1;
import static eu.ess.ics.ce.template.TestConstants.MAIN_BRANCH;
import static eu.ess.ics.ce.template.TestConstants.TAG_NAME;
import static eu.ess.ics.ce.template.TestConstants.TEMPLATE_GIT_PROJECT_ID;
import static eu.ess.ics.ce.template.TestConstants.USER_NAME;
import static eu.ess.ics.ce.template.TestConstants.VERSION;

import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Map;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public final class EntityFactory {

  public static final class InstanceWithOperation {
    private final IocInstanceEntity instance;
    private final OperationEntity operation;

    public InstanceWithOperation(IocInstanceEntity instance, OperationEntity operation) {
      this.instance = instance;
      this.operation = operation;
    }

    public IocInstanceEntity getInstance() {
      return instance;
    }

    public OperationEntity getOperation() {
      return operation;
    }
  }

  public static IocInstanceEntity createIocInstance(
      final IIocInstanceRepository iocInstanceRepository,
      final IOperationRepository operationRepository,
      final ITemplateRepository templateRepository) {

    final IocTypeEntity typeEnt = new IocTypeEntity();
    typeEnt.setName(IOC_TYPE_NAME_1);
    templateRepository.createIocType(typeEnt);

    final OperationEntity op = new OperationEntity();
    op.setTypeName(IOC_TYPE_NAME_1);
    op.setIocType(typeEnt);
    operationRepository.createOperation(op);

    final IocInstanceEntity iocInstance = new IocInstanceEntity();
    iocInstance.setCreatedBy(USER_NAME);
    iocInstance.setCreatedAt(DATE_TIME);
    iocInstance.setGitProjectId(GIT_PROJECT_ID);
    iocInstance.setGitRevision(MAIN_BRANCH);
    iocInstance.setGitTag(TAG_NAME);
    iocInstance.setConfigurationName(CONFIGURATION_FILE_NAME);
    iocInstance.setVersion(VERSION);
    iocInstance.setIocTypeName(IOC_TYPE_NAME_1);
    iocInstanceRepository.create(iocInstance);
    iocInstance.setIocType(typeEnt);

    return iocInstance;
  }

  public static InstanceWithOperation createIocInstanceWithAllDependency(
      final IIocInstanceRepository iocInstanceRepository,
      final IOperationRepository operationRepository,
      final ITemplateRepository templateRepository,
      final IInstanceLogRepository logRepository,
      final String iocTypeName) {
    final IocTypeEntity iocType = createIocType(templateRepository, iocTypeName);

    final IocInstanceEntity iocInstance = new IocInstanceEntity();
    iocInstance.setCreatedBy(USER_NAME);
    iocInstance.setCreatedAt(ZonedDateTime.now());
    iocInstance.setGitProjectId(GIT_PROJECT_ID);
    iocInstance.setGitRevision(MAIN_BRANCH);
    iocInstance.setGitTag(TAG_NAME);
    iocInstance.setConfigurationName(CONFIGURATION_FILE_NAME);
    iocInstance.setVersion(VERSION);
    iocInstance.setIocTypeName(iocTypeName);
    iocInstance.setIocType(iocType);
    iocInstance.setStatus(GenerationStatus.SUCCESSFUL.name());

    OperationEntity operation = createOperation(operationRepository, iocType);

    iocInstanceRepository.create(iocInstance);

    IocInstanceLogEntity log = new IocInstanceLogEntity();
    log.setOperation(operation);
    log.setIocInstance(iocInstance);
    log.setStatus(InstanceStatus.SUCCESSFUL.name());
    logRepository.create(log);

    return new InstanceWithOperation(iocInstance, operation);
  }

  public static IocTypeEntity createIocType(
      ITemplateRepository templateRepository, String iocTypeName) {
    final IocTypeEntity iocType = new IocTypeEntity();
    iocType.setName(iocTypeName);
    iocType.setCreatedAt(DATE_TIME);
    iocType.setCreatedBy(USER_NAME);
    iocType.setConfigGitProjectId(CONFIG_GIT_PROJECT_ID);
    iocType.setTemplateGitProjectId(TEMPLATE_GIT_PROJECT_ID);
    templateRepository.createIocType(iocType);
    return iocType;
  }

  public static OperationEntity createOperationAndType(
      IOperationRepository operationRepository,
      ITemplateRepository templateRepository,
      String iocTypeName) {
    final OperationEntity operationEntity = new OperationEntity();
    operationEntity.setCreatedBy(USER_NAME);
    operationEntity.setCreatedAt(DATE_TIME);
    operationEntity.setGitTag(TAG_NAME);
    operationEntity.setFinishedAt(DATE_TIME);
    operationEntity.setTemplateRevision(MAIN_BRANCH);
    operationEntity.setConfigurationRevision(MAIN_BRANCH);
    IocTypeEntity iocType = createIocType(templateRepository, iocTypeName);
    operationEntity.setIocType(iocType);
    operationEntity.setTypeName(iocType.getName());
    operationRepository.createOperation(operationEntity);
    return operationEntity;
  }

  public static OperationEntity createOperation(
      IOperationRepository operationRepository, IocTypeEntity iocType) {
    final OperationEntity operationEntity = new OperationEntity();
    operationEntity.setCreatedBy(USER_NAME);
    operationEntity.setCreatedAt(DATE_TIME);
    operationEntity.setGitTag(TAG_NAME);
    operationEntity.setFinishedAt(DATE_TIME);
    operationEntity.setTemplateRevision(MAIN_BRANCH);
    operationEntity.setConfigurationRevision(MAIN_BRANCH);
    operationEntity.setJobType(JobType.GENERATE_INSTANCE.name());

    operationEntity.setIocType(iocType);
    operationEntity.setTypeName(iocType.getName());
    operationRepository.createOperation(operationEntity);
    return operationEntity;
  }

  public static OAuth2User fetchOauthUser() {
    return new OAuth2User() {
      @Override
      public Map<String, Object> getAttributes() {
        return null;
      }

      @Override
      public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
      }

      @Override
      public String getName() {
        return "testUser";
      }
    };
  }

  private EntityFactory() {}
}
