/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal;

import static eu.ess.ics.ce.template.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import eu.ess.ics.ce.template.CeTemplatingBackendApplication;
import eu.ess.ics.ce.template.EntityFactory;
import eu.ess.ics.ce.template.EntityFactory.InstanceWithOperation;
import eu.ess.ics.ce.template.common.Literals;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceGenerationStatus;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceGenerationStep;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceLogEntry;
import eu.ess.ics.ce.template.rest.model.jobs.response.Job;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobInstanceLogResponse;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.rest.model.jobs.response.PagedJobResponse;
import eu.ess.ics.ce.template.rest.model.types.request.InstancesToAttach;
import eu.ess.ics.ce.template.rest.model.types.request.IocFetchOption;
import eu.ess.ics.ce.template.rest.model.types.request.ProcessTypeRequest;
import eu.ess.ics.ce.template.rest.model.types.response.Ioc;
import eu.ess.ics.ce.template.rest.model.types.response.OperationStartedResponse;
import eu.ess.ics.ce.template.rest.model.types.response.PagedIocResponse;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.internal.iocinstance.IocInstanceService;
import eu.ess.ics.ce.template.service.internal.jobs.JobsService;
import eu.ess.ics.ce.template.service.internal.jobs.LogService;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import eu.ess.ics.ce.template.service.internal.template.IocTypeService;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.AssertionsForClassTypes;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.TreeItem;
import org.gitlab4j.api.models.TreeItem.Type;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.util.ResourceUtils;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {IocInstanceService.class, CeTemplatingBackendApplication.class})
@DirtiesContext
@Tag("IntegrationTest")
public class IocInstanceServiceIT {

  public static final String README_FILE_CONTENT = "readme file content";
  public static final String COMMIT_ID = "commitId";
  private static final String FAILED_TO_CREATE_GIT_PROJECT_MESSAGE = "Failed to create Git project";
  private static String SCHEMA_FILE_CONTENT;
  private static String CONFIG_FILE_CONTENT;
  private static String TEMPLATE_FILE_CONTENT;
  private static String FILE_TO_COPY_CONTENT;
  private static GitLabService.GitFile SCHEMA_FILE;
  private static GitLabService.GitFile CONFIG_FILE;
  private static GitLabService.GitFile TEMPLATE_FILE;
  private static GitLabService.GitFile FILE_TO_COPY;
  private static final String TEMPLATE_FILE_NAME = "st.cmd.mustache";
  private static final String SCHEMA_FILE_NAME = "config-schema.json";
  private static final String FILE_TO_COPY_NAME = "ioc.json";
  private static final String RANDOM_IRRELEVANT_FILE = "random.txt";

  @MockBean private GitLabService gitLabService;

  @Autowired private ITemplateRepository templateRepository;

  @Autowired private IIocInstanceRepository iocInstanceRepository;

  @Autowired private IOperationRepository operationRepository;

  @Autowired private IocInstanceService iocInstanceService;

  @Autowired private LogService logService;

  @Autowired private JobsService jobsService;

  @Autowired private IInstanceLogRepository logRepository;
  @Autowired private IocTypeService iocTypeService;

  @Value("${spring.datasource.url}")
  private String dataSourceUrl;

  @Value("${spring.datasource.hikari.username}")
  private String dataSourceUsername;

  @Value("${spring.datasource.hikari.password}")
  private String dataSourcePassword;

  @BeforeEach
  void setUp() {
    Mockito.reset(gitLabService);
    initSchemaFile();
    initConfigFile();
    initTemplateFile();
    initFileToCopy();
  }

  @AfterEach
  void clean() throws SQLException {
    Connection conn =
        DriverManager.getConnection(dataSourceUrl, dataSourceUsername, dataSourcePassword);
    Statement st = conn.createStatement();
    st.executeUpdate("set referential_integrity false");
    st.executeUpdate("delete from ioc_instance");
    st.executeUpdate("alter table ioc_instance alter column id restart with 1");
    st.executeUpdate("delete from ioc_type");
    st.executeUpdate("alter table ioc_type alter column id restart with 1");
    st.executeUpdate("delete from operation");
    st.executeUpdate("alter table operation alter column id restart with 1");
    st.executeUpdate("delete from ioc_instance_log");
    st.executeUpdate("alter table ioc_instance_log alter column id restart with 1");
    st.executeUpdate("set referential_integrity true");
    conn.close();
  }

  @Test
  public void findAllSuccess() throws GitLabApiException {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);

    Mockito.when(gitLabService.projectFromId(TEMPLATE_GIT_PROJECT_ID)).thenReturn(TEMPLATE_PROJECT);

    Mockito.when(gitLabService.projectFromId(CONFIG_GIT_PROJECT_ID)).thenReturn(CONFIG_PROJECT);

    final PagedIocResponse actual =
        iocInstanceService.findAll(
            null, CONFIGURATION_FILE_NAME, USER_NAME, PAGE, LIMIT, IocFetchOption.ALL, false);
    final List<Ioc> expectedIocs =
        List.of(
            new Ioc(
                ID_1,
                TEMPLATE_GIT_PROJECT_ID,
                IOC_TYPE_NAME_1,
                USER_NAME,
                DATE_TIME,
                GIT_PROJECT_ID,
                TAG_NAME,
                CONFIGURATION_FILE_NAME,
                VERSION));

    assertEquals(1, actual.getTotalCount());
    assertEquals(1, actual.getListSize());
    assertEquals(PAGE, actual.getPageNumber());
    assertEquals(LIMIT, actual.getLimit());

    AssertionsForClassTypes.assertThat(actual.getIocs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("typeId")
        .isEqualTo(expectedIocs);
  }

  @Test
  public void findLatestInstancesSuccess() {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_2);
    final List<Ioc> expectedIocs =
        List.of(
            new Ioc(
                ID_2,
                TEMPLATE_GIT_PROJECT_ID,
                IOC_TYPE_NAME_2,
                USER_NAME,
                DATE_TIME,
                GIT_PROJECT_ID,
                TAG_NAME,
                CONFIGURATION_FILE_NAME,
                VERSION));
    PagedIocResponse actual =
        iocInstanceService.findAll(
            ID_2, CONFIGURATION_FILE_NAME, null, PAGE, LIMIT, IocFetchOption.LATEST, false);

    AssertionsForClassTypes.assertThat(actual.getIocs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("typeId")
        .isEqualTo(expectedIocs);
  }

  @Test
  public void processTemplateSuccess() throws GitLabApiException {
    EntityFactory.createIocInstanceWithAllDependency(
        iocInstanceRepository,
        operationRepository,
        templateRepository,
        logRepository,
        IOC_TYPE_NAME_1);

    TreeItem stCmd = new TreeItem();
    stCmd.setName(TEMPLATE_FILE_NAME);
    stCmd.setPath(TEMPLATE_FILE_NAME);
    stCmd.setType(Type.BLOB);

    TreeItem iocFile = new TreeItem();
    iocFile.setName(FILE_TO_COPY_NAME);
    iocFile.setPath(FILE_TO_COPY_NAME);
    iocFile.setType(Type.BLOB);

    TreeItem schemaFile = new TreeItem();
    schemaFile.setName(SCHEMA_FILE_NAME);
    schemaFile.setPath(SCHEMA_FILE_NAME);
    schemaFile.setType(Type.BLOB);

    TreeItem randomFile = new TreeItem();
    randomFile.setName(RANDOM_IRRELEVANT_FILE);
    randomFile.setPath(RANDOM_IRRELEVANT_FILE);
    randomFile.setType(Type.BLOB);

    final List<TreeItem> projectFiles = List.of(stCmd, iocFile, schemaFile, randomFile);

    Mockito.when(gitLabService.checkTemplateExists(TEMPLATE_GIT_PROJECT_ID))
        .thenReturn(TEMPLATE_PROJECT);
    Mockito.when(gitLabService.checkConfigExists(CONFIG_GIT_PROJECT_ID)).thenReturn(CONFIG_PROJECT);
    Mockito.when(
            gitLabService.getFile(
                TEMPLATE_GIT_PROJECT_ID, Literals.ST_CMD_TEMPLATE_NAME, MAIN_BRANCH))
        .thenReturn(TEMPLATE_FILE);
    Mockito.when(gitLabService.getFile(TEMPLATE_GIT_PROJECT_ID, Literals.SCHEMA_NAME, MAIN_BRANCH))
        .thenReturn(SCHEMA_FILE);
    Mockito.when(gitLabService.listFilesInRepo(CONFIG_GIT_PROJECT_ID, MAIN_BRANCH))
        .thenReturn(List.of(CONFIGURATION_FILE_NAME));
    Mockito.when(gitLabService.getFile(CONFIG_GIT_PROJECT_ID, CONFIG_NAME, MAIN_BRANCH))
        .thenReturn(CONFIG_FILE);
    Mockito.when(gitLabService.getFile(TEMPLATE_GIT_PROJECT_ID, FILE_TO_COPY_NAME, MAIN_BRANCH))
        .thenReturn(FILE_TO_COPY);
    Mockito.when(gitLabService.allFilesAndFolders(TEMPLATE_PROJECT, MAIN_BRANCH))
        .thenReturn(projectFiles);
    Mockito.when(gitLabService.createOutputProject(INSTANCE_NAME))
        .thenReturn(CONFIG_PROJECT_CHANGED);
    Mockito.when(gitLabService.getReadmeTemplate()).thenReturn(README_FILE_CONTENT);
    Mockito.when(
            gitLabService.createCommit(
                Mockito.any(Project.class),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(List.class),
                Mockito.any(List.class)))
        .thenReturn(COMMIT);
    Mockito.when(gitLabService.createTag(CONFIG_PROJECT_CHANGED, TAG_NAME, COMMIT_ID))
        .thenReturn(TAG);

    ProcessTypeRequest request = new ProcessTypeRequest();
    request.setConfigNames(List.of(CONFIG_NAME));
    request.setTypeReference(MAIN_BRANCH);
    request.setConfigReference(MAIN_BRANCH);

    final OperationStartedResponse actual =
        iocInstanceService.processTemplate(ID_1, request, USER_NAME);
    final OperationStartedResponse expected = new OperationStartedResponse(ID_2);

    assertEquals(expected, actual);

    final OperationStartedResponse newInstanceOperation =
        iocInstanceService.processTemplate(ID_1, request, USER_NAME);
    final OperationStartedResponse expectedNewOperation = new OperationStartedResponse(ID_3);

    assertEquals(expectedNewOperation, newInstanceOperation);
  }

  @Test
  public void getInstanceLogFinished() {
    InstanceWithOperation dependencies =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    IocInstanceEntity iocInstance = dependencies.getInstance();
    OperationEntity operation = dependencies.getOperation();
    logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operation);

    logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_CREATING, operation);
    IocInstanceLogEntity gitProjectCreatedLog =
        logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_CREATED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_TAGGING_AND_COMMITING, operation);
    IocInstanceLogEntity committedLog =
        logService.createLogEntry(iocInstance, InstanceStatus.GIT_TAGGGED_AND_COMMITED, operation);

    List<InstanceLogEntry> expectedLogs =
        List.of(
            new InstanceLogEntry(
                InstanceGenerationStep.CREATE_GIT_PROJECT,
                InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SUCCESSFUL,
                gitProjectCreatedLog.getDescription()),
            new InstanceLogEntry(
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SUCCESSFUL,
                committedLog.getDescription()));
    JobInstanceLogResponse expected =
        new JobInstanceLogResponse(
            iocInstance.getId(),
            InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
            InstanceGenerationStatus.SUCCESSFUL,
            expectedLogs);
    JobInstanceLogResponse actual =
        iocInstanceService.getInstanceLog(operation.getId(), iocInstance.getId());
    assertEquals(expected.currentStep(), actual.currentStep());
    AssertionsForClassTypes.assertThat(actual.logs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected.logs());
  }

  @Test
  public void getInstanceLogOngoing() {
    InstanceWithOperation dependencies =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    IocInstanceEntity iocInstance = dependencies.getInstance();
    OperationEntity operation = dependencies.getOperation();
    logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operation);
    IocInstanceLogEntity gitProjectCreatingLog =
        logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_CREATING, operation);

    List<InstanceLogEntry> expectedLogs =
        List.of(
            new InstanceLogEntry(
                InstanceGenerationStep.CREATE_GIT_PROJECT,
                InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.RUNNING,
                gitProjectCreatingLog.getDescription()),
            new InstanceLogEntry(
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.QUEUED,
                null));
    JobInstanceLogResponse expected =
        new JobInstanceLogResponse(
            iocInstance.getId(),
            InstanceGenerationStep.CREATE_GIT_PROJECT,
            InstanceGenerationStatus.RUNNING,
            expectedLogs);

    JobInstanceLogResponse actual =
        iocInstanceService.getInstanceLog(operation.getId(), iocInstance.getId());
    assertEquals(expected.currentStep(), actual.currentStep());
    AssertionsForClassTypes.assertThat(actual.logs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected.logs());
  }

  @Test
  public void getInstanceLogFailed() {
    InstanceWithOperation dependencies =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    IocInstanceEntity iocInstance = dependencies.getInstance();
    OperationEntity operation = dependencies.getOperation();
    IocInstanceLogEntity queuedLog =
        logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_CREATING, operation);
    logService.createLogEntry(
        iocInstance, FAILED_TO_CREATE_GIT_PROJECT_MESSAGE, InstanceStatus.FAILED, operation);

    List<InstanceLogEntry> expectedLogs =
        List.of(
            new InstanceLogEntry(
                InstanceGenerationStep.CREATE_GIT_PROJECT,
                InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
                queuedLog.getCreatedAt(),
                null,
                InstanceGenerationStatus.FAILED,
                FAILED_TO_CREATE_GIT_PROJECT_MESSAGE),
            new InstanceLogEntry(
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SKIPPED,
                null));
    JobInstanceLogResponse expected =
        new JobInstanceLogResponse(
            iocInstance.getId(),
            InstanceGenerationStep.CREATE_GIT_PROJECT,
            InstanceGenerationStatus.FAILED,
            expectedLogs);

    JobInstanceLogResponse actual =
        iocInstanceService.getInstanceLog(operation.getId(), iocInstance.getId());
    assertEquals(expected.currentStep(), actual.currentStep());
    AssertionsForClassTypes.assertThat(actual.logs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected.logs());
  }

  @Test
  public void getInstanceLogWithSkippedProjectCreation() {
    InstanceWithOperation dependencies =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    IocInstanceEntity iocInstance = dependencies.getInstance();
    OperationEntity operation = dependencies.getOperation();
    logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_FETCHING, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_FETCHED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_TAGGING_AND_COMMITING, operation);
    IocInstanceLogEntity commitLog =
        logService.createLogEntry(iocInstance, InstanceStatus.GIT_TAGGGED_AND_COMMITED, operation);
    List<InstanceLogEntry> expectedLogs =
        List.of(
            new InstanceLogEntry(
                InstanceGenerationStep.CREATE_GIT_PROJECT,
                InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SKIPPED,
                null),
            new InstanceLogEntry(
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SUCCESSFUL,
                commitLog.getDescription()));
    JobInstanceLogResponse expected =
        new JobInstanceLogResponse(
            iocInstance.getId(),
            InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
            InstanceGenerationStatus.SUCCESSFUL,
            expectedLogs);
    JobInstanceLogResponse actual =
        iocInstanceService.getInstanceLog(operation.getId(), iocInstance.getId());
    assertEquals(expected.currentStep(), actual.currentStep());
    AssertionsForClassTypes.assertThat(actual.logs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected.logs());
  }

  @Test
  public void getInstanceLogWithSkippedProjectCreationFailure() {
    InstanceWithOperation dependencies =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);
    IocInstanceEntity iocInstance = dependencies.getInstance();
    OperationEntity operation = dependencies.getOperation();

    logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operation);
    logService.createLogEntry(iocInstance, InstanceStatus.GIT_PROJECT_FETCHING, operation);
    IocInstanceLogEntity failedLog =
        logService.createLogEntry(iocInstance, "failed reason", InstanceStatus.FAILED, operation);
    List<InstanceLogEntry> expectedLogs =
        List.of(
            new InstanceLogEntry(
                InstanceGenerationStep.CREATE_GIT_PROJECT,
                InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.SKIPPED,
                null),
            new InstanceLogEntry(
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
                InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT.ordinal(),
                null,
                null,
                InstanceGenerationStatus.FAILED,
                failedLog.getDescription()));
    JobInstanceLogResponse expected =
        new JobInstanceLogResponse(
            iocInstance.getId(),
            InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT,
            InstanceGenerationStatus.FAILED,
            expectedLogs);
    JobInstanceLogResponse actual =
        iocInstanceService.getInstanceLog(operation.getId(), iocInstance.getId());
    assertEquals(expected.currentStep(), actual.currentStep());
    AssertionsForClassTypes.assertThat(actual.logs())
        .usingRecursiveComparison()
        .ignoringFields("createdAt")
        .ignoringFields("finishedAt")
        .isEqualTo(expected.logs());
  }

  @Test
  void checkOperationType() {
    InstanceWithOperation dep =
        EntityFactory.createIocInstanceWithAllDependency(
            iocInstanceRepository,
            operationRepository,
            templateRepository,
            logRepository,
            IOC_TYPE_NAME_1);

    long typeId = dep.getOperation().getIocType().getId();

    PagedJobResponse operations = jobsService.findOperations(null, typeId, null, null, null);
    assertEquals(1, operations.getJobs().size());

    assertEquals(JobType.GENERATE_INSTANCE, operations.getJobs().get(0).jobType());

    iocTypeService.attachInstances(
        typeId,
        Collections.singletonList(new InstancesToAttach(GIT_PROJECT_ID + 1, "new name")),
        null);

    operations = jobsService.findOperations(null, typeId, null, null, null);

    boolean foundAttach = false;

    for (Job job : operations.getJobs()) {
      if (JobType.ATTACH.equals(job.jobType())) {
        foundAttach = true;
      }
    }

    assertTrue(foundAttach);
  }

  private static void initConfigFile() {
    CONFIG_FILE_CONTENT = getFileContent("classpath:config_project_files/test-instance1.json");
    CONFIG_FILE = new GitLabService.GitFile(CONFIG_FILE_CONTENT, COMMIT_ID);
  }

  private static void initSchemaFile() {
    SCHEMA_FILE_CONTENT = getFileContent("classpath:template_project_files/config-schema.json");
    SCHEMA_FILE = new GitLabService.GitFile(SCHEMA_FILE_CONTENT, COMMIT_ID);
  }

  private static void initTemplateFile() {
    TEMPLATE_FILE_CONTENT = getFileContent("classpath:template_project_files/st.cmd.mustache");
    TEMPLATE_FILE = new GitLabService.GitFile(TEMPLATE_FILE_CONTENT, COMMIT_ID);
  }

  private static void initFileToCopy() {
    FILE_TO_COPY_CONTENT = getFileContent("classpath:template_project_files/ioc.json");
    FILE_TO_COPY = new GitLabService.GitFile(FILE_TO_COPY_CONTENT, COMMIT_ID);
  }

  private static String getFileContent(final String resourceLocation) {
    try {
      return Files.readString(ResourceUtils.getFile(resourceLocation).toPath())
          .replaceAll("\r\n", "\n");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
