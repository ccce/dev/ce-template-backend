package eu.ess.ics.ce.template.repository.impl;

import eu.ess.ics.ce.template.repository.api.IIocTypeHistoryRepository;
import eu.ess.ics.ce.template.repository.entity.IocTypeHistoryEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Repository
public class IocTypeHistoryRepository implements IIocTypeHistoryRepository {
  @PersistenceContext private EntityManager em;

  @Override
  public void create(IocTypeHistoryEntity historyEntity) {
    em.persist(historyEntity);
  }

  @Override
  public List<IocTypeHistoryEntity> findTypeHistory(long typeId, int page, int limit) {
    return em.createQuery(
            "SELECT historyEntry FROM IocTypeHistoryEntity historyEntry"
                + " WHERE historyEntry.typeId = :typeID",
            IocTypeHistoryEntity.class)
        .setParameter("typeID", typeId)
        .setFirstResult(page * limit)
        .setMaxResults(limit)
        .getResultList();
  }

  @Override
  public long countForPaging(long typeId) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocTypeHistoryEntity historyEntry"
                + " WHERE historyEntry.typeId = :typeID",
            Long.class)
        .setParameter("typeID", typeId)
        .getSingleResult();
  }

  @Override
  public void deleteIocTypeHistory(long typeId) {
    em.createQuery(
            "DELETE FROM IocTypeHistoryEntity historyEntry WHERE historyEntry.typeId = :typeId")
        .setParameter("typeId", typeId)
        .executeUpdate();
  }
}
