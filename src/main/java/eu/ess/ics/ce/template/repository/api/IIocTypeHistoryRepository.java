package eu.ess.ics.ce.template.repository.api;

import eu.ess.ics.ce.template.repository.entity.IocTypeHistoryEntity;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public interface IIocTypeHistoryRepository {
  @Transactional
  void create(IocTypeHistoryEntity historyEntity);

  List<IocTypeHistoryEntity> findTypeHistory(long typeId, int page, int limit);

  long countForPaging(long typeId);

  @Transactional
  void deleteIocTypeHistory(long typeId);
}
