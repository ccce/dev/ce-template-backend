package eu.ess.ics.ce.template.service.internal.audit.dto;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public enum AuditEvent {
  CREATE,
  UPDATE,
  DELETE,
  RESTORE
}
