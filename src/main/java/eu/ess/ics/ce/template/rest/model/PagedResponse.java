package eu.ess.ics.ce.template.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class PagedResponse {
  @NotNull
  @JsonProperty("total_count")
  private long totalCount;

  @NotNull
  @JsonProperty("list_size")
  private int listSize;

  @NotNull
  @JsonProperty("page")
  private int pageNumber;

  @JsonProperty("page_size")
  private Integer limit;

  public PagedResponse() {}

  public PagedResponse(long totalCount, int listSize, int pageNumber, Integer limit) {
    this.totalCount = totalCount;
    this.listSize = listSize;
    this.pageNumber = pageNumber;
    this.limit = limit;
  }

  public long getTotalCount() {
    return totalCount;
  }

  public int getListSize() {
    return listSize;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public Integer getLimit() {
    return limit;
  }
}
