package eu.ess.ics.ce.template.repository.api;

import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public interface ITemplateRepository {

  List<IocTypeEntity> findAll(
      String createdBy, String query, boolean includeDeleted, int page, Integer limit);

  IocTypeEntity findById(Long id);

  @Transactional
  void createIocType(IocTypeEntity iocType);

  @Transactional
  void updateIocType(IocTypeEntity iocType);

  long countForPaging(String createdBy, String query, boolean includeDeleted);

  @Transactional
  void deleteIocTypeAndConfigById(long iocTypeId);

  Long countAll();
}
