/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.controller;

import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.ServiceException;
import eu.ess.ics.ce.template.rest.api.v1.IJobs;
import eu.ess.ics.ce.template.rest.model.jobs.response.*;
import eu.ess.ics.ce.template.service.internal.iocinstance.IocInstanceService;
import eu.ess.ics.ce.template.service.internal.jobs.JobsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
public class JobsController implements IJobs {

  private static final Logger LOGGER = LoggerFactory.getLogger(JobsController.class);

  private final JobsService jobsService;
  private final IocInstanceService instanceService;

  public JobsController(JobsService jobsService, IocInstanceService instanceService) {
    this.jobsService = jobsService;
    this.instanceService = instanceService;
  }

  @Override
  public PagedJobResponse listJobs(
      String query, Long iocTypeId, String user, Integer page, Integer limit) {
    try {
      return jobsService.findOperations(query, iocTypeId, page, limit, user);
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch operations", e);
      throw new ServiceException("Error while tying to fetch operations");
    }
  }

  @Override
  public JobDetails jobDetails(Long jobId) {
    try {
      return jobsService.getOperationById(jobId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Operation not found with ID", e);
      throw new EntityNotFoundException("Operation", jobId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch operation details by ID", e);
      throw new ServiceException("Error while trying to fetch operation details");
    }
  }

  @Override
  public JobInstanceListResponse jobInstances(final Long jobId, Integer page, Integer limit) {
    try {
      return jobsService.getInstancesOfOperation(jobId, page, limit);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Operation not found with ID", e);
      throw new EntityNotFoundException("Operation", jobId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch operation instances by ID", e);
      throw new ServiceException("Error while trying to fetch operation instances");
    }
  }

  @Override
  public JobStatusResponse jobStatus(final Long jobId) {
    try {
      return jobsService.getOperationStatusById(jobId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Operation not found with ID", e);
      throw new EntityNotFoundException("Operation", jobId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch operation status by ID", e);
      throw new ServiceException("Error while trying to fetch operation status");
    }
  }

  @Override
  public JobInstanceLogResponse getInstanceLog(Long operationId, Long instanceId) {
    try {
      return instanceService.getInstanceLog(operationId, instanceId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("IOC instance not found with ID", e);
      throw new EntityNotFoundException("IOC Instance", instanceId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to fetch IOC instance log by ID", e);
      throw new ServiceException("Error while trying to fetch IOC instance log");
    }
  }
}
