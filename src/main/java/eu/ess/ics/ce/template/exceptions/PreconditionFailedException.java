package eu.ess.ics.ce.template.exceptions;

public class PreconditionFailedException extends CcceException {
  public PreconditionFailedException(String description) {
    super("Precondition failed", description);
  }
}
