/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ce.template.common.conversion.ZonedDateTimeSerializer;
import jakarta.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

public class TypeBase {
  @NotNull private final Long id;
  @NotNull private final String name;

  @NotNull
  @JsonProperty("type_project_id")
  private final Long typeProjectId;

  @JsonProperty("configuration_project_id")
  private final Long configurationProjectId;

  @NotNull
  @JsonProperty("created_by")
  private final String createdBy;

  @JsonProperty("created_at")
  @JsonSerialize(using = ZonedDateTimeSerializer.class)
  private final ZonedDateTime createdAt;

  @JsonProperty("number_of_iocs")
  private final long numberOfIocs;

  @JsonProperty("latest_ioc_tag")
  private final String latestIocTag;

  @JsonProperty("latest_operation_id")
  private final Long latestOperationId;

  private String description;

  @NotNull private boolean deleted;

  public TypeBase(
      Long id,
      String name,
      String description,
      Long typeProjectId,
      Long configurationProjectId,
      String createdBy,
      ZonedDateTime createdAt,
      long numberOfIocs,
      String latestIocTag,
      Long latestOperationId,
      boolean deleted) {
    this.id = id;
    this.name = name;
    this.typeProjectId = typeProjectId;
    this.configurationProjectId = configurationProjectId;
    this.createdBy = createdBy;
    this.createdAt = createdAt;
    this.numberOfIocs = numberOfIocs;
    this.latestIocTag = latestIocTag;
    this.latestOperationId = latestOperationId;
    this.description = description;
    this.deleted = deleted;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public Long getTypeProjectId() {
    return typeProjectId;
  }

  public Long getConfigurationProjectId() {
    return configurationProjectId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public long getNumberOfIocs() {
    return numberOfIocs;
  }

  public String getLatestIocTag() {
    return latestIocTag;
  }

  public Long getLatestOperationId() {
    return latestOperationId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TypeBase typeBase = (TypeBase) o;
    return numberOfIocs == typeBase.numberOfIocs
        && deleted == typeBase.deleted
        && Objects.equals(id, typeBase.id)
        && Objects.equals(name, typeBase.name)
        && Objects.equals(typeProjectId, typeBase.typeProjectId)
        && Objects.equals(configurationProjectId, typeBase.configurationProjectId)
        && Objects.equals(createdBy, typeBase.createdBy)
        && Objects.equals(createdAt, typeBase.createdAt)
        && Objects.equals(latestIocTag, typeBase.latestIocTag)
        && Objects.equals(latestOperationId, typeBase.latestOperationId)
        && Objects.equals(description, typeBase.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id,
        name,
        typeProjectId,
        configurationProjectId,
        createdBy,
        createdAt,
        numberOfIocs,
        latestIocTag,
        latestOperationId,
        description,
        deleted);
  }

  @Override
  public String toString() {
    return "TypeBase{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", typeProjectId="
        + typeProjectId
        + ", configurationProjectId="
        + configurationProjectId
        + ", createdBy='"
        + createdBy
        + '\''
        + ", createdAt="
        + createdAt
        + ", numberOfIocs="
        + numberOfIocs
        + ", latestIocTag='"
        + latestIocTag
        + '\''
        + ", latestOperationId="
        + latestOperationId
        + ", description='"
        + description
        + '\''
        + ", deleted="
        + deleted
        + '}';
  }
}
