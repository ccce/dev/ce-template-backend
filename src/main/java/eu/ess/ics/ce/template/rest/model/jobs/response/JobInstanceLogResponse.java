package eu.ess.ics.ce.template.rest.model.jobs.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.util.List;

public record JobInstanceLogResponse(
    @NotNull @JsonProperty("instance_id") Long instanceId,
    @JsonProperty("current_step") InstanceGenerationStep currentStep,
    @JsonProperty("total_status") InstanceGenerationStatus totalStatus,
    List<InstanceLogEntry> logs) {}
