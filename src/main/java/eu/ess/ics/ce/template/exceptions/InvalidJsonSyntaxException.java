package eu.ess.ics.ce.template.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class InvalidJsonSyntaxException extends Exception {
  public InvalidJsonSyntaxException(String message) {
    super(message);
  }
}
