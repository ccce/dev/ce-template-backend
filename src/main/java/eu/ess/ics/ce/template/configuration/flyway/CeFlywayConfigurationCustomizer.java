/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.configuration.flyway;

import java.util.Map;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.flywaydb.core.api.migration.JavaMigration;
import org.springframework.boot.autoconfigure.flyway.FlywayConfigurationCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Configuration for loading the migration classes (Spring Boot) Based on: <a
 * href="https://stackoverflow.com/questions/48214783/flyway-spring-boot-autowired-beans-with-jpa-dependency">
 * Flyway Spring Boot Autowired Beans with JPA Dependency </a>
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class CeFlywayConfigurationCustomizer implements FlywayConfigurationCustomizer {
  private final ApplicationContext context;

  /**
   * Flyway configurer constructor
   *
   * @param context Spring boot application context
   */
  public CeFlywayConfigurationCustomizer(ApplicationContext context) {
    this.context = context;
  }

  @Override
  public void customize(FluentConfiguration configuration) {
    Map<String, JavaMigration> migrationBeans = context.getBeansOfType(JavaMigration.class);
    JavaMigration[] migrationBeansAsArray = migrationBeans.values().toArray(new JavaMigration[0]);
    configuration.javaMigrations(migrationBeansAsArray);
  }
}
