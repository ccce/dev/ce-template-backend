/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.model.types.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class ProcessTypeRequest {

  @JsonProperty("type_revision")
  private String typeReference;

  @JsonProperty("config_revision")
  private String configReference;

  @JsonProperty("config_names")
  private List<String> configNames;

  public String getTypeReference() {
    return typeReference;
  }

  public void setTypeReference(String typeReference) {
    this.typeReference = typeReference;
  }

  public String getConfigReference() {
    return configReference;
  }

  public void setConfigReference(String configReference) {
    this.configReference = configReference;
  }

  public List<String> getConfigNames() {
    return configNames;
  }

  public void setConfigNames(List<String> configNames) {
    this.configNames = configNames;
  }
}
