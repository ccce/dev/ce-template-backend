package eu.ess.ics.ce.template.configuration.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.ess.ics.ce.template.exceptions.ConfigListValidationException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class InvalidConfigException extends GeneralException {
  @JsonProperty("invalid_configs")
  private final Map<String, String> invalidConfigs;

  public InvalidConfigException(ConfigListValidationException e) {
    setError(e.getError());
    setDescription(e.getDescription());
    this.invalidConfigs =
        e.getErrorMap().entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, error -> error.getValue().getMessage()));
  }

  public Map<String, String> getInvalidConfigs() {
    return invalidConfigs;
  }
}
