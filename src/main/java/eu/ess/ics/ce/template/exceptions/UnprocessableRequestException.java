package eu.ess.ics.ce.template.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class UnprocessableRequestException extends CcceException {
  public UnprocessableRequestException(String error, String description) {
    super(error, description);
  }
}
