package eu.ess.ics.ce.template.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Entity
@Table(name = "ioc_type")
public class IocTypeEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "created_at")
  private ZonedDateTime createdAt;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "template_git_project_id")
  private Long templateGitProjectId;

  @Column(name = "config_git_project_id")
  private Long configGitProjectId;

  @Column(name = "max_version")
  private Long maxVersion;

  @Column(name = "description")
  private String description;

  @Column(name = "deleted")
  private boolean deleted;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getTemplateGitProjectId() {
    return templateGitProjectId;
  }

  public void setTemplateGitProjectId(Long gitProjectId) {
    this.templateGitProjectId = gitProjectId;
  }

  public Long getConfigGitProjectId() {
    return configGitProjectId;
  }

  public void setConfigGitProjectId(Long configGitProjectId) {
    this.configGitProjectId = configGitProjectId;
  }

  public Long getMaxVersion() {
    return maxVersion;
  }

  public void setMaxVersion(Long maxVersion) {
    this.maxVersion = maxVersion;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    IocTypeEntity iocType = (IocTypeEntity) o;
    return Objects.equals(id, iocType.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "IocTypeEntity{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", createdAt="
        + createdAt
        + ", createdBy='"
        + createdBy
        + '\''
        + ", templateGitProjectId="
        + templateGitProjectId
        + ", configGitProjectId="
        + configGitProjectId
        + ", maxVersion="
        + maxVersion
        + ", description='"
        + description
        + '\''
        + ", deleted="
        + deleted
        + '}';
  }
}
