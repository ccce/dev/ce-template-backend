package eu.ess.ics.ce.template.service.internal.jobs;

import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class LogService {

  private final IInstanceLogRepository logRepository;

  public LogService(IInstanceLogRepository logRepository) {
    this.logRepository = logRepository;
  }

  @Transactional
  public IocInstanceLogEntity createLogEntry(
      final IocInstanceEntity iocInstance,
      final String message,
      final InstanceStatus status,
      final OperationEntity operationEntity) {
    IocInstanceLogEntity iocInstanceLogEntity = new IocInstanceLogEntity();
    iocInstanceLogEntity.setIocInstance(iocInstance);
    iocInstanceLogEntity.setDescription(message);
    iocInstanceLogEntity.setStatus(status.toString());
    iocInstanceLogEntity.setType("CREATE_IOC");
    iocInstanceLogEntity.setCreatedAt(ZonedDateTime.now());
    iocInstanceLogEntity.setOperation(operationEntity);
    iocInstanceLogEntity.setVersion(operationEntity.getVersion());
    iocInstanceLogEntity.setTypeName(operationEntity.getTypeName());
    iocInstanceLogEntity.setCreatedBy(operationEntity.getCreatedBy());
    return logRepository.create(iocInstanceLogEntity);
  }

  @Transactional
  public IocInstanceLogEntity createLogEntry(
      final IocInstanceEntity iocInstance,
      final InstanceStatus status,
      final OperationEntity operationEntity) {
    IocInstanceLogEntity iocInstanceLogEntity = new IocInstanceLogEntity();
    iocInstanceLogEntity.setIocInstance(iocInstance);
    iocInstanceLogEntity.setDescription(status.getMessage());
    iocInstanceLogEntity.setStatus(status.toString());
    iocInstanceLogEntity.setType("CREATE_IOC");
    iocInstanceLogEntity.setCreatedAt(ZonedDateTime.now());
    iocInstanceLogEntity.setOperation(operationEntity);
    iocInstanceLogEntity.setVersion(operationEntity.getVersion());
    iocInstanceLogEntity.setTypeName(operationEntity.getTypeName());
    iocInstanceLogEntity.setCreatedBy(operationEntity.getCreatedBy());
    return logRepository.create(iocInstanceLogEntity);
  }

  public void closeLastEntry(long instanceId, long operationId) {
    IocInstanceLogEntity lastLogEntry = logRepository.findLastLogEntry(instanceId, operationId);

    if (lastLogEntry != null) {
      lastLogEntry.setFinishedAt(ZonedDateTime.now());
      logRepository.updateLogEntry(lastLogEntry);
    }
  }

  public Map<InstanceStatus, IocInstanceLogEntity> getIocInstanceGenerationProgress(
      Long operationId, Long instanceId) {
    return logRepository.findLogsForIocInstance(operationId, instanceId).stream()
        .collect(Collectors.toMap(l -> InstanceStatus.valueOf(l.getStatus()), l -> l));
  }
}
