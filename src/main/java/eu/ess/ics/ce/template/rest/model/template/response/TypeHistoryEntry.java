package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ce.template.common.conversion.ZonedDateTimeSerializer;
import eu.ess.ics.ce.template.service.internal.audit.dto.AuditEvent;
import jakarta.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public record TypeHistoryEntry(
    @NotNull @JsonProperty("type_id") Long typeId,
    @NotNull @JsonProperty("type_name") String typeName,
    @JsonProperty("created_by") String createdBy,
    @NotNull @JsonProperty("type_project_id") Long typeProjectId,
    @NotNull @JsonProperty("configuration_project_id") Long configurationProjectId,
    @JsonProperty("updated_by") String updatedBy,
    @JsonProperty("updated_at") @JsonSerialize(using = ZonedDateTimeSerializer.class)
        ZonedDateTime updatedAt,
    AuditEvent event) {}
