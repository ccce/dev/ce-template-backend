/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.controller;

import com.google.gson.JsonSyntaxException;
import eu.ess.ics.ce.template.exceptions.*;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.rest.api.v1.IType;
import eu.ess.ics.ce.template.rest.model.git.response.GitProject;
import eu.ess.ics.ce.template.rest.model.helper.request.CalculateNewIocsRequest;
import eu.ess.ics.ce.template.rest.model.helper.response.CalculateNewIocsResponse;
import eu.ess.ics.ce.template.rest.model.template.request.CreateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.request.PreviewGenerationRequest;
import eu.ess.ics.ce.template.rest.model.template.request.UpdateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.response.*;
import eu.ess.ics.ce.template.rest.model.types.request.AttachInstanceRequest;
import eu.ess.ics.ce.template.rest.model.types.request.DeleteIocRequest;
import eu.ess.ics.ce.template.rest.model.types.request.IocFetchOption;
import eu.ess.ics.ce.template.rest.model.types.request.ProcessTypeRequest;
import eu.ess.ics.ce.template.rest.model.types.response.AttachInstanceResponse;
import eu.ess.ics.ce.template.rest.model.types.response.GeneratedIocNames;
import eu.ess.ics.ce.template.rest.model.types.response.IocPreview;
import eu.ess.ics.ce.template.rest.model.types.response.NameAndGitRepoWithMessage;
import eu.ess.ics.ce.template.rest.model.types.response.OperationStartedResponse;
import eu.ess.ics.ce.template.rest.model.types.response.PagedIocResponse;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.internal.iocinstance.IocInstanceService;
import eu.ess.ics.ce.template.service.internal.template.IocTypeService;
import java.util.List;
import org.gitlab4j.api.GitLabApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
public class TypeController implements IType {

  private static final Logger LOGGER = LoggerFactory.getLogger(TypeController.class);

  private final IocTypeService iocTypeService;
  private final IocInstanceService iocInstanceService;
  private final GitLabService gitLabService;

  public TypeController(
      IocTypeService iocTypeService,
      IocInstanceService iocInstanceService,
      GitLabService gitLabService) {
    this.iocTypeService = iocTypeService;
    this.iocInstanceService = iocInstanceService;
    this.gitLabService = gitLabService;
  }

  @Override
  public CreateTypeResponse createTypeAndConfiguration(
      OAuth2User user, Long exampleProjectId, CreateTypeRequest createRequest) {

    try {
      return iocTypeService.createTemplateAndConfiguration(
          createRequest, exampleProjectId, user.getName());
    } catch (InputValidationException e) {
      LOGGER.error("Input validation failed", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Gitlab error", e);
      throw new UnprocessableRequestException("Gitlab error", e.getMessage());
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unprocessable request", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to process template", e);
      throw new ServiceException("Error while trying to process template");
    }
  }

  public Type unarchiveType(OAuth2User user, long typeId) {
    try {
      return iocTypeService.unarchiveIocType(user, typeId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Template not found to restore", e);
      throw new EntityNotFoundException("Template", typeId);
    } catch (PreconditionFailedException e) {
      LOGGER.error("Ioc type is not deleted", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service while trying to unarchive template", e);
      throw convertGitlabApiException(e);
    } catch (Exception e) {
      LOGGER.error("Error while trying to restore template", e);
      throw new ServiceException("Error while trying to restore template");
    }
  }

  public Type archiveType(OAuth2User user, long typeId) {
    try {
      return iocTypeService.archiveIocType(user, typeId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Template not found to delete", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service while trying to archive template", e);
      throw convertGitlabApiException(e);
    } catch (Exception e) {
      LOGGER.error("Error while trying to delete template", e);
      throw new ServiceException("Error while trying to delete template");
    }
  }

  @Override
  public void deleteType(OAuth2User user, long typeId) {
    try {
      iocTypeService.deleteIocType(user, typeId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Type not found to delete", e);
      throw new EntityNotFoundException("Template", typeId);
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service while trying to delete template", e);
      throw convertGitlabApiException(e);
    } catch (Exception e) {
      LOGGER.error("Error while trying to delete template", e);
      throw new ServiceException("Error while trying to delete template");
    }
  }

  @Override
  public PagedTypeResponse listTypes(
      String createdBy,
      String query,
      boolean includeArchived,
      Integer page,
      Integer limit,
      boolean listAll) {
    try {
      return iocTypeService.findAll(createdBy, query, includeArchived, page, limit, listAll);
    } catch (Exception e) {
      LOGGER.error("Error while trying to list templates", e);
      throw new ServiceException("Error while trying to list templates");
    }
  }

  @Override
  public Type getType(long typeId) {
    try {
      return iocTypeService.getIocTypeById(typeId);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find template", e);
      throw new ServiceException("Error while trying to find template");
    }
  }

  @Override
  public OperationStartedResponse createIocRevision(
      long typeId, OAuth2User user, ProcessTypeRequest processRequest) {
    try {
      return iocInstanceService.processTemplate(typeId, processRequest, user.getName());
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Gitlab error", e);
      throw convertGitlabApiException(e);
    } catch (ConfigValidationException e) {
      LOGGER.error("Unprocessable template or config", e);
      throw e;
    } catch (InputValidationException e) {
      LOGGER.error("Unprocessable config", e);
      throw new UnprocessableRequestException("Unprocessable configuration file", e.getMessage());
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unprocessable request", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to process template", e);
      throw new ServiceException("Error while trying to process template");
    }
  }

  @Override
  public IocPreview previewGeneration(
      long typeId, PreviewGenerationRequest previewGenerationRequest) {
    try {
      return iocInstanceService.processTemplatePreview(
          typeId,
          previewGenerationRequest.typeRevision(),
          previewGenerationRequest.template_name(),
          previewGenerationRequest.configRevision(),
          previewGenerationRequest.configName());
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (FileRestrictionException e) {
      LOGGER.error("Preview generation is restricted to .mustache extensions", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Gitlab error", e);
      throw convertGitlabApiException(e);
    } catch (ConfigValidationException e) {
      LOGGER.error("Unprocessable template or config", e);
      throw e;
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unprocessable request", e);
      throw e;
    } catch (JsonSyntaxException e) {
      LOGGER.error("File cannot be parsed as it is not valid JSON", e);
      throw new PreconditionFailedException("File cannot be parsed as it is not valid JSON");
    } catch (Exception e) {
      LOGGER.error("Error while trying to process template", e);
      throw new ServiceException("Error while trying to process template");
    }
  }

  @Override
  public PagedIocResponse listIocs(
      IocFetchOption listScope,
      Long typeId,
      String user,
      String query,
      Integer page,
      Integer limit,
      boolean listAll) {
    try {
      return iocInstanceService.findAll(typeId, query, user, page, limit, listScope, listAll);
    } catch (Exception e) {
      LOGGER.error("Error while trying to list IOC", e);
      throw new ServiceException("Error while trying to list IOC");
    }
  }

  @Override
  public List<ConfigFile> getConfigFiles(long typeId, String typeRevision, String configRevision) {
    try {
      IocTypeEntity iocType = iocTypeService.findTemplateById(typeId, false);
      return iocInstanceService.getAndValidateConfigFiles(iocType, typeRevision, configRevision);
    } catch (ConfigListValidationException e) {
      LOGGER.error("Validation failed", e);
      throw e;
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service", e);
      throw convertGitlabApiException(e);
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unprocessable request", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find template", e);
      throw new ServiceException("Error while trying to find template");
    }
  }

  @Override
  public Type updateType(OAuth2User user, long typeId, UpdateTypeRequest request) {
    try {
      return iocTypeService.updateTemplate(
          user, typeId, request.typeNewName(), request.description());
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service", e);
      throw convertGitlabApiException(e);
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unprocessable request", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find template", e);
      throw new ServiceException("Error while trying to find template");
    }
  }

  @Override
  public TypeFilesResponse getTypeFileNames(long typeId, String typeRevision) {
    try {
      IocTypeEntity iocType = iocTypeService.findTemplateById(typeId, false);
      return iocInstanceService.getTemplateFileNames(iocType, typeRevision);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service", e);
      throw convertGitlabApiException(e);
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Unable to process template directory", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to list template files", e);
      throw new ServiceException("Error while trying to list template files");
    }
  }

  public List<TypeFile> getTemplateFiles(long templateId, String templateRevision) {
    try {
      IocTypeEntity iocType = iocTypeService.findTemplateById(templateId, false);
      return iocInstanceService.getTemplateFiles(iocType, templateRevision);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (GitLabApiException e) {
      LOGGER.error("Error in Gitlab service", e);
      throw convertGitlabApiException(e);
    } catch (Exception e) {
      LOGGER.error("Error while trying to find template", e);
      throw new ServiceException("Error while trying to find template");
    }
  }

  @Override
  public List<GitProject> listExampleProjects() {
    try {
      return gitLabService.listExampleProjects();
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to list example projects", e);
      throw new RemoteException("Gitlab Exception", "Error while trying to list example projects");
    } catch (Exception e) {
      LOGGER.error("Error while trying to list example projects", e);
      throw new ServiceException("Error while trying to list example projects");
    }
  }

  @Override
  public GeneratedIocNames listIocsMinimal(long typeId) {
    try {
      return iocInstanceService.listInstanceNames(typeId);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Type not found by ID: {} to fetch its IOCs", typeId, e);
      throw new EntityNotFoundException("IOC Type", typeId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to list generated IOC names", e);
      throw new ServiceException("Error while trying to list generated IOC names");
    }
  }

  @Override
  public List<NameAndGitRepoWithMessage> deleteTypeIocs(
      long typeId, boolean forceDelete, DeleteIocRequest request) {
    try {
      return iocInstanceService.deleteInstances(typeId, request, forceDelete);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("Type not found by ID: {} to delete it's IOC", typeId, e);
      throw new EntityNotFoundException("IOC Type", typeId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to delete IOCs", e);
      throw new ServiceException("Error while trying to delete IOCs");
    }
  }

  @Override
  public PagedTypeHistoryResponse getTypeHistory(long typeId, Integer page, Integer limit) {
    try {
      return iocTypeService.getIocTypeHistory(typeId, page, limit);
    } catch (EntityNotFoundException e) {
      LOGGER.error("Not found", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to find template", e);
      throw new ServiceException("Error while trying to find template");
    }
  }

  @Override
  public CalculateNewIocsResponse calculateNewIocs(
      OAuth2User user, Long typeId, CalculateNewIocsRequest configNames) {
    try {
      return iocInstanceService.calculateNewInstances(typeId, configNames);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("IOC type ID not found for new instance count calculation", e);
      throw new EntityNotFoundException("IOC type", typeId);
    } catch (UnprocessableRequestException e) {
      LOGGER.error("Config-name list empty for new instance calculation", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to calculate new instances", e);
      throw new ServiceException("Error while trying to calculate new instances");
    }
  }

  @Override
  public List<AttachInstanceResponse> attachInstances(
      OAuth2User user, long typeId, AttachInstanceRequest request) {
    try {
      return iocTypeService.attachInstances(typeId, request.attachInstances(), user.getName());
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error("IOC type not found by ID ({}) - cannot attach instance", typeId, e);
      throw new EntityNotFoundException("IOC type", typeId);
    } catch (Exception e) {
      LOGGER.error("Error while trying to attach instance", e);
      throw new ServiceException("Error while trying to attach instance");
    }
  }

  @Override
  public void moveInstance(OAuth2User user, long typeId, long instanceId) {
    try {
      iocInstanceService.moveInstance(typeId, instanceId, user.getName());
    } catch (EntityNotFoundException e) {
      LOGGER.error("IOC Type [{}] or Instance [{}] not found to move", typeId, instanceId);
      throw new EntityNotFoundException("IOC Type or Instance not found by ID to move");
    } catch (Exception e) {
      LOGGER.error("Error while trying to move instance", e);
      throw new ServiceException("Error while trying to move instance");
    }
  }

  private static CcceException convertGitlabApiException(GitLabApiException exception) {
    if (exception.getHttpStatus() == 404) {
      return new EntityNotFoundException(exception.getMessage());
    }

    return new RemoteException(exception.getReason(), exception.getMessage());
  }
}
