package eu.ess.ics.ce.template.service.internal.metrics;

import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.service.internal.jobs.JobsService;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MetricsService {

  private static final String METRIC_NAME_IOC_TYPE_COUNT = "ce.ioc.type.count";
  private static final String METRIC_NAME_IOC_COUNT = "ce.ioc.count";
  private static final String METRIC_NAME_JOB_COUNT = "ce.job.count";
  private static final String METRIC_NAME_IOC_TYPE_INSTANCE_COUNT = "ce.ioc.type.instance.count";

  private static final String METRIC_DESCRIPTION_IOC_TYPES = "Count of registered IOC Types";
  private static final String METRIC_DESCRIPTION_IOC_TYPE_INSTANCE_COUNT = "Count of IOCs per type";
  private static final String METRIC_DESCRIPTION_JOBS = "Count of jobs";
  private static final String METRIC_DESCRIPTION_IOC_INSTANCES =
      "Count of templated IOC instances.";

  private static final String KEY_TYPE = "type";
  private static final String KEY_JOB_TYPE = "job_type";
  private static final String KEY_STATUS = "status";
  private static final String TEMPLATED = "templated";

  private final ITemplateRepository templateRepository;
  private final IIocInstanceRepository instanceRepository;
  private final JobsService jobsService;
  private final MeterRegistry meterRegistry;

  // metrics whose parameters may change
  private final Map<String, AtomicLong> iocsPerType = new HashMap<>();

  public MetricsService(
      ITemplateRepository templateRepository,
      IIocInstanceRepository instanceRepository,
      JobsService jobsService,
      MeterRegistry meterRegistry) {
    this.templateRepository = templateRepository;
    this.instanceRepository = instanceRepository;
    this.jobsService = jobsService;
    this.meterRegistry = meterRegistry;
  }

  @PostConstruct
  private void registerGaugeMetrics() {
    Gauge.builder(METRIC_NAME_IOC_TYPE_COUNT, templateRepository::countAll)
        .description(METRIC_DESCRIPTION_IOC_TYPES)
        .register(meterRegistry);

    Gauge.builder(METRIC_NAME_IOC_COUNT, instanceRepository::countAllLatestInstances)
        .description(METRIC_DESCRIPTION_IOC_INSTANCES)
        .tag(KEY_TYPE, TEMPLATED)
        .register(meterRegistry);

    updateIocMetrics();
    createJobMetrics();
  }

  private void createJobMetrics() {
    for (JobType jobType : JobType.values()) {
      for (GenerationStatus status : GenerationStatus.values()) {
        Gauge.builder(METRIC_NAME_JOB_COUNT, () -> countJobs(jobType, status))
            .description(METRIC_DESCRIPTION_JOBS)
            .tag(KEY_JOB_TYPE, jobType.name().toLowerCase())
            .tag(KEY_STATUS, status.name().toLowerCase())
            .register(meterRegistry);
      }
    }
  }

  private long countJobs(JobType jobType, GenerationStatus status) {
    Map<JobType, Map<GenerationStatus, Long>> jobMetrics = jobsService.getJobMetrics();

    if (jobMetrics.containsKey(jobType)) {
      return jobMetrics.get(jobType).getOrDefault(status, 0L);
    }

    return 0L;
  }

  @Scheduled(fixedDelayString = "${gauge.update.interval.in.ms}")
  private void updateGauges() {
    updateIocMetrics();
  }

  private void updateIocMetrics() {
    List<IocTypeEntity> iocTypes = templateRepository.findAll(null, null, false, 0, null);
    for (IocTypeEntity iocType : iocTypes) {
      String typeName = iocType.getName();
      if (iocsPerType.containsKey(typeName)) {
        iocsPerType
            .get(typeName)
            .set(instanceRepository.countInstancesForPaging(iocType.getId(), null));
      } else {
        iocsPerType.put(
            typeName,
            new AtomicLong(instanceRepository.countLatestInstancesForIocType(iocType.getId())));
        Gauge.builder(METRIC_NAME_IOC_TYPE_INSTANCE_COUNT, () -> iocsPerType.get(typeName))
            .description(METRIC_DESCRIPTION_IOC_TYPE_INSTANCE_COUNT)
            .tag(KEY_TYPE, typeName)
            .register(meterRegistry);
      }
    }
  }
}
