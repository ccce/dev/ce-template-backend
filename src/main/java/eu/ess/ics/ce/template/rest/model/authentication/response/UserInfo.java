package eu.ess.ics.ce.template.rest.model.authentication.response;

import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class UserInfo extends BaseUserInfo {

  public UserInfo() {}

  public UserInfo(BaseUserInfo userInfo) {
    this.setLoginName(userInfo.getLoginName());
    this.setFullName(userInfo.getFullName());
    this.setAvatar(userInfo.getAvatar());
    this.setGitlabUserName(userInfo.getGitlabUserName());
    this.setEmail(userInfo.getEmail());
  }

  @NotNull private List<String> roles;

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }
}
