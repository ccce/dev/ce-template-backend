/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.api.v1;

import eu.ess.ics.ce.template.configuration.exception.GeneralException;
import eu.ess.ics.ce.template.rest.model.jobs.response.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/v1/jobs")
@Tag(name = "Jobs")
public interface IJobs {
  @Operation(summary = "List Jobs")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of Jobs",
            content = @Content(schema = @Schema(implementation = PagedJobResponse.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedJobResponse listJobs(
      @Parameter(description = "Search text (Search for IOC Type name, Git tag, User)")
          @RequestParam(required = false)
          String query,
      @Parameter(description = "IOC Type ID") @RequestParam(required = false, name = "ioc_type_id")
          Long iocTypeId,
      @Parameter(description = "User") @RequestParam(required = false) String user,
      @Parameter(description = "Page offset - starts with 0") @RequestParam(required = false)
          Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

  @Operation(summary = "Job Detail by ID")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Job not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job details",
            content = @Content(schema = @Schema(implementation = JobDetails.class)))
      })
  @GetMapping(
      produces = {"application/json"},
      value = "/{job_id}/details")
  JobDetails jobDetails(
      @Parameter(description = "The Job ID for which the details should be fetched")
          @PathVariable(name = "job_id")
          Long jobId);

  @Operation(summary = "Job Instances")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Job not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job Instances",
            content = @Content(schema = @Schema(implementation = JobInstanceListResponse.class)))
      })
  @GetMapping(
      produces = {"application/json"},
      value = "/{job_id}/iocs")
  JobInstanceListResponse jobInstances(
      @Parameter(description = "The Job ID for which the instances should be fetched")
          @PathVariable(name = "job_id")
          Long jobId,
      @Parameter(description = "Page offset - starts with 0") @RequestParam(required = false)
          Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

  @Operation(summary = "Get Job status")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Job not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job status",
            content = @Content(schema = @Schema(implementation = JobStatusResponse.class)))
      })
  @GetMapping(
      produces = {"application/json"},
      value = "/{job_id}/status")
  JobStatusResponse jobStatus(
      @Parameter(description = "The Job ID for which the status should be fetched")
          @PathVariable(name = "job_id")
          final Long jobId);

  @Operation(summary = "Job instance log")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Instance not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Job instance log",
            content = @Content(schema = @Schema(implementation = JobInstanceLogResponse.class)))
      })
  @GetMapping(
      produces = {"application/json"},
      value = "/{job_id}/iocs/{ioc_id}/log")
  JobInstanceLogResponse getInstanceLog(
      @Parameter(description = "The Job ID for the IOC Instance that should be fetched")
          @PathVariable(name = "job_id")
          final Long operationId,
      @Parameter(description = "The IOC instance ID for which the log should be fetched")
          @PathVariable(name = "ioc_id")
          final Long instanceId);
}
