package eu.ess.ics.ce.template.common.conversion;

import eu.ess.ics.ce.template.service.internal.PagingLimitDto;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Calculation of paging parameters
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class PagingUtil {

  private static final String LIST_LIMIT = "response.list.max.size";

  private final int responseListMaxSize;

  /**
   * Constructor
   *
   * @param env Environment variables representation
   */
  public PagingUtil(Environment env) {
    this.responseListMaxSize = Integer.parseInt(env.getProperty(LIST_LIMIT));
  }

  /**
   * Limit maximum page size according to CE backend configuration
   *
   * @param page Desired page
   * @param limit Desired page size
   * @return Calculated paging parameters (limited if needed)
   */
  public PagingLimitDto pageLimitConverter(Integer page, Integer limit) {
    return pageLimitConverter(page, limit, null);
  }

  public PagingLimitDto pageLimitConverter(Integer page, Integer limit, boolean listAll) {
    if (listAll) {
      return new PagingLimitDto(0, null);
    }
    return pageLimitConverter(page, limit);
  }

  /**
   * Limit maximum page size according to an external service API's configuration
   *
   * @param page Desired page
   * @param limit Desired page size
   * @param customPageLimit External service API's page size limit
   * @return Calculated paging parameters (limited if needed)
   */
  public PagingLimitDto pageLimitConverter(Integer page, Integer limit, Integer customPageLimit) {

    int maxLimitSize = customPageLimit != null ? customPageLimit : responseListMaxSize;

    int pageSize = page == null ? 0 : page;
    int limitSize = limit == null ? maxLimitSize : Math.min(limit, maxLimitSize);

    return new PagingLimitDto(pageSize, limitSize);
  }
}
