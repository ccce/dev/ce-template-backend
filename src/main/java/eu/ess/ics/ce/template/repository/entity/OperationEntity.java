/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Entity
@Table(name = "operation")
public class OperationEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "ioc_type_id")
  private IocTypeEntity iocType;

  @Column(name = "created_at")
  private ZonedDateTime createdAt;

  @Column(name = "ioc_type_name")
  private String typeName;

  @Column(name = "git_tag")
  private String gitTag;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "template_revision")
  private String templateRevision;

  @Column(name = "configuration_revision")
  private String configurationRevision;

  @Column(name = "finished_at")
  private ZonedDateTime finishedAt;

  @Column(name = "job_type")
  private String jobType;

  @Column(name = "version")
  private Long version;

  @ManyToOne
  @JoinColumn(name = "predecessor_ioc_type_id")
  private IocTypeEntity predecessorType;

  @Column(name = "predecessor_ioc_type_name")
  private String predecessorTypeName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public IocTypeEntity getIocType() {
    return iocType;
  }

  public void setIocType(IocTypeEntity iocType) {
    this.iocType = iocType;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  public String getGitTag() {
    return gitTag;
  }

  public void setGitTag(String gitTag) {
    this.gitTag = gitTag;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public ZonedDateTime getFinishedAt() {
    return finishedAt;
  }

  public void setFinishedAt(ZonedDateTime finishedAt) {
    this.finishedAt = finishedAt;
  }

  public String getTemplateRevision() {
    return templateRevision;
  }

  public void setTemplateRevision(String templateRevision) {
    this.templateRevision = templateRevision;
  }

  public String getConfigurationRevision() {
    return configurationRevision;
  }

  public void setConfigurationRevision(String configurationRevision) {
    this.configurationRevision = configurationRevision;
  }

  public String getJobType() {
    return jobType;
  }

  public void setJobType(String jobType) {
    this.jobType = jobType;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public IocTypeEntity getPredecessorType() {
    return predecessorType;
  }

  public String getPredecessorTypeName() {
    return predecessorTypeName;
  }

  public void setPredecessorType(IocTypeEntity predecessorType) {
    this.predecessorType = predecessorType;
  }

  public void setPredecessorTypeName(String predecessorTypeName) {
    this.predecessorTypeName = predecessorTypeName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OperationEntity that = (OperationEntity) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "OperationEntity{"
        + "id="
        + id
        + ", iocType="
        + iocType
        + ", createdAt="
        + createdAt
        + ", typeName='"
        + typeName
        + '\''
        + ", gitTag='"
        + gitTag
        + '\''
        + ", createdBy='"
        + createdBy
        + '\''
        + ", templateRevision='"
        + templateRevision
        + '\''
        + ", configurationRevision='"
        + configurationRevision
        + '\''
        + ", finishedAt="
        + finishedAt
        + ", jobType='"
        + jobType
        + '\''
        + ", version="
        + version
        + ", predecessorType="
        + predecessorType
        + ", predecessorTypeName="
        + predecessorTypeName
        + '}';
  }
}
