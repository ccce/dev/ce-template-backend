/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.controller;

import eu.ess.ics.ce.template.common.Literals;
import eu.ess.ics.ce.template.exceptions.FileAccessDeniedException;
import eu.ess.ics.ce.template.exceptions.GitRepoNotFoundException;
import eu.ess.ics.ce.template.exceptions.RemoteException;
import eu.ess.ics.ce.template.exceptions.ServiceException;
import eu.ess.ics.ce.template.rest.api.v1.IGitIntegration;
import eu.ess.ics.ce.template.rest.model.git.response.FileContentResponse;
import eu.ess.ics.ce.template.rest.model.git.response.GitProject;
import eu.ess.ics.ce.template.rest.model.git.response.GitReference;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestController
public class GitIntegrationController implements IGitIntegration {

  private static final Logger LOGGER = LoggerFactory.getLogger(GitIntegrationController.class);

  private final GitLabService gitLabService;

  public GitIntegrationController(GitLabService gitLabService) {
    this.gitLabService = gitLabService;
  }

  @Override
  public List<GitProject> listTypeProjects() {
    try {
      return gitLabService.listTemplateProjects();
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to list template projects", e);
      throw new RemoteException("Gitlab Exception", "Error while trying to list template projects");
    } catch (Exception e) {
      LOGGER.error("Error while trying to list template projects", e);
      throw new ServiceException("Error while trying to list template projects");
    }
  }

  @Override
  public List<GitProject> listConfigProjects() {
    try {
      return gitLabService.listConfigProjects();
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to list config projects", e);
      throw new RemoteException("Gitlab Exception", "Error while trying to list config projects");
    } catch (Exception e) {
      LOGGER.error("Error while trying to list config projects", e);
      throw new ServiceException("Error while trying to list config projects");
    }
  }

  @Override
  public List<GitReference> listTagsAndCommitIds(long projectId, String reference) {
    try {
      return gitLabService.tagsAndCommits(projectId, reference);
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to list tags and commits for a projects", e);

      if (e.getHttpStatus() == 404) {
        throw new GitRepoNotFoundException("Git project not found!");
      }

      throw new RemoteException(
          "Gitlab Exception", "Error while trying to list tags and commits for a projects");
    } catch (Exception e) {
      LOGGER.error("Error while trying to list tags and commits for a projects", e);
      throw new ServiceException("Error while trying to list tags and commits for a projects");
    }
  }

  @Override
  public List<String> listProjectFiles(long projectId, String reference) {
    try {
      return gitLabService.listFilesInRepo(projectId, reference);
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to list files for specific a projects", e);

      if (e.getHttpStatus() == 404) {
        throw new GitRepoNotFoundException("Git project not found!");
      }

      throw new RemoteException(
          "Gitlab Exception", "Error while trying to list files for a projects");
    } catch (Exception e) {
      LOGGER.error("Error while trying to list files for a projects", e);
      throw new ServiceException("Error while trying to list files for a projects");
    }
  }

  @Override
  public FileContentResponse fetchFileContent(long projectId, String projectRef, String fileName) {
    try {
      return new FileContentResponse(getProjectFileContent(projectId, projectRef, fileName));
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to get file content", e);

      if (e.getHttpStatus() == 404) {
        throw new GitRepoNotFoundException("Git project/file not found!");
      }

      throw new RemoteException("GitLab Exception", "Error while trying to get file content");
    } catch (GitRepoNotFoundException e) {
      LOGGER.error("Git repo/file not found while trying to fetch file content", e);
      throw e;
    } catch (FileAccessDeniedException e) {
      LOGGER.error("Trying to get file content that is not in config/template group", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to get file content", e);
      throw new ServiceException("Error while trying to get file content");
    }
  }

  @Override
  public GitProject getProjectInformation(final long projectId) {
    try {
      return gitLabService.getGitProject(projectId);
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to get project information", e);

      if (e.getHttpStatus() == 404) {
        throw new GitRepoNotFoundException("Git project not found!");
      }

      throw new RemoteException(
          "Gitlab Exception", "Error while trying to get project information");
    } catch (Exception e) {
      LOGGER.error("Error while trying to get project information", e);
      throw new ServiceException("Error while trying to get project information");
    }
  }

  public String getProjectFileContent(long projectId, String reference, String fileName)
      throws GitLabApiException {
    // project is a template project -> respond with template file content
    Project project = gitLabService.checkTemplateExists(projectId);
    if (project != null) {
      String ref = reference;
      // if no reference given -> use default branch
      if (StringUtils.isEmpty(ref)) {
        ref = project.getDefaultBranch();
      }

      // check if fileName is empty
      String fName = fileName;
      if (StringUtils.isEmpty(fName)) {
        fName = Literals.ST_CMD_TEMPLATE_NAME;
      }

      // getting back content
      return gitLabService.getFile(projectId, fName, ref).content();
    }

    // project is a config project
    project = gitLabService.checkConfigExists(projectId);
    if (project != null) {
      String ref = reference;
      // if no reference given -> use default branch
      if (StringUtils.isEmpty(ref)) {
        ref = project.getDefaultBranch();
      }

      return gitLabService.getFile(projectId, fileName, ref).content();
    }

    // the project is not a template-, nor config project -> can not be accessed via this tool
    throw new FileAccessDeniedException(
        "File access denied",
        "File couldn't be accessed, only template or config projects are allowed to read");
  }
}
