package eu.ess.ics.ce.template.rest.model.template.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PreviewGenerationRequest(
    @JsonProperty("type_revision") String typeRevision,
    @JsonProperty("template_name") String template_name,
    @JsonProperty("config_revision") String configRevision,
    @JsonProperty("config_name") String configName) {}
