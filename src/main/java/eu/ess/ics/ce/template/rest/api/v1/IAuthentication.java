package eu.ess.ics.ce.template.rest.api.v1;

import eu.ess.ics.ce.template.configuration.exception.GeneralException;
import eu.ess.ics.ce.template.rest.model.authentication.response.UserInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/authentication")
@Tag(name = "Authentication")
public interface IAuthentication {

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = UserInfo.class))),
    @ApiResponse(
        responseCode = "401",
        description = "Unauthorized",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "403",
        description = "Permission denied",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "404",
        description = "User not found in Gitlab",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "503",
        description = "Login server unavailable",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Get user information",
      description = "Get user information from authorization service",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(
      value = "/user",
      produces = {"application/json"})
  UserInfo userInfo(@AuthenticationPrincipal OAuth2User user);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = String.class))),
    @ApiResponse(
        responseCode = "403",
        description = "Permission denied",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Forward the request to Oauth2 login page",
      description =
          "If the user is not logged in it forwards the request to the login page, or opens up the default FE page",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(
      value = "/oauthlogin",
      produces = {"application/json"})
  ResponseEntity<Void> oauthLogin(
      HttpServletRequest request,
      HttpServletResponse response,
      @Parameter(description = "Redirect url after successful login")
          @RequestParam(required = false, name = "redirect_url")
          String redirectUrl);

  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = String.class))),
    @ApiResponse(
        responseCode = "401",
        description = "Unauthorized",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "403",
        description = "Permission denied",
        content = @Content(schema = @Schema(implementation = GeneralException.class))),
    @ApiResponse(
        responseCode = "500",
        description = "Service exception",
        content = @Content(schema = @Schema(implementation = GeneralException.class)))
  })
  @Operation(
      summary = "Logs out user from Oauth2 server",
      description =
          "Logs out the user from Oauth2 server and forwards the browser to the URL that is given in the parameter",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @GetMapping(
      value = "/oauthlogout",
      produces = {"application/json"})
  ResponseEntity<Void> oauthLogout(
      HttpServletRequest request,
      HttpServletResponse response,
      @Parameter(description = "Redirect url after successful logout")
          @RequestParam(required = false, name = "redirect_url")
          String redirectUrl);
}
