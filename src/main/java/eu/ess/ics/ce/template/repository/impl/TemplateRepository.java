package eu.ess.ics.ce.template.repository.impl;

import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Repository
public class TemplateRepository implements ITemplateRepository {
  @PersistenceContext private EntityManager em;

  @Override
  public List<IocTypeEntity> findAll(
      String createdBy, String query, boolean includeDeleted, int page, Integer limit) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<IocTypeEntity> cq = cb.createQuery(IocTypeEntity.class);
    Root<IocTypeEntity> from = cq.from(IocTypeEntity.class);
    CriteriaQuery<IocTypeEntity> select = cq.select(from);

    // Default order
    cq.orderBy(cb.asc(from.get("name")));

    TypedQuery<IocTypeEntity> q =
        pagingQuery(cb, select, from, cq, createdBy, query, includeDeleted);

    if (limit != null) {
      q.setFirstResult(page * limit);
      q.setMaxResults(limit);
    }

    return q.getResultList();
  }

  @Override
  public IocTypeEntity findById(Long id) {
    return em.createQuery("SELECT t FROM IocTypeEntity t WHERE t.id =:pId", IocTypeEntity.class)
        .setParameter("pId", id)
        .getSingleResult();
  }

  @Override
  public void createIocType(IocTypeEntity iocType) {
    em.persist(iocType);
  }

  @Override
  public void updateIocType(IocTypeEntity iocType) {
    em.merge(iocType);
  }

  @Override
  public long countForPaging(String createdBy, String query, boolean includeDeleted) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<Long> cq = cb.createQuery(Long.class);
    Root<IocTypeEntity> from = cq.from(IocTypeEntity.class);
    Expression<Long> count1 = cb.count(from);
    CriteriaQuery<Long> select = cq.select(count1);

    return pagingQuery(cb, select, from, cq, createdBy, query, includeDeleted).getSingleResult();
  }

  @Override
  public void deleteIocTypeAndConfigById(long iocTypeId) {
    em.createQuery("DELETE FROM IocTypeEntity WHERE id = :pIocTypeId")
        .setParameter("pIocTypeId", iocTypeId)
        .executeUpdate();
  }

  @Override
  public Long countAll() {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocTypeEntity t WHERE t.deleted = false ", Long.class)
        .getSingleResult();
  }

  private <T, R> TypedQuery<T> pagingQuery(
      CriteriaBuilder cb,
      CriteriaQuery<T> select,
      Root<R> from,
      CriteriaQuery<T> cq,
      String createdBy,
      String query,
      boolean includeDeleted) {
    List<Predicate> predicates = new ArrayList<>();

    if (createdBy != null) {

      Predicate createdByPredicate =
          cb.like(cb.lower(from.get("createdBy")), "%" + createdBy.toLowerCase() + "%");
      predicates.add(createdByPredicate);
    }

    if (query != null) {
      predicates.add(cb.or(cb.like(cb.lower(from.get("name")), "%" + query.toLowerCase() + "%")));
    }

    if (!includeDeleted) {
      predicates.add(cb.and(cb.isFalse(from.get("deleted"))));
    }

    select.where(cb.and(predicates.toArray(new Predicate[0])));
    return em.createQuery(cq);
  }
}
