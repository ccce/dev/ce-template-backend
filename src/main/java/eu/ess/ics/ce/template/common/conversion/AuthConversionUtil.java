package eu.ess.ics.ce.template.common.conversion;

import eu.ess.ics.ce.template.common.Constants;
import eu.ess.ics.ce.template.configuration.WebSecurityConfig;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class AuthConversionUtil {
  private AuthConversionUtil() {}

  public static <T> List<String> convertAuthorities(T authority) {
    List<String> result = new ArrayList<>();

    if (authority instanceof List<?>) {
      for (String auth : (List<String>) authority) {
        String authString = convertAuthority(auth);
        if (authString != null) {
          result.add(authString);
        }
      }
    } else {
      if (authority instanceof String) {
        String authString = convertAuthority(authority.toString());
        if (authString != null) {
          result.add(authString);
        }
      }
    }

    return result;
  }

  private static String convertAuthority(String authority) {
    if (Constants.PING_MEMBER_ADMIN.equalsIgnoreCase(authority)) {
      return WebSecurityConfig.ROLE_TEMPLATING_TOOL_ADMIN;
    }

    if (Constants.PING_MEMBER_INTEGRATOR.equalsIgnoreCase(authority)) {
      return WebSecurityConfig.ROLE_TEMPLATING_TOOL_INTEGRATOR;
    }

    return null;
  }
}
