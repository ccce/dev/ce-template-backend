package eu.ess.ics.ce.template.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class ConfigValidationException extends CcceException {

  public ConfigValidationException(String error, String description) {
    super(error, description);
  }
}
