package eu.ess.ics.ce.template.rest.model.jobs.response;

public enum InstanceGenerationStep {
  CREATE_GIT_PROJECT,
  PROCESSING_TEMPLATE_AND_COMMIT,
  ATTACH_INSTANCE,
  MOVE_INSTANCE
}
