package eu.ess.ics.ce.template.common;

import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceGenerationStep;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public interface Constants {

  String PING_FULL_NAME = "fullname";
  String PING_EMAIL = "email";
  String PING_MEMBEROF = "memberof";
  String PING_MEMBER_ADMIN = "ess_service_ics_ce_template_admin";
  String PING_MEMBER_INTEGRATOR = "ess_service_ics_ce_template_user";
  String OAUTH_LOGOUT_URL = "/api/v1/authentication/logout";

  List<String> QUEUED_STATUSES =
      List.of(GenerationStatus.QUEUED.toString(), GenerationStatus.RUNNING.toString());

  List<String> FINISHED_STATUSES =
      List.of(InstanceStatus.SUCCESSFUL.toString(), InstanceStatus.FAILED.toString());

  Map<InstanceStatus, InstanceGenerationStep> INSTANCE_STATUS_TRANSITION =
      Map.of(
          InstanceStatus.GIT_PROJECT_CREATING,
          InstanceGenerationStep.CREATE_GIT_PROJECT,
          InstanceStatus.GIT_TAGGING_AND_COMMITING,
          InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT);
}
