package eu.ess.ics.ce.template.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Entity
@Table(name = "ioc_instance_log")
public class IocInstanceLogEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "ioc_instance_id")
  private IocInstanceEntity iocInstance;

  @Column(name = "description")
  private String description;

  @Column(name = "created_at")
  private ZonedDateTime createdAt;

  @Column(name = "type")
  private String type;

  @Column(name = "status")
  private String status;

  @ManyToOne
  @JoinColumn(name = "operation_id")
  private OperationEntity operation;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "finished_at")
  private ZonedDateTime finishedAt;

  @Column(name = "version")
  private Long version;

  @Column(name = "ioc_type_name")
  private String typeName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public IocInstanceEntity getIocInstance() {
    return iocInstance;
  }

  public void setIocInstance(IocInstanceEntity iocInstance) {
    this.iocInstance = iocInstance;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public OperationEntity getOperation() {
    return operation;
  }

  public void setOperation(OperationEntity operation) {
    this.operation = operation;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public ZonedDateTime getFinishedAt() {
    return finishedAt;
  }

  public void setFinishedAt(ZonedDateTime finishedAt) {
    this.finishedAt = finishedAt;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    IocInstanceLogEntity iocInstanceLogEntity = (IocInstanceLogEntity) o;
    return Objects.equals(id, iocInstanceLogEntity.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "LogEntity{"
        + "id="
        + id
        + ", description='"
        + description
        + '\''
        + ", createdAt="
        + createdAt
        + ", type='"
        + type
        + '\''
        + ", status='"
        + status
        + '\''
        + '}';
  }
}
