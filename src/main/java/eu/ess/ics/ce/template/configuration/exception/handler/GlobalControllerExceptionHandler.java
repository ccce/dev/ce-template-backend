/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.configuration.exception.handler;

import eu.ess.ics.ce.template.configuration.exception.GeneralException;
import eu.ess.ics.ce.template.configuration.exception.InvalidConfigException;
import eu.ess.ics.ce.template.exceptions.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler
  public ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {

    GeneralException generalException = new GeneralException();
    if (ex instanceof CcceException ccceException) {
      generalException.setError(ccceException.getError());
      generalException.setDescription(ccceException.getDescription());
    } else {
      generalException.setError("Generic error");
      generalException.setDescription(ex.getMessage());
    }

    HttpStatus resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    // your exception comes here
    if (ex instanceof UnauthorizedException | ex instanceof AccessDeniedException) {
      resultStatus = HttpStatus.FORBIDDEN;
    }

    if (ex instanceof AuthenticationException) {
      resultStatus = HttpStatus.UNAUTHORIZED;
    }

    if (ex instanceof ConfigListValidationException exception) {
      resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
      return new ResponseEntity<>(new InvalidConfigException(exception), resultStatus);
    }

    if (ex instanceof ConfigValidationException || ex instanceof UnprocessableRequestException) {
      resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
    }

    if (ex instanceof RemoteException) {
      resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
    }

    if (ex instanceof PreconditionFailedException) {
      resultStatus = HttpStatus.PRECONDITION_FAILED;
    }

    if (ex instanceof GitRepoNotFoundException || ex instanceof EntityNotFoundException) {
      resultStatus = HttpStatus.NOT_FOUND;
    }

    if (ex instanceof FileAccessDeniedException) {
      resultStatus = HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS;
    }

    if (ex instanceof InputValidationException || ex instanceof FileRestrictionException) {
      resultStatus = HttpStatus.BAD_REQUEST;
    }

    if (ex instanceof ResourceCanNotBeDeletedException) {
      resultStatus = HttpStatus.LOCKED;
    }

    return new ResponseEntity<>(generalException, resultStatus);
  }

  @NotNull
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatusCode status,
      WebRequest request) {
    GeneralException generalException = new GeneralException();
    generalException.setError("Method argument not valid");
    generalException.setDescription(ex.getMessage());
    return new ResponseEntity<>(generalException, status);
  }

  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex, WebRequest request) {

    // queryparam transformation error should respond with 400
    // pathparam transformation error should respond with 404
    HttpStatus status =
        ex.getParameter().getParameterAnnotation(PathVariable.class) != null
            ? HttpStatus.NOT_FOUND
            : HttpStatus.BAD_REQUEST;

    GeneralException generalException = new GeneralException();
    generalException.setError("Request type problem");
    generalException.setDescription("Requested resource cannot be found");

    return new ResponseEntity<Object>(generalException, new HttpHeaders(), status);
  }
}
