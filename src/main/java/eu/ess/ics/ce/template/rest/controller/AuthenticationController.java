package eu.ess.ics.ce.template.rest.controller;

import eu.ess.ics.ce.template.common.Constants;
import eu.ess.ics.ce.template.common.conversion.AuthConversionUtil;
import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.RemoteException;
import eu.ess.ics.ce.template.exceptions.ServiceException;
import eu.ess.ics.ce.template.exceptions.UnauthorizedException;
import eu.ess.ics.ce.template.rest.api.v1.IAuthentication;
import eu.ess.ics.ce.template.rest.model.authentication.response.UserInfo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.net.URI;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
public class AuthenticationController implements IAuthentication {
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

  @Value("${ui.baseurl}")
  private String uiBaseUrl;

  @Override
  public UserInfo userInfo(OAuth2User user) {
    try {
      UserInfo result = new UserInfo();

      result.setEmail(user.getAttribute(Constants.PING_EMAIL));
      result.setLoginName(user.getName());
      result.setFullName(user.getAttribute(Constants.PING_FULL_NAME));
      result.setRoles(
          AuthConversionUtil.convertAuthorities(user.getAttribute(Constants.PING_MEMBEROF)));

      return result;
    } catch (UnauthorizedException e) {
      LOGGER.error("Authorization error while trying to get user roles");
      throw e;
    } catch (EntityNotFoundException e) {
      LOGGER.error("Error while trying to get Gitlab user", e);
      throw e;
    } catch (RemoteException e) {
      LOGGER.error("Error while trying to get user roles from RBAC", e);
      throw e;
    } catch (Exception e) {
      LOGGER.error("Error while trying to retrieve user roles", e);
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public ResponseEntity<Void> oauthLogin(
      HttpServletRequest request, HttpServletResponse response, String redirectUrl) {

    String redirectTo = StringUtils.isEmpty(redirectUrl) ? uiBaseUrl : redirectUrl;

    return ResponseEntity.status(302).location(URI.create(redirectTo)).build();
  }

  @Override
  public ResponseEntity<Void> oauthLogout(
      HttpServletRequest request, HttpServletResponse response, String redirectUrl) {

    String redirectTo = Constants.OAUTH_LOGOUT_URL;
    if (StringUtils.isNotEmpty(request.getQueryString())) {
      redirectTo += "?" + request.getQueryString();
    }

    return ResponseEntity.status(302).location(URI.create(redirectTo)).build();
  }
}
