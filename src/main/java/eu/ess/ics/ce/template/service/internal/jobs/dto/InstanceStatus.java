package eu.ess.ics.ce.template.service.internal.jobs.dto;

public enum InstanceStatus {
  QUEUED("IOC has been added to the queue"),
  STARTED("Started processing IOC"),
  GIT_PROJECT_CREATING("Git repository creating"),
  GIT_PROJECT_CREATED("Git repository has been created"),
  GIT_PROJECT_FETCHING("Git repository fetching"),
  GIT_PROJECT_FETCHED("Git repository has been fetched"),
  GIT_TAGGING_AND_COMMITING("Starting to commit and then tag repo"),
  GIT_TAGGGED_AND_COMMITED("Git repository has been tagged and commited"),
  SUCCESSFUL("IOC has been processed successfully"),
  FAILED("Failed to process IOC"),
  ;

  private final String message;

  InstanceStatus(final String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
