package eu.ess.ics.ce.template.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Entity
@Table(name = "ioc_instance")
public class IocInstanceEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "ioc_type_id")
  private IocTypeEntity iocType;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "created_at")
  private ZonedDateTime createdAt;

  @Column(name = "git_project_id")
  private Long gitProjectId;

  @Column(name = "git_revision")
  private String gitRevision;

  @Column(name = "git_tag")
  private String gitTag;

  @Column(name = "configuration_name")
  private String configurationName;

  @Column(name = "version")
  private Long version;

  @Column(name = "ioc_type_name")
  private String iocTypeName;

  @Column(name = "status")
  private String status;

  @Column(name = "finished_at")
  private ZonedDateTime finishedAt;

  @Column(name = "instance_name")
  private String instanceName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public void setGitProjectId(Long gitProjectId) {
    this.gitProjectId = gitProjectId;
  }

  public String getGitRevision() {
    return gitRevision;
  }

  public void setGitRevision(String gitRevision) {
    this.gitRevision = gitRevision;
  }

  public String getGitTag() {
    return gitTag;
  }

  public void setGitTag(String gitTag) {
    this.gitTag = gitTag;
  }

  public String getConfigurationName() {
    return configurationName;
  }

  public void setConfigurationName(String configurationName) {
    this.configurationName = configurationName;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public String getIocTypeName() {
    return iocTypeName;
  }

  public void setIocTypeName(String iocTypeName) {
    this.iocTypeName = iocTypeName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public ZonedDateTime getFinishedAt() {
    return finishedAt;
  }

  public void setFinishedAt(ZonedDateTime finishedAt) {
    this.finishedAt = ZonedDateTime.of(finishedAt.toLocalDateTime(), finishedAt.getZone());
  }

  public IocTypeEntity getIocType() {
    return iocType;
  }

  public void setIocType(IocTypeEntity iocType) {
    this.iocType = iocType;
  }

  public String getInstanceName() {
    return instanceName;
  }

  public void setInstanceName(String instanceName) {
    this.instanceName = instanceName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    IocInstanceEntity that = (IocInstanceEntity) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "IocInstanceEntity{"
        + "id="
        + id
        + ", iocType="
        + iocType
        + ", createdBy='"
        + createdBy
        + '\''
        + ", createdAt="
        + createdAt
        + ", gitProjectId="
        + gitProjectId
        + ", gitRevision='"
        + gitRevision
        + '\''
        + ", gitTag='"
        + gitTag
        + '\''
        + ", configurationName='"
        + configurationName
        + '\''
        + ", version="
        + version
        + ", iocTypeName='"
        + iocTypeName
        + '\''
        + ", status='"
        + status
        + '\''
        + ", finishedAt="
        + finishedAt
        + ", instanceName='"
        + instanceName
        + '\''
        + '}';
  }
}
