/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.api.v1;

import eu.ess.ics.ce.template.configuration.WebSecurityConfig;
import eu.ess.ics.ce.template.configuration.exception.GeneralException;
import eu.ess.ics.ce.template.rest.model.git.response.GitProject;
import eu.ess.ics.ce.template.rest.model.helper.request.CalculateNewIocsRequest;
import eu.ess.ics.ce.template.rest.model.helper.response.CalculateNewIocsResponse;
import eu.ess.ics.ce.template.rest.model.template.request.CreateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.request.PreviewGenerationRequest;
import eu.ess.ics.ce.template.rest.model.template.request.UpdateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.response.*;
import eu.ess.ics.ce.template.rest.model.types.request.AttachInstanceRequest;
import eu.ess.ics.ce.template.rest.model.types.request.DeleteIocRequest;
import eu.ess.ics.ce.template.rest.model.types.request.IocFetchOption;
import eu.ess.ics.ce.template.rest.model.types.request.ProcessTypeRequest;
import eu.ess.ics.ce.template.rest.model.types.response.AttachInstanceResponse;
import eu.ess.ics.ce.template.rest.model.types.response.GeneratedIocNames;
import eu.ess.ics.ce.template.rest.model.types.response.IocPreview;
import eu.ess.ics.ce.template.rest.model.types.response.NameAndGitRepoWithMessage;
import eu.ess.ics.ce.template.rest.model.types.response.OperationStartedResponse;
import eu.ess.ics.ce.template.rest.model.types.response.PagedIocResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/types")
@Tag(name = "Types")
public interface IType {
  @Operation(
      summary = "Create new type and configuration projects",
      description =
          """
              Create new type and configuration projects.

              Disclaimer: if you want to create project from example, you don't have to fill the IOCs field in the request, that field will be ignored!
              """,
      security = {@SecurityRequirement(name = "bearerAuth")},
      requestBody =
          @io.swagger.v3.oas.annotations.parameters.RequestBody(
              content =
                  @Content(
                      schema = @Schema(implementation = CreateTypeRequest.class),
                      examples =
                          @ExampleObject(
                              value =
                                  "{"
                                      + "\"type_name\": \"abc-cde-123\","
                                      + "\"description\": \"This is a test IOC Type\","
                                      + "\"iocs\": [ \"010\", \"020\", \"030\" ]"
                                      + "}"))))
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Type and/or config already exist",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Permission denied",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid type name/invalid example project",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "201",
            description = "Type created",
            content = @Content(schema = @Schema(implementation = CreateTypeResponse.class)))
      })
  @PostMapping(produces = {"application/json"})
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  CreateTypeResponse createTypeAndConfiguration(
      @AuthenticationPrincipal OAuth2User user,
      @RequestParam(name = "example_project_id", required = false) Long exampleProjectId,
      @RequestBody CreateTypeRequest createRequest);

  @Operation(
      summary = "Unarchive IOC type",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Type is successfully unarchived",
            content = @Content(schema = @Schema(implementation = Type.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "412",
            description = "Type is not archived",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Type or config not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @PostMapping(
      value = "/{type_id}/unarchive",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @ResponseStatus(HttpStatus.OK)
  Type unarchiveType(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(in = ParameterIn.PATH, description = "Id of a Type to unarchive", required = true)
          @PathVariable("type_id")
          long typeId);

  @Operation(
      summary = "Archive IOC type",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Type is successfully archived",
            content = @Content(schema = @Schema(implementation = Type.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Type or config not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @DeleteMapping(
      value = "/{type_id}/archive",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @ResponseStatus(HttpStatus.OK)
  Type archiveType(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(in = ParameterIn.PATH, description = "Id of a Type to archive", required = true)
          @PathVariable("type_id")
          long typeId);

  @Operation(
      summary = "Deletes a Type and its corresponding Config based on the ID supplied",
      security = {@SecurityRequirement(name = "bearerAuth")},
      description =
          """
              Deletes a Type, and its corresponding Config from the DB and from GitLab.
              Also deletes those IOC instances that were created from the Type.
          """)
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Type and config are deleted"),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Forbidden: User doesn't have the necessary permissions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Type or config not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class)))
      })
  @DeleteMapping(
      value = "/{type_id}",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void deleteType(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(in = ParameterIn.PATH, description = "Id of a Type to delete", required = true)
          @PathVariable("type_id")
          long typeId);

  @Operation(summary = "List types")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of types",
            content = @Content(schema = @Schema(implementation = PagedTypeResponse.class)))
      })
  @GetMapping(produces = {"application/json"})
  PagedTypeResponse listTypes(
      @Parameter(description = "Search by the field: Created by") @RequestParam(required = false)
          String createdBy,
      @Parameter(description = "Search text (Search for Type-name)") @RequestParam(required = false)
          String query,
      @Parameter(description = "Show archived entries")
          @RequestParam(required = false, defaultValue = "false", value = "include_archived")
          boolean includeArchived,
      @Parameter(description = "Page offset - starts with 0") @RequestParam(required = false)
          Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit,
      @RequestParam(required = false, name = "list_all", defaultValue = "false") boolean listAll);

  @Operation(summary = "Get type")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Type not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Type descriptor",
            content = @Content(schema = @Schema(implementation = Type.class)))
      })
  @GetMapping(
      path = "/{type_id}",
      produces = {"application/json"})
  Type getType(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId);

  @Operation(
      summary = "Generate IOC instances by processing template and configuration",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable template or config",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Template or commit not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Permission denied",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "202",
            description = "The creation of IOC instances has started",
            content = @Content(schema = @Schema(implementation = OperationStartedResponse.class)))
      })
  @PostMapping(
      path = "/{type_id}/generation",
      produces = {"application/json"})
  @ResponseStatus(HttpStatus.ACCEPTED)
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  OperationStartedResponse createIocRevision(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @AuthenticationPrincipal OAuth2User user,
      @RequestBody ProcessTypeRequest processRequest);

  @Operation(summary = "Generate IOC preview")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Preview generation is restricted to .mustache extensions",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable template or config",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Template or commit not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "412",
            description = "The JSON config file is invalid",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "IOC preview",
            content = @Content(schema = @Schema(implementation = IocPreview.class)))
      })
  @PostMapping(
      path = "/{type_id}/generation/preview",
      produces = {"application/json"})
  IocPreview previewGeneration(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @RequestBody PreviewGenerationRequest previewGenerationRequest);

  @Operation(summary = "List latest, or all IOCs")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of IOCs",
            content = @Content(schema = @Schema(implementation = PagedIocResponse.class)))
      })
  @GetMapping(
      produces = {"application/json"},
      value = "/{type_id}/iocs")
  PagedIocResponse listIocs(
      @Parameter(
              description = "Which IOC-list type is requested: ALL, or LATEST (default is LATEST)")
          @RequestParam(required = false, name = "request_type")
          IocFetchOption listScope,
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable(name = "type_id")
          Long typeId,
      @Parameter(description = "User") @RequestParam(required = false) String user,
      @Parameter(description = "Query for config-, or creator-name") @RequestParam(required = false)
          String query,
      @Parameter(description = "Page offset - starts with 0") @RequestParam(required = false)
          Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit,
      @RequestParam(required = false, name = "list_all", defaultValue = "false") boolean listAll);

  @Operation(summary = "List config files")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Type or commit not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description =
                "List of config files (with validation error message"
                    + " in case of validation error)",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = ConfigFile.class))))
      })
  @GetMapping(
      path = "/{type_id}/generation/validation",
      produces = {"application/json"})
  List<ConfigFile> getConfigFiles(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @Parameter(in = ParameterIn.QUERY, description = "Type revision", required = true)
          @RequestParam("type_revision")
          String typeRevision,
      @Parameter(in = ParameterIn.QUERY, description = "Configuration revision", required = true)
          @RequestParam("config_revision")
          String configRevision)
      throws GitLabApiException;

  @Operation(
      summary = "Update type",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description =
                "Unprocessable request: " + "type or config Git project is already registered",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Type or git project not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Permission denied",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Type updated",
            content = @Content(schema = @Schema(implementation = Type.class)))
      })
  @PatchMapping(
      path = "/{type_id}",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  Type updateType(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @RequestBody UpdateTypeRequest request);

  @Operation(summary = "Get type files")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Type or commit not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service error",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Type file descriptors",
            content = @Content(schema = @Schema(implementation = TypeFilesResponse.class)))
      })
  @GetMapping(
      path = "/{type_id}/generation/files",
      produces = {"application/json"})
  TypeFilesResponse getTypeFileNames(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @Parameter(in = ParameterIn.QUERY, description = "Type revision", required = true)
          @RequestParam("type_revision")
          String typeRevision);

  @Operation(summary = "List Example projects")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Example projects",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = GitProject.class))))
      })
  @GetMapping(
      value = "/examples",
      produces = {"application/json"})
  List<GitProject> listExampleProjects();

  @Operation(summary = "List generated IOCs")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "IOC Type can not be found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of generated IOCs",
            content = @Content(schema = @Schema(implementation = GeneratedIocNames.class)))
      })
  @GetMapping(
      value = "/{type_id}/generation/minimal",
      produces = {"application/json"})
  GeneratedIocNames listIocsMinimal(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId);

  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @Operation(
      security = {@SecurityRequirement(name = "bearerAuth")},
      summary = "Deletes generated IOCs",
      description =
          "Administrators can delete IOCs from the DB, and it also removes the repo from Git")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC Type can not be found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Result of IOC deletion",
            content =
                @Content(
                    array =
                        @ArraySchema(
                            schema = @Schema(implementation = NameAndGitRepoWithMessage.class))))
      })
  @DeleteMapping(
      value = "/{type_id}/iocs",
      produces = {"application/json"})
  List<NameAndGitRepoWithMessage> deleteTypeIocs(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @Parameter(
              description =
                  "Force delete from DB regardless if deleting from repo was successful or not")
          @RequestParam(required = false, name = "force_delete", defaultValue = "false")
          boolean forceDelete,
      @RequestBody DeleteIocRequest request);

  @Operation(summary = "Get type history")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Type not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "A paged array of type history entries",
            content = @Content(schema = @Schema(implementation = PagedTypeHistoryResponse.class)))
      })
  @GetMapping(
      path = "/{type_id}/jobs",
      produces = {"application/json"})
  PagedTypeHistoryResponse getTypeHistory(
      @Parameter(in = ParameterIn.PATH, description = "Type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @Parameter(description = "Page offset - starts with 0") @RequestParam(required = false)
          Integer page,
      @Parameter(description = "Page size") @RequestParam(required = false) Integer limit);

  @Operation(
      summary = "Calculates how many new IOCs/repos will be generated from the selected configs",
      security = {@SecurityRequirement(name = "bearerAuth")})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "422",
            description = "Unprocessable request due empty config list",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC type not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "403",
            description = "Permission denied",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Number of new IOCs that will be created",
            content = @Content(schema = @Schema(implementation = CalculateNewIocsResponse.class)))
      })
  @PostMapping(
      path = "/{type_id}/generation/calculate_new_iocs",
      produces = {"application/json"})
  @PreAuthorize(WebSecurityConfig.IS_ADMIN_OR_INTEGRATOR)
  CalculateNewIocsResponse calculateNewIocs(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(description = "The IOC type ID for which the new IOCs has to be counted")
          @PathVariable(name = "type_id")
          Long typeId,
      @RequestBody CalculateNewIocsRequest configNames);

  @Operation(
      security = {@SecurityRequirement(name = "bearerAuth")},
      summary = "Attach git repositories to a type as instances",
      description = "Attaches git repositories as instances of the IOC type")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "400",
            description = "IOC instance already belongs to an IOC type",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC type can not be found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Instances have been attached",
            content =
                @Content(
                    array =
                        @ArraySchema(
                            schema = @Schema(implementation = AttachInstanceResponse.class))))
      })
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @PostMapping(
      value = "/{type_id}/attach",
      produces = {"application/json"})
  List<AttachInstanceResponse> attachInstances(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(in = ParameterIn.PATH, description = "IOC type ID", required = true)
          @PathVariable("type_id")
          long typeId,
      @RequestBody AttachInstanceRequest request);

  @Operation(
      security = {@SecurityRequirement(name = "bearerAuth")},
      summary = "Move instance between IOC types",
      description = "Move instance between IOC types")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "401",
            description = "Unauthorized",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "404",
            description = "IOC Type or IOC Instance can not be found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(responseCode = "204", description = "Instance has been moved")
      })
  @PreAuthorize(WebSecurityConfig.IS_ADMIN)
  @PutMapping(
      value = "/{type_id}/iocs/{ioc_instance_id}/move",
      produces = {"application/json"})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void moveInstance(
      @AuthenticationPrincipal OAuth2User user,
      @Parameter(
              in = ParameterIn.PATH,
              description = "Move instance from another type to this",
              required = true)
          @PathVariable("type_id")
          long typeId,
      @Parameter(
              in = ParameterIn.PATH,
              description = "IOC Instance ID that should be moved",
              required = true)
          @PathVariable("ioc_instance_id")
          long instanceId);
}
