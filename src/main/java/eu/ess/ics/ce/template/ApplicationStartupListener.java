/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template;

import eu.ess.ics.ce.template.service.internal.iocinstance.IocInstanceService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Component
public class ApplicationStartupListener {

  private final IocInstanceService iocInstanceService;

  public ApplicationStartupListener(IocInstanceService iocInstanceService) {
    this.iocInstanceService = iocInstanceService;
  }

  @EventListener(ApplicationReadyEvent.class)
  public void initTasks() {
    iocInstanceService.executeQueuedOperations();
  }
}
