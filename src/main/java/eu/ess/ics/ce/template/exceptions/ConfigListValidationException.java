package eu.ess.ics.ce.template.exceptions;

import java.util.Map;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class ConfigListValidationException extends CcceException {
  private static final String ERROR =
      "One or more configuration files are incompatible with the selected template revision";
  private final Map<String, ConfigValidationException> errorMap;

  public ConfigListValidationException(Map<String, ConfigValidationException> errorMap) {
    super("Validation failed", ERROR);
    this.errorMap = errorMap;
  }

  public Map<String, ConfigValidationException> getErrorMap() {
    return errorMap;
  }
}
