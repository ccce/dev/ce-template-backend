package eu.ess.ics.ce.template.rest.model.jobs.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ce.template.common.conversion.ZonedDateTimeSerializer;
import java.time.ZonedDateTime;
import java.util.Objects;

public class InstanceLogEntry {

  private InstanceGenerationStep step;

  @JsonProperty("step_index")
  private Integer stepIndex;

  @JsonProperty("created_at")
  @JsonSerialize(using = ZonedDateTimeSerializer.class)
  private ZonedDateTime createdAt;

  @JsonProperty("finished_at")
  @JsonSerialize(using = ZonedDateTimeSerializer.class)
  private ZonedDateTime finishedAt;

  @JsonProperty("status")
  private InstanceGenerationStatus status;

  private String message;

  public InstanceLogEntry(
      InstanceGenerationStep step,
      Integer stepIndex,
      ZonedDateTime createdDate,
      ZonedDateTime finishedDate,
      InstanceGenerationStatus status,
      String message) {
    this.step = step;
    this.stepIndex = stepIndex;
    this.createdAt = createdDate;
    this.finishedAt = finishedDate;
    this.status = status;
    this.message = message;
  }

  public InstanceGenerationStep getStep() {
    return step;
  }

  public void setStep(InstanceGenerationStep step) {
    this.step = step;
  }

  public Integer getStepIndex() {
    return stepIndex;
  }

  public void setStepIndex(Integer stepIndex) {
    this.stepIndex = stepIndex;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public ZonedDateTime getFinishedAt() {
    return finishedAt;
  }

  public void setFinishedAt(ZonedDateTime finishedAt) {
    this.finishedAt = finishedAt;
  }

  public InstanceGenerationStatus status() {
    return status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    InstanceLogEntry that = (InstanceLogEntry) o;
    return step == that.step
        && Objects.equals(stepIndex, that.stepIndex)
        && Objects.equals(createdAt, that.createdAt)
        && Objects.equals(finishedAt, that.finishedAt)
        && Objects.equals(status, that.status)
        && Objects.equals(message, that.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(step, stepIndex, createdAt, finishedAt, status, message);
  }

  @Override
  public String toString() {
    return "InstanceLogEntry["
        + "step="
        + step
        + ", stepIndex="
        + stepIndex
        + ", createdDate="
        + createdAt
        + ", finishedDate="
        + finishedAt
        + ", status="
        + status
        + ", message='"
        + message
        + '\''
        + ']';
  }
}
