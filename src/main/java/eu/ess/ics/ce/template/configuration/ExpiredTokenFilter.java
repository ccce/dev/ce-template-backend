/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.configuration;

import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.ClientAuthorizationException;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class ExpiredTokenFilter extends OncePerRequestFilter {
  /*
  IMPORTANT!!
      this value has to be the same as in the application.properties:
      spring.security.oauth2.client.registration.[registrationId]
   */
  private static final String REGISTRATION_ID = "ping";

  private final OAuth2AuthorizedClientManager authorizedClientManager;

  private final OAuth2AuthorizedClientRepository authorizedClientRepository;

  public ExpiredTokenFilter(
      OAuth2AuthorizedClientManager authorizedClientManager,
      OAuth2AuthorizedClientRepository authorizedClientRepository) {
    this.authorizedClientManager = authorizedClientManager;
    this.authorizedClientRepository = authorizedClientRepository;
  }

  @Override
  protected void doFilterInternal(
      @Nonnull HttpServletRequest request,
      @Nonnull HttpServletResponse response,
      @Nonnull FilterChain filterChain)
      throws ServletException, IOException {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (Objects.nonNull(authentication) && authentication.getPrincipal() instanceof OidcUser) {
      validateToken(authentication, request, response);
    }

    filterChain.doFilter(request, response);
  }

  private void validateToken(
      Authentication auth, HttpServletRequest request, HttpServletResponse response) {

    OAuth2AuthorizeRequest authorizeRequest =
        OAuth2AuthorizeRequest.withClientRegistrationId(REGISTRATION_ID)
            .principal(auth)
            .attributes(
                attrs -> {
                  attrs.put(HttpServletRequest.class.getName(), request);
                  attrs.put(HttpServletResponse.class.getName(), response);
                })
            .build();
    try {
      authorizedClientManager.authorize(authorizeRequest);
    } catch (ClientAuthorizationException e) {
      authorizedClientRepository.removeAuthorizedClient(REGISTRATION_ID, auth, request, response);
      SecurityContextHolder.getContext().setAuthentication(null);
    }
  }
}
