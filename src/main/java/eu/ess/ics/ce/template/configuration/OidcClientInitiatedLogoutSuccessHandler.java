/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.configuration;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistration.ProviderDetails;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class OidcClientInitiatedLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
  private final ClientRegistrationRepository clientRegistrationRepository;

  @Value("${ui.baseurl}")
  private String uiBaseUrl;

  private String postLogoutRedirectUri;

  public OidcClientInitiatedLogoutSuccessHandler(
      ClientRegistrationRepository clientRegistrationRepository) {
    Assert.notNull(clientRegistrationRepository, "clientRegistrationRepository cannot be null");
    this.clientRegistrationRepository = clientRegistrationRepository;
  }

  @Override
  protected String determineTargetUrl(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    String targetUrl = null;
    if (authentication instanceof OAuth2AuthenticationToken token
        && authentication.getPrincipal() instanceof OidcUser) {

      String redirectTo;

      String redirectRequest = request.getParameter("redirect_url");

      if (StringUtils.isEmpty(redirectRequest)) {
        redirectTo = uiBaseUrl;
      } else {
        try {
          // the query param for the logout-redirect url should be URL encoded
          redirectTo = parseUrl(redirectRequest).toString();
        } catch (Exception e) {
          redirectTo = uiBaseUrl;
        }
      }

      setPostLogoutRedirectUri(redirectTo);
      String registrationId = token.getAuthorizedClientRegistrationId();
      ClientRegistration clientRegistration =
          this.clientRegistrationRepository.findByRegistrationId(registrationId);
      URI endSessionEndpoint = this.endSessionEndpoint(clientRegistration);
      if (endSessionEndpoint != null) {
        String idToken = idToken(authentication);
        String postLogoutRedirectUri = postLogoutRedirectUri(request, clientRegistration);
        targetUrl = endpointUri(endSessionEndpoint, idToken, postLogoutRedirectUri);
      }
    }
    return (targetUrl != null) ? targetUrl : super.determineTargetUrl(request, response);
  }

  private URI endSessionEndpoint(ClientRegistration clientRegistration) {
    if (clientRegistration != null) {
      ProviderDetails providerDetails = clientRegistration.getProviderDetails();
      Object endSessionEndpoint =
          providerDetails.getConfigurationMetadata().get("end_session_endpoint");
      if (endSessionEndpoint != null) {
        return URI.create(endSessionEndpoint.toString());
      }
    }
    return null;
  }

  private String idToken(Authentication authentication) {
    return ((OidcUser) authentication.getPrincipal()).getIdToken().getTokenValue();
  }

  private String postLogoutRedirectUri(
      HttpServletRequest request, ClientRegistration clientRegistration) {
    if (this.postLogoutRedirectUri == null) {
      return null;
    }
    // @formatter:off
    UriComponents uriComponents =
        UriComponentsBuilder.fromHttpUrl(UrlUtils.buildFullRequestUrl(request))
            .replacePath(request.getContextPath())
            .replaceQuery(null)
            .fragment(null)
            .build();

    Map<String, String> uriVariables = new HashMap<>();
    String scheme = uriComponents.getScheme();
    uriVariables.put("baseScheme", (scheme != null) ? scheme : "");
    uriVariables.put("baseUrl", uriComponents.toUriString());

    String host = uriComponents.getHost();
    uriVariables.put("baseHost", (host != null) ? host : "");

    String path = uriComponents.getPath();
    uriVariables.put("basePath", (path != null) ? path : "");

    int port = uriComponents.getPort();
    uriVariables.put("basePort", (port == -1) ? "" : ":" + port);

    uriVariables.put("registrationId", clientRegistration.getRegistrationId());

    return UriComponentsBuilder.fromUriString(this.postLogoutRedirectUri)
        .buildAndExpand(uriVariables)
        .toUriString();
    // @formatter:on
  }

  private String endpointUri(URI endSessionEndpoint, String idToken, String postLogoutRedirectUri) {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUri(endSessionEndpoint);
    builder.queryParam("id_token_hint", idToken);
    if (postLogoutRedirectUri != null) {
      builder.queryParam("TargetResource", postLogoutRedirectUri);
    }
    // @formatter:off
    return builder.encode(StandardCharsets.UTF_8).build().toUriString();
    // @formatter:on
  }

  /**
   * Set the post logout redirect uri template. <br>
   * The supported uri template variables are: {@code {baseScheme}}, {@code {baseHost}}, {@code
   * {basePort}} and {@code {basePath}}. <br>
   * <b>NOTE:</b> {@code "{baseUrl}"} is also supported, which is the same as {@code
   * "{baseScheme}://{baseHost}{basePort}{basePath}"}
   *
   * <pre>
   * 	handler.setPostLogoutRedirectUri("{baseUrl}");
   * </pre>
   *
   * will make so that {@code post_logout_redirect_uri} will be set to the base url for the client
   * application.
   *
   * @param postLogoutRedirectUri - A template for creating the {@code post_logout_redirect_uri}
   *     query parameter
   * @since 5.3
   */
  public void setPostLogoutRedirectUri(String postLogoutRedirectUri) {
    Assert.notNull(postLogoutRedirectUri, "postLogoutRedirectUri cannot be null");
    this.postLogoutRedirectUri = postLogoutRedirectUri;
  }

  public static URL parseUrl(String s) throws Exception {
    URL u = new URL(s);
    return new URI(u.getProtocol(), u.getAuthority(), u.getPath(), u.getQuery(), u.getRef())
        .toURL();
  }
}
