package eu.ess.ics.ce.template.rest.model.template.request;

import static eu.ess.ics.ce.template.rest.model.template.request.ValidationConstants.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class CreateTypeRequest {

  @JsonProperty("type_name")
  @Schema(required = true, description = TEMPLATE_NAME_ALLOWED_FORMAT, pattern = TYPE_NAME_REGEX)
  private String templateName;

  @ArraySchema(
      schema =
          @Schema(
              description = IOC_INSTANCE_NAME_ALLOWED_FORMAT,
              pattern = IOC_INSTANCE_NAME_REGEX))
  private List<String> iocs;

  @Schema(description = "Description for the IOC Type")
  private String description;

  public String getTemplateName() {
    return templateName;
  }

  public List<String> getIocs() {
    return iocs;
  }

  public String getDescription() {
    return description;
  }
}
