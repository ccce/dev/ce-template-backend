package eu.ess.ics.ce.template.rest.model.types.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.ess.ics.ce.template.common.conversion.ZonedDateTimeSerializer;
import jakarta.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Ioc {
  @NotNull private final Long id;

  @NotNull
  @JsonProperty("type_id")
  private final Long typeId;

  @NotNull
  @JsonProperty("type_name")
  private final String typeName;

  @NotNull
  @JsonProperty("created_by")
  private final String createdBy;

  @JsonProperty("created_at")
  @JsonSerialize(using = ZonedDateTimeSerializer.class)
  private final ZonedDateTime createdAt;

  @JsonProperty("git_project_id")
  private final Long gitProjectId;

  @JsonProperty("git_tag")
  private final String gitTag;

  @JsonProperty("configuration_name")
  private final String configurationName;

  @JsonProperty("version")
  private final long version;

  public Ioc(
      Long id,
      Long typeId,
      String typeName,
      String createdBy,
      ZonedDateTime createdAt,
      Long gitProjectId,
      String gitTag,
      String configurationName,
      long version) {
    this.id = id;
    this.typeId = typeId;
    this.typeName = typeName;
    this.createdBy = createdBy;
    this.createdAt = createdAt;
    this.gitProjectId = gitProjectId;
    this.configurationName = configurationName;
    this.version = version;
    this.gitTag = gitTag;
  }

  public Long getId() {
    return id;
  }

  public Long getTypeId() {
    return typeId;
  }

  public String getTypeName() {
    return typeName;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public Long getGitProjectId() {
    return gitProjectId;
  }

  public String getConfigurationName() {
    return configurationName;
  }

  public long getVersion() {
    return version;
  }

  public String getGitTag() {
    return gitTag;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Ioc that = (Ioc) o;
    return version == that.version
        && Objects.equals(id, that.id)
        && Objects.equals(typeId, that.typeId)
        && Objects.equals(typeName, that.typeName)
        && Objects.equals(createdBy, that.createdBy)
        && Objects.equals(createdAt, that.createdAt)
        && Objects.equals(gitProjectId, that.gitProjectId)
        && Objects.equals(gitTag, that.gitTag)
        && Objects.equals(configurationName, that.configurationName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        id,
        typeId,
        typeName,
        createdBy,
        createdAt,
        gitProjectId,
        gitTag,
        configurationName,
        version);
  }

  @Override
  public String toString() {
    return "Ioc{"
        + "id="
        + id
        + ", typeId="
        + typeId
        + ", typeName='"
        + typeName
        + '\''
        + ", createdBy='"
        + createdBy
        + '\''
        + ", createdAt="
        + createdAt
        + ", gitProjectId="
        + gitProjectId
        + ", gitTag='"
        + gitTag
        + '\''
        + ", configurationName='"
        + configurationName
        + '\''
        + ", version="
        + version
        + '}';
  }
}
