package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record TypeFilesResponse(
    @NotNull @JsonProperty("type_id") Long typeId,
    @NotNull @JsonProperty("type_revision") String typeRevision,
    @JsonProperty("type_files") List<String> typeFiles,
    @JsonProperty("files_to_copy") List<String> filesToCopy,
    @JsonProperty("ignored_files") List<String> ignoredFiles) {}
