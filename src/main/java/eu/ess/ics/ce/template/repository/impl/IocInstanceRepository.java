package eu.ess.ics.ce.template.repository.impl;

import static eu.ess.ics.ce.template.common.Constants.QUEUED_STATUSES;

import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Repository
public class IocInstanceRepository implements IIocInstanceRepository {
  @PersistenceContext private EntityManager em;

  @Override
  public void create(IocInstanceEntity iocInstance) {
    em.persist(iocInstance);
  }

  @Override
  public void updateInstance(IocInstanceEntity iocInstance) {
    em.merge(iocInstance);
  }

  private static final String COUNT_OF_ALL_CURRENT_INSTANCES =
      "SELECT COUNT(DISTINCT ie.gitProjectId) FROM IocInstanceEntity ie "
          + " WHERE ie.iocTypeName NOT IN (SELECT i.name FROM IocTypeEntity i WHERE i.deleted = true)";

  private static final String LATEST_INSTANCES =
      "FROM IocInstanceEntity ie " + " WHERE ie.iocType.id = :pTemplateId";

  private static final String LATEST_INSTANCE_QUERY =
      " AND (LOWER(ie.configurationName) LIKE LOWER(:pQuery))";
  private static final String LATEST_INSTANCE_NAME_ORDER = " ORDER BY ie.configurationName ASC";
  private static final String LATEST_INSTANCE_VERSION_ORDER = " ORDER BY ie.version DESC";

  @Override
  public List<IocInstanceEntity> findAll(
      Long templateId, String query, String user, int page, Integer limit) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<IocInstanceEntity> cq = cb.createQuery(IocInstanceEntity.class);
    Root<IocInstanceEntity> from = cq.from(IocInstanceEntity.class);
    CriteriaQuery<IocInstanceEntity> select = cq.select(from);

    // Default order
    cq.orderBy(cb.desc(from.get("createdAt")));

    TypedQuery<IocInstanceEntity> q = pagingQuery(cb, select, from, cq, templateId, query, user);
    if (limit != null) {
      q.setFirstResult(page * limit);
      q.setMaxResults(limit);
    }

    return q.getResultList();
  }

  @Override
  public List<IocInstanceEntity> findLatestInstances(
      long templateId, String query, int page, Integer limit) {
    return findLatestInstances(templateId, query, page, limit, null);
  }

  @Override
  public List<IocInstanceEntity> findLatestInstances(
      long templateId, String query, int page, Integer limit, Boolean sortByVersion) {

    String queryString = "SELECT  ie " + LATEST_INSTANCES;

    if (query != null) {
      queryString += LATEST_INSTANCE_QUERY;
    }

    if (BooleanUtils.toBoolean(sortByVersion)) {
      queryString += LATEST_INSTANCE_VERSION_ORDER;
    } else {
      queryString += LATEST_INSTANCE_NAME_ORDER;
    }

    TypedQuery<IocInstanceEntity> dbQuery =
        em.createQuery(queryString, IocInstanceEntity.class)
            .setParameter("pTemplateId", templateId);

    if (query != null) {
      dbQuery = dbQuery.setParameter("pQuery", "%" + query + "%");
    }

    if (limit != null) {
      dbQuery.setMaxResults(limit).setFirstResult(page * limit);
    }

    return dbQuery.getResultList();
  }

  @Override
  public long countAllLatestInstances() {
    return em.createQuery(COUNT_OF_ALL_CURRENT_INSTANCES, Long.class).getSingleResult();
  }

  @Override
  public long countLatestInstancesForIocType(long typeId) {
    return em.createQuery(
            COUNT_OF_ALL_CURRENT_INSTANCES + " AND ie.iocType.id = :typeId", Long.class)
        .setParameter("typeId", typeId)
        .getSingleResult();
  }

  @Override
  public long countInstancesForPaging(long templateId, String query) {
    String queryString = "SELECT COUNT(*) " + LATEST_INSTANCES;

    if (query != null) {
      queryString += LATEST_INSTANCE_QUERY;
    }

    TypedQuery<Long> dbQuery =
        (TypedQuery<Long>)
            em.createQuery(queryString, Long.class).setParameter("pTemplateId", templateId);

    if (query != null) {
      dbQuery = dbQuery.setParameter("pQuery", "%" + query + "%");
    }

    return dbQuery.getSingleResult();
  }

  @Override
  public long countInstancesForOperation(final long operationId) {
    return em.createQuery(
            "SELECT COUNT( DISTINCT il.iocInstance.id) FROM IocInstanceLogEntity il WHERE il.operation.id = :operationId",
            Long.class)
        .setParameter("operationId", operationId)
        .getSingleResult();
  }

  @Override
  public long countForPaging(Long iocType, String query, String user) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<Long> cq = cb.createQuery(Long.class);
    Root<IocInstanceEntity> from = cq.from(IocInstanceEntity.class);
    Expression<Long> count1 = cb.count(from);
    CriteriaQuery<Long> select = cq.select(count1);

    return pagingQuery(cb, select, from, cq, iocType, query, user).getSingleResult();
  }

  @Override
  public Long findMaxVersionForRepoId(long repoId) {
    return em.createQuery(
            "SELECT MAX(v.version) FROM IocInstanceEntity v WHERE v.gitProjectId = :pProjectId",
            Long.class)
        .setParameter("pProjectId", repoId)
        .getSingleResult();
  }

  @Override
  public List<IocInstanceEntity> findByOperationId(Long operationId) {
    return findByOperationId(operationId, null, null);
  }

  @Override
  public IocInstanceEntity findById(long instanceId) {
    return em.createQuery(
            "SELECT ie FROM IocInstanceEntity ie WHERE ie.id = :instanceId",
            IocInstanceEntity.class)
        .setParameter("instanceId", instanceId)
        .getSingleResult();
  }

  @Override
  public List<IocInstanceEntity> findByOperationId(Long operationId, Integer page, Integer limit) {
    TypedQuery<IocInstanceEntity> query =
        em.createQuery(
                "SELECT DISTINCT il.iocInstance FROM IocInstanceLogEntity il WHERE il.operation.id = :pOpId",
                IocInstanceEntity.class)
            .setParameter("pOpId", operationId);

    if (page != null && limit != null) {
      query.setMaxResults(limit).setFirstResult(page * limit);
    }

    return query.getResultList();
  }

  @Override
  public boolean checkInstanceIsCreated(Long typeId, String configName) {
    Long countInstances =
        em.createQuery(
                "SELECT COUNT(*) FROM IocInstanceEntity i "
                    + " WHERE i.iocType.id = :pTypeId AND "
                    + " i.configurationName = :pConfigName",
                Long.class)
            .setParameter("pTypeId", typeId)
            .setParameter("pConfigName", configName)
            .getSingleResult();

    return countInstances > 0;
  }

  @Override
  public List<IocInstanceEntity> findQueuedIocInstances() {
    return em.createQuery(
            "SELECT i FROM IocInstanceEntity i WHERE i.status IN :statuses",
            IocInstanceEntity.class)
        .setParameter("statuses", QUEUED_STATUSES)
        .getResultList();
  }

  @Override
  public boolean hasInstanceRepositoryNewlyCreated(IocInstanceEntity instance) {
    return em.createQuery(
                "SELECT COUNT(*) FROM IocInstanceEntity i "
                    + " WHERE i.id != :pInstanceId AND i.createdAt < :pCreatedAt "
                    + " AND i.gitProjectId = :pGitProjectId",
                Long.class)
            .setParameter("pInstanceId", instance.getId())
            .setParameter("pCreatedAt", instance.getCreatedAt())
            .setParameter("pGitProjectId", instance.getGitProjectId())
            .getSingleResult()
        == 0L;
  }

  @Override
  public boolean instanceExists(long typeId, long gitRepoId, String configName) {
    return em.createQuery(
                "SELECT COUNT(*) FROM IocInstanceEntity i "
                    + " WHERE i.gitProjectId = :pGitRepoId AND lower(i.configurationName) = lower( :pConfigName ) "
                    + " AND i.iocType.id = :pTypeId",
                Long.class)
            .setParameter("pGitRepoId", gitRepoId)
            .setParameter("pConfigName", configName)
            .setParameter("pTypeId", typeId)
            .getSingleResult()
        > 0L;
  }

  @Override
  public void deleteInstances(long typeId, long gitRepoId, String configName) {
    em.createQuery(
            "DELETE FROM IocInstanceEntity ie WHERE   "
                + " ie.iocType.id = :pTypeId AND "
                + " lower(ie.configurationName) = lower( :pConfigName ) AND "
                + " ie.gitProjectId = :pGitRepoId")
        .setParameter("pTypeId", typeId)
        .setParameter("pConfigName", configName)
        .setParameter("pGitRepoId", gitRepoId)
        .executeUpdate();
  }

  @Override
  public void deleteInstance(long instanceId) {
    em.createQuery("DELETE FROM IocInstanceEntity i WHERE i.id = :instanceId")
        .setParameter("instanceId", instanceId)
        .executeUpdate();
  }

  @Override
  public Long findOutputProjectForTypeAndConfig(long typeId, String configurationName) {
    return em
        .createQuery(
            "SELECT instance.gitProjectId FROM IocInstanceEntity instance "
                + "WHERE instance.operation.id IN "
                + "(SELECT operation.id FROM OperationEntity operation WHERE operation.iocType.id = :iocTypeId) "
                + "AND instance.configurationName = :configurationName "
                + "AND instance.gitProjectId IS NOT NULL ORDER BY instance.createdAt DESC",
            Long.class)
        .setParameter("iocTypeId", typeId)
        .setParameter("configurationName", configurationName)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public String latestTag(long typeId) {
    return em
        .createQuery(
            "SELECT ie.gitTag FROM IocInstanceEntity ie WHERE ie.iocType.id = :typeId "
                + " AND ie.gitTag IS NOT NULL ORDER BY ie.version DESC",
            String.class)
        .setParameter("typeId", typeId)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public IocInstanceEntity findByTypeAndName(Long typeId, String configName) {

    return em
        .createQuery(
            "FROM IocInstanceEntity ie WHERE ie.iocType.id = :pTypeId AND "
                + " ie.configurationName = :pConfigName",
            IocInstanceEntity.class)
        .setParameter("pTypeId", typeId)
        .setParameter("pConfigName", configName)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public OperationEntity fetchOperation(long iocInstanceId) {

    return em
        .createQuery(
            "SELECT o FROM IocInstanceLogEntity il INNER JOIN il.iocInstance ie INNER JOIN il.operation o "
                + " WHERE ie.id = :pInstanceId ORDER BY o.createdAt DESC",
            OperationEntity.class)
        .setParameter("pInstanceId", iocInstanceId)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public long countInstancesWithRepoId(long repoId) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocInstanceEntity WHERE gitProjectId = :pRepoId", Long.class)
        .setParameter("pRepoId", repoId)
        .getSingleResult();
  }

  private <T, R> TypedQuery<T> pagingQuery(
      CriteriaBuilder cb,
      CriteriaQuery<T> select,
      Root<R> from,
      CriteriaQuery<T> cq,
      Long iocTypeId,
      String query,
      String user) {
    List<Predicate> predicates = new ArrayList<>();

    if (!StringUtils.isEmpty(query)) {
      predicates.add(
          cb.like(cb.lower(from.get("configurationName")), "%" + query.toLowerCase() + "%"));
    }

    if (iocTypeId != null) {
      predicates.add(cb.equal(from.get("iocType").get("id"), iocTypeId));
    }

    if (!StringUtils.isEmpty(user)) {
      predicates.add(cb.like(cb.lower(from.get("createdBy")), "%" + user.toLowerCase() + "%"));
    }

    select.where(cb.and(predicates.toArray(new Predicate[0])));
    return em.createQuery(cq);
  }
}
