package eu.ess.ics.ce.template.rest.model.jobs.response;

public enum InstanceGenerationStatus {
  QUEUED,
  SKIPPED,
  FAILED,
  RUNNING,
  SUCCESSFUL,
}
