package eu.ess.ics.ce.template.service.internal;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class PagingLimitDto {
  private int page;
  private Integer limit;

  public PagingLimitDto(int page, Integer limit) {
    this.page = page;
    this.limit = limit;
  }

  public int getPage() {
    return page;
  }

  public Integer getLimit() {
    return limit;
  }
}
