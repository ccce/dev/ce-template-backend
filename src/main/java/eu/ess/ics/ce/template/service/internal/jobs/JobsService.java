/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal.jobs;

import static eu.ess.ics.ce.template.common.Constants.FINISHED_STATUSES;

import eu.ess.ics.ce.template.common.conversion.PagingUtil;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.*;
import eu.ess.ics.ce.template.rest.model.types.response.GitProjectInfo;
import eu.ess.ics.ce.template.rest.model.types.response.InstanceParent;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.external.gitlab.dto.GitRef;
import eu.ess.ics.ce.template.service.internal.PagingLimitDto;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import jakarta.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class JobsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(JobsService.class);

  private final IOperationRepository operationRepository;
  private final IIocInstanceRepository instanceRepository;
  private final IInstanceLogRepository logRepository;
  private final GitLabService gitLabService;
  private final PagingUtil pagingUtil;

  public JobsService(
      IOperationRepository operationRepository,
      IIocInstanceRepository instanceRepository,
      IInstanceLogRepository logRepository,
      GitLabService gitLabService,
      PagingUtil pagingUtil) {
    this.operationRepository = operationRepository;
    this.instanceRepository = instanceRepository;
    this.logRepository = logRepository;
    this.gitLabService = gitLabService;
    this.pagingUtil = pagingUtil;
  }

  public PagedJobResponse findOperations(
      String query, Long iocTypeId, Integer page, Integer limit, String user) {
    PagingLimitDto pageSizes = pagingUtil.pageLimitConverter(page, limit);

    long count = operationRepository.countForPaging(query, iocTypeId, user);

    List<OperationEntity> operations =
        operationRepository.findOperations(
            query, iocTypeId, user, pageSizes.getPage(), pageSizes.getLimit());

    List<Job> jobResult = convertOperation(operations);

    return new PagedJobResponse(
        jobResult, count, jobResult.size(), pageSizes.getPage(), pageSizes.getLimit());
  }

  public JobDetails getOperationById(Long operationId) {
    OperationEntity operationEntity = operationRepository.findOperationById(operationId);
    long instanceCount = instanceRepository.countInstancesForOperation(operationId);
    return convertOperations(operationEntity, instanceCount);
  }

  public JobInstanceListResponse getInstancesOfOperation(
      final Long operationId, Integer page, Integer limit) {
    operationRepository.findOperationById(operationId);

    PagingLimitDto pageSizes = pagingUtil.pageLimitConverter(page, limit);
    final List<IocInstanceEntity> iocInstanceEntities =
        instanceRepository.findByOperationId(
            operationId, pageSizes.getPage(), pageSizes.getLimit());

    final List<JobInstance> instances = convertOperationInstances(iocInstanceEntities);

    final long count = instanceRepository.countInstancesForOperation(operationId);
    return new JobInstanceListResponse(
        operationId, instances, count, instances.size(), pageSizes.getPage(), pageSizes.getLimit());
  }

  public JobStatusResponse getOperationStatusById(final Long operationId) {
    final OperationEntity operation = operationRepository.findOperationById(operationId);

    final List<IocInstanceLogEntity> logsForOperation =
        logRepository.findLastLogEntriesForOperation(operationId);

    final GenerationStatus status = convertGenerationStatus(logsForOperation);
    SummaryResponse summary = createSummary(logsForOperation);

    return new JobStatusResponse(
        operationId, status, operation.getCreatedAt(), operation.getFinishedAt(), summary);
  }

  private static SummaryResponse createSummary(
      final List<IocInstanceLogEntity> instancesOfOperation) {
    if (isOperationFinished(instancesOfOperation)) {
      final long all = instancesOfOperation.size();
      final long successful = countSuccessfulProcesses(instancesOfOperation);
      final long failed = all - successful;
      return new SummaryResponse(successful, failed);
    }
    return null;
  }

  private static boolean isOperationFinished(
      final List<IocInstanceLogEntity> instancesOfOperation) {
    return instancesOfOperation.stream()
        .allMatch(iocInstanceEntity -> FINISHED_STATUSES.contains(iocInstanceEntity.getStatus()));
  }

  private static long countSuccessfulProcesses(
      final List<IocInstanceLogEntity> instancesOfOperation) {
    return instancesOfOperation.stream()
        .filter(
            iocInstanceEntity ->
                GenerationStatus.SUCCESSFUL.equals(
                    GenerationStatus.valueOf(iocInstanceEntity.getStatus())))
        .count();
  }

  public static List<JobInstance> convertOperationInstances(
      final List<IocInstanceEntity> instances) {
    return instances.stream()
        .map(i -> new JobInstance(i.getId(), i.getConfigurationName(), i.getGitProjectId()))
        .toList();
  }

  public List<Job> convertOperation(List<OperationEntity> operationEntities) {
    List<Job> result = new ArrayList<>();

    if ((operationEntities == null) || (operationEntities.isEmpty())) {
      return result;
    }

    for (OperationEntity o : operationEntities) {
      result.add(
          new Job(
              o.getId(),
              o.getTypeName(),
              o.getCreatedBy(),
              o.getGitTag(),
              o.getCreatedAt(),
              o.getFinishedAt(),
              instanceRepository.countInstancesForOperation(o.getId()),
              o.getIocType().getId(),
              convertGenerationStatus(logRepository.findLastLogEntriesForOperation(o.getId())),
              JobType.valueOf(o.getJobType())));
    }

    return result;
  }

  public JobDetails convertOperations(OperationEntity operation, Long iocCount) {

    final String typeRevision = operation.getTemplateRevision();
    final GitRef templateRev =
        gitLabService.getRef(operation.getIocType().getTemplateGitProjectId(), typeRevision);

    final String configRevision = operation.getConfigurationRevision();
    final GitRef configRev =
        gitLabService.getRef(operation.getIocType().getConfigGitProjectId(), configRevision);

    InstanceParent parent =
        operation.getPredecessorTypeName() == null
            ? null
            : new InstanceParent(
                operation.getPredecessorType() == null
                    ? null
                    : operation.getPredecessorType().getId(),
                operation.getPredecessorType().getName());

    GitProjectInfo templateProject =
        new GitProjectInfo(templateRev.revision(), templateRev.webUrl(), templateRev.refType());

    GitProjectInfo configProject =
        new GitProjectInfo(configRev.revision(), configRev.webUrl(), configRev.refType());

    return new JobDetails(
        operation.getId(),
        JobType.valueOf(operation.getJobType()),
        operation.getTypeName(),
        operation.getIocType().getId(),
        parent,
        operation.getCreatedBy(),
        operation.getGitTag(),
        templateProject,
        configProject,
        iocCount,
        operation.getCreatedAt(),
        operation.getFinishedAt());
  }

  private Commit getGitCommit(final Long projectId, final String gitRevision) {
    try {
      return gitLabService.getCommit(projectId, gitRevision);
    } catch (GitLabApiException e) {
      LOGGER.error(
          "Cannot find on Gitlab the commit by ProjectID: {} and CommitID: {}",
          projectId,
          gitRevision,
          e);
      return null;
    }
  }

  public static GenerationStatus convertGenerationStatus(
      @NotNull final List<IocInstanceLogEntity> instancesLogs) {

    if (instancesLogs.stream()
        .allMatch(
            iocInstanceLogEntity ->
                InstanceStatus.SUCCESSFUL.toString().equals(iocInstanceLogEntity.getStatus()))) {
      return GenerationStatus.SUCCESSFUL;
    }

    if (instancesLogs.stream()
        .allMatch(
            iocInstanceLogEntity ->
                InstanceStatus.FAILED.toString().equals(iocInstanceLogEntity.getStatus()))) {
      return GenerationStatus.FAILED;
    }

    if (instancesLogs.stream()
        .anyMatch(
            iocInstanceLogEntity ->
                InstanceStatus.FAILED.toString().equals(iocInstanceLogEntity.getStatus()))) {
      return GenerationStatus.PARTIAL_FAILURE;
    }

    return GenerationStatus.RUNNING;
  }

  @Cacheable(value = "job_metrics", cacheManager = "metricCacheManager")
  public Map<JobType, Map<GenerationStatus, Long>> getJobMetrics() {
    return logRepository.findLastLogEntries().stream()
        .collect(
            Collectors.groupingBy(
                log -> JobType.valueOf(log.getOperation().getJobType()),
                Collectors.collectingAndThen(
                    Collectors.groupingBy(
                        IocInstanceLogEntity::getOperation,
                        Collectors.collectingAndThen(
                            Collectors.toList(), JobsService::convertGenerationStatus)),
                    statusMap ->
                        statusMap.entrySet().stream()
                            .collect(
                                Collectors.groupingBy(
                                    Map.Entry::getValue, Collectors.counting())))));
  }
}
