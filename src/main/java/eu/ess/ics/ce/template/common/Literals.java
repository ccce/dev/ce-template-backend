/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.common;

import java.util.regex.Pattern;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public interface Literals {

  String TEMPLATE_EXTENSION = ".mustache";
  String CONFIG_EXTENSION = ".json";
  String FILE_NAME_PREFIX = "^[a-zA-Z0-9_\\-]+";
  String VALID_FILE_NAME = "^[a-zA-Z0-9_\\-]+\\.[a-zA-Z]+$";
  String DB_FILE_EXTENSION = ".db";
  String IOCSH_FILE_EXTENSION = ".iocsh";
  String TEMPLATE_FILE_EXTENSION = ".template";
  String CMD_FILE = "st.cmd";
  String SUBSTITUTION_EXTENSION = ".substitutions";
  String PLC_EXTENSION = ".plc";
  String ENVIRONMENT_FILE = "environment.yml";

  String DB_FILE_PATTERN = FILE_NAME_PREFIX + DB_FILE_EXTENSION + "$";
  String IOCSH_FILE_PATTERN = FILE_NAME_PREFIX + IOCSH_FILE_EXTENSION + "$";
  String TEMPLATE_FILE_PATTERN = FILE_NAME_PREFIX + TEMPLATE_FILE_EXTENSION + "$";
  String ST_CMD_TEMPLATE_NAME = CMD_FILE + TEMPLATE_EXTENSION;
  String IOCSH_TEMPLATE_NAME_PATTERN =
      FILE_NAME_PREFIX + IOCSH_FILE_EXTENSION + TEMPLATE_EXTENSION + "$";
  String DB_TEMPLATE_NAME_PATTERN = FILE_NAME_PREFIX + DB_FILE_EXTENSION + TEMPLATE_EXTENSION + "$";
  String TEMPLATE_TEMPLATE_NAME_PATTERN =
      FILE_NAME_PREFIX + TEMPLATE_FILE_EXTENSION + TEMPLATE_EXTENSION + "$";
  String SUBSTITUTION_FILE_PATTERN = FILE_NAME_PREFIX + SUBSTITUTION_EXTENSION + "$";
  String PLC_FILE_PATTERN = FILE_NAME_PREFIX + PLC_EXTENSION + "$";
  String SCHEMA_NAME = "config-schema.json";
  String TEMPLATE_OUTPUT_PATH_PREFIX = ".factory/template/";
  String CONFIG_OUTPUT_PATH = ".factory/config.json";
  String IOC_JSON = "ioc.json";
  String OUTPUT_PROJECT_PREFIX = "e3-ioc-";
  String VERSION_PREFIX = "v";
  String README_FILE = "README.md";
  String README_FILE_TEMPLATE = "README.md" + TEMPLATE_EXTENSION;
  String GITIGNORE_FILE = ".gitignore";
  String GITIGNORE_FILE_CONTENT = "*.swp";

  Pattern iocshTemplatePattern = Pattern.compile(IOCSH_TEMPLATE_NAME_PATTERN);
  Pattern dbTemplatePattern = Pattern.compile(DB_TEMPLATE_NAME_PATTERN);
  Pattern templateTemplatePattern = Pattern.compile(TEMPLATE_TEMPLATE_NAME_PATTERN);

  Pattern iocshPattern = Pattern.compile(IOCSH_FILE_PATTERN);
  Pattern dbPattern = Pattern.compile(DB_FILE_PATTERN);
  Pattern templatePattern = Pattern.compile(TEMPLATE_FILE_PATTERN);
  Pattern substitutionPattern = Pattern.compile(SUBSTITUTION_FILE_PATTERN);
  Pattern allowedFileNamePattern = Pattern.compile(VALID_FILE_NAME);
  Pattern plcPattern = Pattern.compile(PLC_FILE_PATTERN);
}
