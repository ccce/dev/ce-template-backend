/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.api.v1;

import eu.ess.ics.ce.template.configuration.exception.GeneralException;
import eu.ess.ics.ce.template.rest.model.git.response.FileContentResponse;
import eu.ess.ics.ce.template.rest.model.git.response.GitProject;
import eu.ess.ics.ce.template.rest.model.git.response.GitReference;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/repositories")
public interface IGitIntegration {
  @Operation(summary = "List Type projects")
  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Template projects",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = GitProject.class))))
      })
  @GetMapping(
      value = "/type_projects",
      produces = {"application/json"})
  List<GitProject> listTypeProjects();

  @Operation(summary = "List Config projects")
  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "503",
            description = "Remote service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Config projects",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = GitProject.class))))
      })
  @GetMapping(
      value = "/config_projects",
      produces = {"application/json"})
  List<GitProject> listConfigProjects();

  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Git repo not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of Tags and CommitIds for a specific GitLab repo",
            content =
                @Content(
                    array = @ArraySchema(schema = @Schema(implementation = GitReference.class))))
      })
  @GetMapping(
      value = "/{project_id}/tags_and_commits",
      produces = {"application/json"})
  List<GitReference> listTagsAndCommitIds(
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId,
      @Parameter(description = "Git reference") @RequestParam(required = false) String reference);

  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Git repo not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "List of files created in a specific GitLab repo, and reference",
            content =
                @Content(array = @ArraySchema(schema = @Schema(implementation = String.class))))
      })
  @GetMapping(
      value = "/{project_id}/files",
      produces = {"application/json"})
  List<String> listProjectFiles(
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId,
      @Parameter(description = "Git reference") @RequestParam(required = false) String reference);

  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Git repo/file not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "451",
            description = "File access denied",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Content of a template/config file",
            content = @Content(schema = @Schema(implementation = FileContentResponse.class)))
      })
  @GetMapping(
      value = "/file_content",
      produces = {"application/json"})
  FileContentResponse fetchFileContent(
      @Parameter(description = "Git project ID") @RequestParam(value = "project_id") long projectId,
      @Parameter(description = "Git reference - optional")
          @RequestParam(value = "project_reference", required = false)
          String projectRef,
      @Parameter(description = "Filename - optional")
          @RequestParam(value = "file_name", required = false)
          String fileName);

  @Operation(summary = "Git project information")
  @Tag(name = "Repositories")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "404",
            description = "Git repo not found",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "503",
            description = "Git exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "500",
            description = "Service exception",
            content = @Content(schema = @Schema(implementation = GeneralException.class))),
        @ApiResponse(
            responseCode = "200",
            description = "Git project information",
            content = @Content(schema = @Schema(implementation = GitProject.class)))
      })
  @GetMapping(
      value = "/{project_id}/info",
      produces = {"application/json"})
  GitProject getProjectInformation(
      @Parameter(in = ParameterIn.PATH, description = "Git repo project ID", required = true)
          @PathVariable("project_id")
          long projectId);
}
