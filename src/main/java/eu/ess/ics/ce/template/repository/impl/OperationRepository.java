/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.repository.impl;

import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Repository
public class OperationRepository implements IOperationRepository {

  @PersistenceContext private EntityManager em;

  @Override
  public void createOperation(OperationEntity operationEntity) {
    em.persist(operationEntity);
  }

  @Override
  public void updateOperation(OperationEntity operationEntity) {
    em.merge(operationEntity);
  }

  @Override
  public long countForPaging(String query, Long iocTypeId, String user) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<Long> cq = cb.createQuery(Long.class);
    Root<OperationEntity> from = cq.from(OperationEntity.class);
    Expression<Long> count = cb.count(from);
    CriteriaQuery<Long> select = cq.select(count);

    return pagingQuery(cb, select, from, cq, query, iocTypeId, user).getSingleResult();
  }

  @Override
  public List<OperationEntity> findOperations(
      String query, Long iocTypeId, String user, Integer page, Integer limit) {
    CriteriaBuilder cb = em.getCriteriaBuilder();

    CriteriaQuery<OperationEntity> cq = cb.createQuery(OperationEntity.class);
    Root<OperationEntity> from = cq.from(OperationEntity.class);
    CriteriaQuery<OperationEntity> select = cq.select(from);

    // Default order
    cq.orderBy(cb.desc(from.get("createdAt")));

    TypedQuery<OperationEntity> q = pagingQuery(cb, select, from, cq, query, iocTypeId, user);

    if (limit != null) {
      q.setFirstResult(page * limit);
      q.setMaxResults(limit);
    }

    return q.getResultList();
  }

  @Override
  public OperationEntity findOperationById(Long operationId) {
    return em.createQuery("FROM OperationEntity o WHERE o.id = :pOpId", OperationEntity.class)
        .setParameter("pOpId", operationId)
        .getSingleResult();
  }

  @Override
  public void deleteOrphanOperations() {
    em.createQuery(
            "DELETE FROM OperationEntity o WHERE o.id NOT IN "
                + "(SELECT il.operation.id FROM IocInstanceLogEntity il) ")
        .executeUpdate();
  }

  @Override
  public void deleteOperation(long operationId) {
    em.createQuery("DELETE FROM OperationEntity o WHERE o.id = :pOpId")
        .setParameter("pOpId", operationId)
        .executeUpdate();
  }

  @Override
  public OperationEntity findLatestOperation(long typeId) {
    return em
        .createQuery(
            "FROM OperationEntity op WHERE op.iocType.id = :pTypeId ORDER BY op.version DESC",
            OperationEntity.class)
        .setParameter("pTypeId", typeId)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public void removePredecessorLinkForType(long iocTypeId) {
    em.createQuery(
            "UPDATE OperationEntity SET predecessorType = NULL WHERE predecessorType.id = :pTypeId ")
        .setParameter("pTypeId", iocTypeId)
        .executeUpdate();
  }

  private <T, R> TypedQuery<T> pagingQuery(
      CriteriaBuilder cb,
      CriteriaQuery<T> select,
      Root<R> from,
      CriteriaQuery<T> cq,
      String query,
      Long iocTypeId,
      String user) {
    List<Predicate> predicates = new ArrayList<>();

    if (!StringUtils.isEmpty(query)) {
      predicates.add(
          cb.or(
              cb.like(cb.lower(from.get("gitTag")), "%" + query.toLowerCase() + "%"),
              cb.like(cb.lower(from.get("typeName")), "%" + query.toLowerCase() + "%"),
              cb.like(cb.lower(from.get("createdBy")), "%" + query.toLowerCase() + "%")));
    }

    if (!StringUtils.isEmpty(user)) {
      predicates.add(cb.like(cb.lower(from.get("createdBy")), "%" + user.toLowerCase() + "%"));
    }

    if (iocTypeId != null) {
      predicates.add(cb.equal(from.get("iocType").get("id"), iocTypeId));
    }

    select.where(cb.and(predicates.toArray(new Predicate[0])));
    return em.createQuery(cq);
  }
}
