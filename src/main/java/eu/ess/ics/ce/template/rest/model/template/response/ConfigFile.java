package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record ConfigFile(
    @JsonProperty("file_name") String fileName,
    @JsonProperty("is_valid") boolean valid,
    @JsonProperty("error_message") String errorMessage) {
  public ConfigFile(String fileName, boolean valid, String errorMessage) {
    this.fileName = fileName;
    this.valid = valid;
    this.errorMessage = errorMessage;
  }

  @Override
  public String fileName() {
    return fileName;
  }

  @Override
  public boolean valid() {
    return valid;
  }

  @Override
  public String errorMessage() {
    return errorMessage;
  }
}
