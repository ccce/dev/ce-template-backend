/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.model.jobs.response;

import eu.ess.ics.ce.template.rest.model.PagedResponse;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class PagedJobResponse extends PagedResponse {
  private final List<Job> jobs;

  public PagedJobResponse(
      List<Job> jobs, long totalCount, int listSize, int pageNumber, int limit) {
    super(totalCount, listSize, pageNumber, limit);
    this.jobs = jobs;
  }

  public List<Job> getJobs() {
    return jobs;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PagedJobResponse that = (PagedJobResponse) o;
    return Objects.equals(jobs, that.jobs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobs);
  }

  @Override
  public String toString() {
    return "PagedOperationResponse{" + "jobs=" + jobs + '}';
  }
}
