package eu.ess.ics.ce.template.rest.model.git.response;

public enum ReferenceType {
  TAG,
  COMMIT,
  UNKNOWN
}
