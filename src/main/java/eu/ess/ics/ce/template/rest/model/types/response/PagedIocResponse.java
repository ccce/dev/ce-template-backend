package eu.ess.ics.ce.template.rest.model.types.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.ess.ics.ce.template.rest.model.PagedResponse;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class PagedIocResponse extends PagedResponse {
  @JsonProperty("iocs")
  private final List<Ioc> iocs;

  @NotNull
  @JsonProperty("type_id")
  private final Long typeId;

  public PagedIocResponse(
      Long typeId, List<Ioc> iocs, long totalCount, int listSize, int pageNumber, Integer limit) {
    super(totalCount, listSize, pageNumber, limit);
    this.iocs = iocs;
    this.typeId = typeId;
  }

  public List<Ioc> getIocs() {
    return iocs;
  }

  public Long getTypeId() {
    return typeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof PagedIocResponse that)) {
      return false;
    }
    return Objects.equals(iocs, that.iocs) && Objects.equals(typeId, that.typeId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(iocs, typeId);
  }

  @Override
  public String toString() {
    return "PagedIocResponse{" + "iocs=" + iocs + "} " + super.toString();
  }
}
