package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Type extends TypeBase {
  @JsonProperty("type_project_url")
  private final String typeProjectUrl;

  @JsonProperty("configuration_project_url")
  private final String configurationProjectUrl;

  public Type(
      @NotNull Long id,
      @NotNull String name,
      String description,
      Long typeProjectId,
      String typeProjectUrl,
      Long configurationProjectId,
      String configurationProjectUrl,
      String createdBy,
      ZonedDateTime createdAt,
      @NotNull long numberOfIocs,
      String latestIocTag,
      Long latestOperationId,
      @NotNull boolean deleted) {
    super(
        id,
        name,
        description,
        typeProjectId,
        configurationProjectId,
        createdBy,
        createdAt,
        numberOfIocs,
        latestIocTag,
        latestOperationId,
        deleted);
    this.typeProjectUrl = typeProjectUrl;
    this.configurationProjectUrl = configurationProjectUrl;
  }

  public String getTypeProjectUrl() {
    return typeProjectUrl;
  }

  public String getConfigurationProjectUrl() {
    return configurationProjectUrl;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    Type type = (Type) o;
    return Objects.equals(typeProjectUrl, type.typeProjectUrl)
        && Objects.equals(configurationProjectUrl, type.configurationProjectUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), typeProjectUrl, configurationProjectUrl);
  }

  @Override
  public String toString() {
    return "Type{"
        + "typeProjectUrl='"
        + typeProjectUrl
        + '\''
        + ", configurationProjectUrl='"
        + configurationProjectUrl
        + '\''
        + '}';
  }
}
