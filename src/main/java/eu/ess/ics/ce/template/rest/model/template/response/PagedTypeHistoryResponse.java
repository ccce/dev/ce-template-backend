package eu.ess.ics.ce.template.rest.model.template.response;

import eu.ess.ics.ce.template.rest.model.PagedResponse;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public class PagedTypeHistoryResponse extends PagedResponse {
  private final List<TypeHistoryEntry> historyEntries;

  public PagedTypeHistoryResponse(
      List<TypeHistoryEntry> historyEntries,
      long totalCount,
      int listSize,
      int pageNumber,
      int limit) {
    super(totalCount, listSize, pageNumber, limit);
    this.historyEntries = historyEntries;
  }

  public List<TypeHistoryEntry> getHistoryEntries() {
    return historyEntries;
  }
}
