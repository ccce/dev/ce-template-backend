/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.model.jobs.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import eu.ess.ics.ce.template.rest.model.PagedResponse;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@JsonPropertyOrder({"operation_id", "total_count", "list_size", "page", "page_size", "instances"})
public class JobInstanceListResponse extends PagedResponse {
  @NotNull
  @JsonProperty("job_id")
  private final Long jobId;

  private final List<JobInstance> instances;

  public JobInstanceListResponse(
      Long jobId,
      List<JobInstance> instances,
      long totalCount,
      int listSize,
      int pageNumber,
      int limit) {
    super(totalCount, listSize, pageNumber, limit);
    this.jobId = jobId;
    this.instances = instances;
  }

  public Long getJobId() {
    return jobId;
  }

  public List<JobInstance> getInstances() {
    return instances;
  }
}
