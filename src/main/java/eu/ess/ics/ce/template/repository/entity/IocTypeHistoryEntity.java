package eu.ess.ics.ce.template.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.ZonedDateTime;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Entity
@Table(name = "ioc_type_history")
public class IocTypeHistoryEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "type_id")
  private Long typeId;

  @Column(name = "type_name")
  private String typeName;

  @Column(name = "createdBy")
  private String owner;

  @Column(name = "template_git_project_id")
  private Long templateGitProjectId;

  @Column(name = "config_git_project_id")
  private Long configGitProjectId;

  @Column(name = "updated_by")
  private String updatedBy;

  @Column(name = "updated_at")
  private ZonedDateTime updatedAt;

  @Column(name = "event")
  private String event;

  public void setId(Long id) {
    this.id = id;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public void setTemplateGitProjectId(Long templateGitProjectId) {
    this.templateGitProjectId = templateGitProjectId;
  }

  public void setConfigGitProjectId(Long configGitProjectId) {
    this.configGitProjectId = configGitProjectId;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public void setUpdatedAt(ZonedDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public Long getId() {
    return id;
  }

  public Long getTypeId() {
    return typeId;
  }

  public String getTypeName() {
    return typeName;
  }

  public String getOwner() {
    return owner;
  }

  public Long getTemplateGitProjectId() {
    return templateGitProjectId;
  }

  public Long getConfigGitProjectId() {
    return configGitProjectId;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public ZonedDateTime getUpdatedAt() {
    return updatedAt;
  }

  public String getEvent() {
    return event;
  }
}
