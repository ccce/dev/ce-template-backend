package eu.ess.ics.ce.template.repository.api;

import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public interface IIocInstanceRepository {

  @Transactional
  void create(IocInstanceEntity iocInstance);

  @Transactional
  void updateInstance(IocInstanceEntity iocInstance);

  List<IocInstanceEntity> findAll(
      Long templateId, String query, String user, int page, Integer limit);

  List<IocInstanceEntity> findLatestInstances(
      long templateId, String query, int page, Integer limit);

  List<IocInstanceEntity> findLatestInstances(
      long templateId, String query, int page, Integer limit, Boolean sortByVersion);

  long countAllLatestInstances();

  long countLatestInstancesForIocType(long typeId);

  long countInstancesForPaging(long templateId, String query);

  long countInstancesForOperation(final long operationId);

  long countForPaging(Long templateId, String query, String user);

  Long findMaxVersionForRepoId(long repoId);

  List<IocInstanceEntity> findByOperationId(Long operationId);

  IocInstanceEntity findById(long instanceId);

  List<IocInstanceEntity> findByOperationId(Long operationId, Integer page, Integer limit);

  boolean checkInstanceIsCreated(Long typeId, String configName);

  List<IocInstanceEntity> findQueuedIocInstances();

  boolean hasInstanceRepositoryNewlyCreated(IocInstanceEntity instance);

  boolean instanceExists(long typeId, long gitRepoId, String configName);

  @Transactional
  void deleteInstances(long typeId, long gitRepoId, String configName);

  @Transactional
  void deleteInstance(long instanceId);

  Long findOutputProjectForTypeAndConfig(long typeId, String configurationName);

  String latestTag(long typeId);

  IocInstanceEntity findByTypeAndName(Long typeId, String configName);

  OperationEntity fetchOperation(long IocInstanceId);

  long countInstancesWithRepoId(long repoId);
}
