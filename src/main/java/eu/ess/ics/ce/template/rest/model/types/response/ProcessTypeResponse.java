package eu.ess.ics.ce.template.rest.model.types.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class ProcessTypeResponse {
  @JsonProperty("project_id")
  private final Long projectId;

  @JsonProperty("project_name")
  private final String projectName;

  private final String reference;

  private final String url;

  public ProcessTypeResponse(Long projectId, String projectName, String reference, String url) {
    this.projectId = projectId;
    this.projectName = projectName;
    this.reference = reference;
    this.url = url;
  }

  public Long getProjectId() {
    return projectId;
  }

  public String getUrl() {
    return url;
  }

  public String getReference() {
    return reference;
  }

  public String getProjectName() {
    return projectName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProcessTypeResponse that = (ProcessTypeResponse) o;
    return Objects.equals(projectId, that.projectId)
        && Objects.equals(projectName, that.projectName)
        && Objects.equals(reference, that.reference)
        && Objects.equals(url, that.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(projectId, projectName, reference, url);
  }

  @Override
  public String toString() {
    return "ProcessTypeResponse{"
        + "projectId="
        + projectId
        + ", projectName='"
        + projectName
        + '\''
        + ", reference='"
        + reference
        + '\''
        + ", url='"
        + url
        + '\''
        + '}';
  }
}
