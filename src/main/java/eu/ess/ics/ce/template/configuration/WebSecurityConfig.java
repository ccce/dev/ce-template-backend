/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.configuration;

import static org.springframework.security.config.Customizer.withDefaults;

import eu.ess.ics.ce.template.common.Constants;
import eu.ess.ics.ce.template.common.conversion.AuthConversionUtil;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.util.matcher.RequestHeaderRequestMatcher;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class WebSecurityConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(WebSecurityConfig.class);

  public static final String ROLE_TEMPLATING_TOOL_ADMIN = "CE_TemplatingToolAdmin";
  public static final String ROLE_TEMPLATING_TOOL_INTEGRATOR = "CE_TemplatingToolIntegrator";
  public static final String IS_ADMIN = "hasAuthority('" + ROLE_TEMPLATING_TOOL_ADMIN + "')";
  public static final String IS_INTEGRATOR =
      "hasAuthority('" + ROLE_TEMPLATING_TOOL_INTEGRATOR + "')";
  public static final String IS_ADMIN_OR_INTEGRATOR = IS_ADMIN + " or " + IS_INTEGRATOR;
  public static final List<String> ALLOWED_ROLES_TO_LOGIN =
      Arrays.asList(
          WebSecurityConfig.ROLE_TEMPLATING_TOOL_ADMIN,
          WebSecurityConfig.ROLE_TEMPLATING_TOOL_INTEGRATOR);

  @Value("${springdoc.api-docs.path}")
  private String apiPath;

  @Value("${springdoc.swagger-ui.path}")
  private String swaggerPath;

  @Value("${ui.baseurl}")
  private String uiBaseUrl;

  @Autowired private OidcClientInitiatedLogoutSuccessHandler logoutSuccessHandler;

  @Autowired private ExpiredTokenFilter expiredTokenFilter;

  @Bean
  GrantedAuthoritiesMapper userAuthoritiesMapper() {
    return (authorities) -> {
      Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

      authorities.forEach(
          authority -> {
            if (authority instanceof OidcUserAuthority oiddAuth) {
              OidcUserAuthority oidcUserAuthority = (OidcUserAuthority) authority;

              OidcUserInfo userInfo = oidcUserAuthority.getUserInfo();

              // Map the claims found in idToken and/or userInfo
              // to one or more GrantedAuthority's and add it to mappedAuthorities
              if (userInfo.getClaims().containsKey(Constants.PING_MEMBEROF)) {
                List<String> roleList =
                    AuthConversionUtil.convertAuthorities(
                        userInfo.getClaim(Constants.PING_MEMBEROF));

                for (String role : roleList) {
                  mappedAuthorities.add(new SimpleGrantedAuthority(role));
                }
              }

            } else if (authority instanceof OAuth2UserAuthority auth) {
              Map<String, Object> userAttributes = auth.getAttributes();

              // Map the attributes found in userAttributes
              // to one or more GrantedAuthority's and add it to mappedAuthorities
              if (userAttributes.containsKey(Constants.PING_MEMBEROF)) {

                List<String> roleList =
                    AuthConversionUtil.convertAuthorities(
                        userAttributes.get(Constants.PING_MEMBEROF));

                for (String role : roleList) {
                  mappedAuthorities.add(new SimpleGrantedAuthority(role));
                }
              }
            }
          });

      return mappedAuthorities;
    };
  }

  @Bean
  SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
    // We don't need CSRF
    httpSecurity
        .csrf(csrf -> csrf.disable())
        // dont authenticate this particular request
        .authorizeHttpRequests(
            requests ->
                requests
                    // openAPI web UI
                    .requestMatchers(
                        "/v3/api-docs/**",
                        "/swagger-ui/**",
                        "/api/swagger-ui/**",
                        swaggerPath,
                        apiPath + "/**",
                        "/login/**",
                        "/logout/**",
                        "/oauth2/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/healthcheck")
                    .permitAll()
                    // actuator endpoints
                    .requestMatchers(HttpMethod.GET, "/api/actuator/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/types/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/repositories/**")
                    .permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/v1/jobs/**")
                    .permitAll()
                    // all other requests need to be authenticated
                    .anyRequest()
                    .authenticated())
        .addFilterBefore(expiredTokenFilter, AbstractPreAuthenticatedProcessingFilter.class)
        .oauth2Login(
            oauth2Login -> {
              try {
                oauth2Login
                    .userInfoEndpoint(
                        userInfoEndpoint ->
                            userInfoEndpoint.userAuthoritiesMapper(this.userAuthoritiesMapper()))
                    .and()
                    .logout(
                        logout ->
                            logout
                                .logoutUrl(Constants.OAUTH_LOGOUT_URL)
                                .clearAuthentication(true)
                                .invalidateHttpSession(true)
                                .deleteCookies("JSESSIONID")
                                .logoutSuccessHandler(logoutSuccessHandler)
                                .logoutSuccessUrl(uiBaseUrl));
              } catch (Exception e) {
                LOGGER.error("Error while trying to communicate with Oauth2 server", e);
                throw new RuntimeException(e);
              }
            })
        .exceptionHandling(
            handling ->
                handling.defaultAuthenticationEntryPointFor(
                    new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED),
                    new RequestHeaderRequestMatcher("X-Requested-With", "XMLHttpRequest")));
    httpSecurity.cors(withDefaults());

    return httpSecurity.build();
  }
}
