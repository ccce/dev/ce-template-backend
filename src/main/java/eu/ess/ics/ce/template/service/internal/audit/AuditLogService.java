package eu.ess.ics.ce.template.service.internal.audit;

import eu.ess.ics.ce.template.common.conversion.PagingUtil;
import eu.ess.ics.ce.template.repository.api.IIocTypeHistoryRepository;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.repository.entity.IocTypeHistoryEntity;
import eu.ess.ics.ce.template.rest.model.template.response.PagedTypeHistoryResponse;
import eu.ess.ics.ce.template.rest.model.template.response.TypeHistoryEntry;
import eu.ess.ics.ce.template.service.internal.PagingLimitDto;
import eu.ess.ics.ce.template.service.internal.audit.dto.AuditEvent;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class AuditLogService {

  private final IIocTypeHistoryRepository iocTypeHistoryRepository;
  private final PagingUtil utils;

  public AuditLogService(
      final IIocTypeHistoryRepository iocTypeHistoryRepository, final PagingUtil utils) {
    this.iocTypeHistoryRepository = iocTypeHistoryRepository;
    this.utils = utils;
  }

  public PagedTypeHistoryResponse getIocTypeHistory(long typeId, Integer page, Integer limit) {
    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit);
    List<TypeHistoryEntry> historyEntries =
        convertHistories(
            iocTypeHistoryRepository.findTypeHistory(
                typeId, pagingLimitDto.getPage(), pagingLimitDto.getLimit()));

    long totalCount = iocTypeHistoryRepository.countForPaging(typeId);

    return new PagedTypeHistoryResponse(
        historyEntries,
        totalCount,
        historyEntries.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit());
  }

  public void logIocTypeCreation(final IocTypeEntity iocType, final String userName) {
    IocTypeHistoryEntity historyEntity = toHistoryEntry(iocType, userName, AuditEvent.CREATE);
    iocTypeHistoryRepository.create(historyEntity);
  }

  public void logIocTypeModification(final IocTypeEntity iocType, final String userName) {
    IocTypeHistoryEntity historyEntity = toHistoryEntry(iocType, userName, AuditEvent.UPDATE);
    iocTypeHistoryRepository.create(historyEntity);
  }

  public void logIocTypeDeletion(final IocTypeEntity iocType, final String userName) {
    IocTypeHistoryEntity historyEntity = toHistoryEntry(iocType, userName, AuditEvent.DELETE);
    iocTypeHistoryRepository.create(historyEntity);
  }

  public void logIocTypeRestoration(final IocTypeEntity iocType, final String userName) {
    IocTypeHistoryEntity historyEntity = toHistoryEntry(iocType, userName, AuditEvent.RESTORE);
    iocTypeHistoryRepository.create(historyEntity);
  }

  public void deleteIocTypeHistory(final long typeId) {
    iocTypeHistoryRepository.deleteIocTypeHistory(typeId);
  }

  private IocTypeHistoryEntity toHistoryEntry(
      final IocTypeEntity iocType, final String userName, final AuditEvent auditEvent) {
    IocTypeHistoryEntity historyEntity = new IocTypeHistoryEntity();
    historyEntity.setTypeId(iocType.getId());
    historyEntity.setTypeName(iocType.getName());
    historyEntity.setOwner(iocType.getCreatedBy());
    if (AuditEvent.CREATE.equals(auditEvent)) {
      historyEntity.setUpdatedAt(iocType.getCreatedAt());
    } else {
      historyEntity.setUpdatedAt(ZonedDateTime.now());
    }
    historyEntity.setTemplateGitProjectId(iocType.getTemplateGitProjectId());
    historyEntity.setConfigGitProjectId(iocType.getConfigGitProjectId());
    historyEntity.setEvent(auditEvent.name());
    historyEntity.setUpdatedBy(userName);
    return historyEntity;
  }

  private static List<TypeHistoryEntry> convertHistories(
      List<IocTypeHistoryEntity> historyEntities) {
    return historyEntities.stream().map(AuditLogService::convertHistory).toList();
  }

  private static TypeHistoryEntry convertHistory(IocTypeHistoryEntity historyEntity) {
    return new TypeHistoryEntry(
        historyEntity.getTypeId(),
        historyEntity.getTypeName(),
        historyEntity.getOwner(),
        historyEntity.getTemplateGitProjectId(),
        historyEntity.getConfigGitProjectId(),
        historyEntity.getUpdatedBy(),
        historyEntity.getUpdatedAt(),
        AuditEvent.valueOf(historyEntity.getEvent()));
  }
}
