package eu.ess.ics.ce.template.repository.impl;

import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Repository
public class InstanceLogRepository implements IInstanceLogRepository {

  @PersistenceContext private EntityManager em;

  @Override
  public IocInstanceLogEntity create(IocInstanceLogEntity iocInstanceLogEntity) {
    return em.merge(iocInstanceLogEntity);
  }

  @Override
  public void deleteLogsRelatedToInstance(long typeId, long gitRepoId, String configName) {
    em.createQuery(
            "DELETE FROM IocInstanceLogEntity log WHERE log.iocInstance = "
                + " (FROM IocInstanceEntity ie WHERE ie.iocType.id = :pTypeId AND "
                + " lower(ie.configurationName) = lower( :pConfigName ) AND "
                + " ie.gitProjectId = :pGitRepoId)")
        .setParameter("pTypeId", typeId)
        .setParameter("pConfigName", configName)
        .setParameter("pGitRepoId", gitRepoId)
        .executeUpdate();
  }

  @Override
  public List<IocInstanceLogEntity> findByOperationId(
      long operationId, Integer page, Integer limit) {
    TypedQuery<IocInstanceLogEntity> query =
        em.createQuery(
                "SELECT log FROM IocInstanceLogEntity log "
                    + "WHERE log.operation.id = :operationId",
                IocInstanceLogEntity.class)
            .setParameter("operationId", operationId);

    if (limit != null) {
      query.setFirstResult(page * limit).setMaxResults(limit);
    }

    return query.getResultList();
  }

  @Override
  public long countForPaging(long operationId) {
    return em.createQuery(
            "SELECT COUNT(*) FROM IocInstanceLogEntity log WHERE log.operation.id = :operationId",
            Long.class)
        .setParameter("operationId", operationId)
        .getSingleResult();
  }

  @Override
  public List<IocInstanceLogEntity> findLogsForIocInstance(long operationId, long instanceId) {
    return em.createQuery(
            "SELECT log FROM IocInstanceLogEntity log WHERE log.iocInstance.id = :pInstanceId AND "
                + " log.operation.id = :pOperationId",
            IocInstanceLogEntity.class)
        .setParameter("pInstanceId", instanceId)
        .setParameter("pOperationId", operationId)
        .getResultList();
  }

  @Override
  public void deleteLogsForIocInstance(long instanceId) {
    em.createQuery("DELETE FROM IocInstanceLogEntity log WHERE log.iocInstance.id = :pInstanceId")
        .setParameter("pInstanceId", instanceId)
        .executeUpdate();
  }

  @Override
  public void deleteLogsForOperation(long operationId) {
    em.createQuery("DELETE FROM IocInstanceLogEntity log WHERE log.operation.id = :pOperationId")
        .setParameter("pOperationId", operationId)
        .executeUpdate();
  }

  @Override
  public void updateLogEntry(IocInstanceLogEntity iocInstanceLogEntity) {
    if (iocInstanceLogEntity != null) {
      em.merge(iocInstanceLogEntity);
    }
  }

  public IocInstanceLogEntity findLastLogEntry(long instanceId, long operationId) {
    return em
        .createQuery(
            "FROM IocInstanceLogEntity WHERE operation.id = :pOperationId AND "
                + " iocInstance.id = :pInstanceId ORDER BY createdAt DESC",
            IocInstanceLogEntity.class)
        .setParameter("pOperationId", operationId)
        .setParameter("pInstanceId", instanceId)
        .getResultList()
        .stream()
        .findFirst()
        .orElse(null);
  }

  @Override
  public List<IocInstanceLogEntity> findLastLogEntriesForOperation(long operationId) {
    return em.createQuery(
            "FROM IocInstanceLogEntity iil WHERE (iil.createdAt, iil.iocInstance.id) IN "
                + " (SELECT MAX(iil2.createdAt ), iil2.iocInstance.id  FROM IocInstanceLogEntity iil2 WHERE "
                + " iil2.operation.id = :pOperationId group by iil2.iocInstance.id )",
            IocInstanceLogEntity.class)
        .setParameter("pOperationId", operationId)
        .getResultList();
  }

  @Override
  public List<IocInstanceLogEntity> findLastLogEntries() {
    return em.createQuery(
            "FROM IocInstanceLogEntity iil WHERE (iil.createdAt, iil.iocInstance.id, iil.operation.id) IN "
                + " (SELECT MAX(iil2.createdAt ), iil2.iocInstance.id, iil2.operation.id  FROM IocInstanceLogEntity iil2 "
                + " group by iil2.iocInstance.id, iil2.operation.id )",
            IocInstanceLogEntity.class)
        .getResultList();
  }
}
