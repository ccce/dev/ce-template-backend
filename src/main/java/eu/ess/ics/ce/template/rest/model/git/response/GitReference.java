/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.rest.model.git.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class GitReference {
  @NotNull private final String reference;

  @JsonProperty("short_reference")
  private final String shortReference;

  private final String description;

  @NotNull
  @JsonProperty("commit_date")
  private final Date commitDate;

  @NotNull private final ReferenceType type;

  public GitReference(
      String reference,
      String shortReference,
      String description,
      Date commitDate,
      ReferenceType type) {
    this.reference = reference;
    this.shortReference = shortReference;
    this.description = description;
    this.commitDate = commitDate;
    this.type = type;
  }

  public GitReference(String reference) {
    this.reference = reference;
    this.shortReference = reference;
    this.description = "";
    this.commitDate = null;
    this.type = null;
  }

  public String getReference() {
    return reference;
  }

  public String getShortReference() {
    return shortReference;
  }

  public String getDescription() {
    return description;
  }

  public Date getCommitDate() {
    return commitDate;
  }

  public ReferenceType getType() {
    return type;
  }
}
