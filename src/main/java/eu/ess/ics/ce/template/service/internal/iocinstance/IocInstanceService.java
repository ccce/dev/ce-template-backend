/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal.iocinstance;

import static eu.ess.ics.ce.template.common.Constants.FINISHED_STATUSES;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.ess.ics.ce.template.common.Literals;
import eu.ess.ics.ce.template.common.Utils;
import eu.ess.ics.ce.template.common.conversion.PagingUtil;
import eu.ess.ics.ce.template.common.validation.JsonValidator;
import eu.ess.ics.ce.template.exceptions.ConfigListValidationException;
import eu.ess.ics.ce.template.exceptions.ConfigValidationException;
import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.FileRestrictionException;
import eu.ess.ics.ce.template.exceptions.InvalidJsonSyntaxException;
import eu.ess.ics.ce.template.exceptions.UnprocessableRequestException;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.helper.request.CalculateNewIocsRequest;
import eu.ess.ics.ce.template.rest.model.helper.response.CalculateNewIocsResponse;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceGenerationStatus;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceGenerationStep;
import eu.ess.ics.ce.template.rest.model.jobs.response.InstanceLogEntry;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobInstanceLogResponse;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.rest.model.template.response.ConfigFile;
import eu.ess.ics.ce.template.rest.model.template.response.TypeFile;
import eu.ess.ics.ce.template.rest.model.template.response.TypeFilesResponse;
import eu.ess.ics.ce.template.rest.model.types.NameAndGitRepo;
import eu.ess.ics.ce.template.rest.model.types.request.DeleteIocRequest;
import eu.ess.ics.ce.template.rest.model.types.request.IocFetchOption;
import eu.ess.ics.ce.template.rest.model.types.request.ProcessTypeRequest;
import eu.ess.ics.ce.template.rest.model.types.response.*;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.internal.PagingLimitDto;
import eu.ess.ics.ce.template.service.internal.TaskExecutorService;
import eu.ess.ics.ce.template.service.internal.jobs.LogService;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import eu.ess.ics.ce.template.service.internal.jobs.dto.InstanceStatus;
import eu.ess.ics.ce.template.service.internal.template.IocTypeService;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Tag;
import org.gitlab4j.api.models.TreeItem;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class IocInstanceService {

  private static final Logger LOGGER = LoggerFactory.getLogger(IocInstanceService.class);

  private static final String INVALID_TEMPLATE_PROJECT = "Invalid template project";
  private static final String INVALID_CONFIG_SCHEMA = "Invalid config-schema";
  private static final String INSTANCE_COMMIT_MESSAGE = "Generated by CE template";

  private final GitLabService gitLabService;
  private final IIocInstanceRepository iocInstanceRepository;
  private final IOperationRepository operationRepository;
  private final ITemplateRepository templateRepository;
  private final IocTypeService iocTypeService;
  private final PagingUtil utils;
  private final TaskExecutorService taskExecutorService;
  private final LogService logService;
  private final IInstanceLogRepository logRepository;

  @Value("${template.host.address}")
  private String hostAddress;

  public IocInstanceService(
      GitLabService gitLabService,
      IIocInstanceRepository iocInstanceRepository,
      IOperationRepository operationRepository,
      IocTypeService iocTypeService,
      PagingUtil utils,
      ITemplateRepository templateRepository,
      TaskExecutorService taskExecutorService,
      LogService logService,
      IInstanceLogRepository logRepository) {
    this.gitLabService = gitLabService;
    this.iocInstanceRepository = iocInstanceRepository;
    this.iocTypeService = iocTypeService;
    this.utils = utils;
    this.operationRepository = operationRepository;
    this.templateRepository = templateRepository;
    this.taskExecutorService = taskExecutorService;
    this.logService = logService;
    this.logRepository = logRepository;
  }

  private String extractTemplateFileName(String templateName) {
    if (StringUtils.isEmpty(templateName)) {
      return Literals.ST_CMD_TEMPLATE_NAME;
    } else {
      if (Utils.checkTemplateFileNameConvention(templateName)) {
        return templateName;
      }
    }

    throw new FileRestrictionException("Preview generation is restricted to .mustache files!");
  }

  public IocPreview processTemplatePreview(
      long templateId,
      String templateRevision,
      String templateName,
      String configRevision,
      String configName)
      throws GitLabApiException, IOException {

    String templateFileName = extractTemplateFileName(templateName);

    IocTypeEntity iocTypeEntity = iocTypeService.findTemplateById(templateId, true);
    Project templateProject =
        gitLabService.checkTemplateExists(iocTypeEntity.getTemplateGitProjectId());
    GitLabService.GitFile templateSchema =
        gitLabService.getFile(templateProject.getId(), Literals.SCHEMA_NAME, templateRevision);
    GitLabService.GitFile mustacheFile =
        gitLabService.getFile(templateProject.getId(), templateFileName, templateRevision);
    String schema = templateSchema.content();
    Project configProject = gitLabService.checkConfigExists(iocTypeEntity.getConfigGitProjectId());
    GitLabService.GitFile configFile =
        gitLabService.getFile(configProject.getId(), configName, configRevision);
    String config = configFile.content();

    // perform config file validation against template schema
    validateConfig(schema, config, false);

    return new IocPreview(
        Utils.generateProjectName(templateProject.getName(), configName),
        schema,
        substituteTemplate(mustacheFile.content(), config));
  }

  public OperationStartedResponse processTemplate(
      long typeId, ProcessTypeRequest request, String user)
      throws GitLabApiException, ConfigValidationException {

    IocTypeEntity iocTypeEntity = iocTypeService.findTemplateById(typeId, true);
    Project templateProject =
        gitLabService.checkTemplateExists(iocTypeEntity.getTemplateGitProjectId());
    String templateReference = request.getTypeReference();

    // get template-default branch if not specified
    if (StringUtils.isEmpty(templateReference)) {
      templateReference = templateProject.getDefaultBranch();
    }

    List<TreeItem> projectFiles =
        fetchAndValidateTemplateProjectFiles(templateProject, templateReference);

    GitLabService.GitFile templateSchema =
        gitLabService.getFile(templateProject.getId(), Literals.SCHEMA_NAME, templateReference);
    String schema = templateSchema.content();

    validateSchemaContent(
        schema,
        projectFiles.stream()
            .filter(f -> f.getType().equals(TreeItem.Type.BLOB))
            .map(TreeItem::getName)
            .toList());

    GitLabService.GitFile templateFile =
        gitLabService.getFile(
            iocTypeEntity.getTemplateGitProjectId(),
            Literals.ST_CMD_TEMPLATE_NAME,
            templateReference);
    String templateRevision = templateFile.commitId();

    Project configProject = gitLabService.checkConfigExists(iocTypeEntity.getConfigGitProjectId());
    // this config reference contains only the short commitId (or Tag)
    String configReference = request.getConfigReference();

    // get config-default branch if not specified
    if (StringUtils.isEmpty(configReference)) {
      configReference = configProject.getDefaultBranch();
    }

    List<String> configNames = request.getConfigNames();

    // getting configs if request part was empty
    if ((request.getConfigNames() == null) || (request.getConfigNames().isEmpty())) {
      configNames =
          extractConfigs(iocTypeEntity.getConfigGitProjectId(), request.getConfigReference());
    }

    // validating config file names
    IocTypeService.validateInstanceNames(
        configNames.stream()
            .map(n -> StringUtils.removeEndIgnoreCase(n, Literals.CONFIG_EXTENSION))
            .collect(Collectors.toList()));

    // validating ALL configs against the schema BEFORE creating new project
    validateConfigRevision(schema, configProject, configReference, configNames);

    return queueIocInstances(
        user,
        iocTypeEntity,
        templateProject,
        templateRevision,
        createTemplateMap(projectFiles, templateProject.getId(), templateReference),
        configProject,
        configReference,
        collectFilesToCopy(projectFiles, templateProject.getId(), templateReference),
        configNames);
  }

  public OperationStartedResponse queueIocInstances(
      final String user,
      final IocTypeEntity iocTypeEntity,
      final Project templateProject,
      final String templateRevision,
      final Map<TreeItem, String> templates,
      final Project configProject,
      final String configReference,
      final Map<TreeItem, String> filesToCopy,
      final List<String> configNames) {

    final Long nextVersion = getIocTypeNextVersionNum(iocTypeEntity);

    final String tagName = Literals.VERSION_PREFIX + nextVersion;

    iocTypeEntity.setMaxVersion(nextVersion);
    templateRepository.updateIocType(iocTypeEntity);

    final OperationEntity operationEntity = new OperationEntity();
    operationEntity.setIocType(iocTypeEntity);
    operationEntity.setTypeName(iocTypeEntity.getName());
    operationEntity.setGitTag(tagName);
    operationEntity.setCreatedBy(user);
    operationEntity.setCreatedAt(ZonedDateTime.now());
    operationEntity.setTemplateRevision(templateRevision);
    operationEntity.setConfigurationRevision(configReference);
    operationEntity.setVersion(nextVersion);
    operationEntity.setJobType(JobType.GENERATE_INSTANCE.name());

    operationRepository.createOperation(operationEntity);

    for (final String actualConfig : configNames) {

      IocInstanceEntity iocInstance =
          iocInstanceRepository.findByTypeAndName(
              iocTypeEntity.getId(), FilenameUtils.removeExtension(actualConfig));

      boolean hasToBeCreated = iocInstance == null;

      if (hasToBeCreated) {
        iocInstance = new IocInstanceEntity();

        iocInstance.setConfigurationName(FilenameUtils.removeExtension(actualConfig));
        iocInstance.setIocTypeName(iocTypeEntity.getName());
        iocInstance.setCreatedBy(user);
        iocInstance.setCreatedAt(ZonedDateTime.now());
        iocInstance.setIocType(iocTypeEntity);
        iocInstance.setInstanceName(
            Utils.generateProjectName(
                iocTypeEntity.getName(), FilenameUtils.removeExtension(actualConfig)));
      }

      iocInstance.setVersion(nextVersion);
      iocInstance.setStatus(GenerationStatus.QUEUED.toString());

      if (hasToBeCreated) {
        iocInstanceRepository.create(iocInstance);
      } else {
        iocInstanceRepository.updateInstance(iocInstance);
      }
      logService.createLogEntry(iocInstance, InstanceStatus.QUEUED, operationEntity);
      LOGGER.debug("Queued new ioc instance {}", iocInstance);

      IocInstanceEntity iocEnt = iocInstance;

      taskExecutorService.executeTask(
          operationEntity.getId(),
          () ->
              queueAndExecuteIocInstance(
                  iocEnt,
                  actualConfig,
                  operationEntity,
                  templateProject,
                  templates,
                  configProject,
                  filesToCopy,
                  tagName));
    }

    return new OperationStartedResponse(operationEntity.getId());
  }

  public void queueAndExecuteIocInstance(
      final IocInstanceEntity iocInstance,
      final String actConfig,
      final OperationEntity operationEntity,
      final Project templateProject,
      final Map<TreeItem, String> templates,
      final Project configProject,
      final Map<TreeItem, String> filesToCopy,
      final String tagName) {

    if (GenerationStatus.QUEUED.toString().equals(iocInstance.getStatus())) {
      iocInstance.setStatus(GenerationStatus.RUNNING.toString());
      iocInstanceRepository.updateInstance(iocInstance);
      logService.closeLastEntry(iocInstance.getId(), operationEntity.getId());
      logService.createLogEntry(iocInstance, InstanceStatus.STARTED, operationEntity);
      LOGGER.debug("Started ioc instance {}", iocInstance);
    }

    createOutputProject(
        iocInstance,
        actConfig,
        operationEntity,
        templateProject,
        templates,
        configProject,
        filesToCopy,
        tagName);
  }

  public void createOutputProject(
      final IocInstanceEntity iocInstanceEntity,
      final String actualConfig,
      final OperationEntity operationEntity,
      final Project templateProject,
      final Map<TreeItem, String> templates,
      final Project configProject,
      final Map<TreeItem, String> filesToCopy,
      final String tagName) {
    try {

      final GitLabService.GitFile configFile =
          gitLabService.getFile(
              configProject.getId(), actualConfig, operationEntity.getConfigurationRevision());
      final String config = configFile.content();
      // to store the long commitID not just the short-commitID
      operationEntity.setConfigurationRevision(configFile.commitId());
      operationRepository.updateOperation(operationEntity);

      final String iocInstanceName =
          Utils.generateProjectName(templateProject.getName(), actualConfig);

      Long latestOutputProjectId = iocInstanceEntity.getGitProjectId();

      boolean isNewProject = gitLabService.isNewProject(iocInstanceName, latestOutputProjectId);

      final Project outputProject;
      if (isNewProject) {
        logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
        logService.createLogEntry(
            iocInstanceEntity, InstanceStatus.GIT_PROJECT_CREATING, operationEntity);
        outputProject = gitLabService.createOutputProject(iocInstanceName);
        logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
        logService.createLogEntry(
            iocInstanceEntity, InstanceStatus.GIT_PROJECT_CREATED, operationEntity);
      } else {
        logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
        logService.createLogEntry(
            iocInstanceEntity, InstanceStatus.GIT_PROJECT_FETCHING, operationEntity);
        outputProject = gitLabService.getOutputProject(iocInstanceName, latestOutputProjectId);
        logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
        logService.createLogEntry(
            iocInstanceEntity, InstanceStatus.GIT_PROJECT_FETCHED, operationEntity);
      }

      LOGGER.debug(
          "Created or fetched project {} for instance {}", outputProject, iocInstanceEntity);

      logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
      logService.createLogEntry(
          iocInstanceEntity, InstanceStatus.GIT_TAGGING_AND_COMMITING, operationEntity);
      final List<GitLabService.FileToCommit> files =
          getFilesToCommit(iocInstanceEntity, templates, filesToCopy, config, iocInstanceName);

      final String outputReference = outputProject.getDefaultBranch();
      iocInstanceEntity.setGitProjectId(outputProject.getId());

      if (isNewProject) {
        // if output was created at the first time protect every tag on instance repo
        // only maintainer will be allowed to override tags
        gitLabService.protectTag(outputProject.getId(), "*", AccessLevel.MAINTAINER);
      }

      // delete unused files
      final List<String> filesToDelete = filesToDelete(files, outputProject, outputReference);

      final Commit commit =
          gitLabService.createCommit(
              outputProject, outputReference, INSTANCE_COMMIT_MESSAGE, files, filesToDelete);

      iocInstanceEntity.setGitRevision(commit.getId());
      LOGGER.debug("Created commit {} for instance {}", commit, iocInstanceEntity);

      final Tag tag = gitLabService.createTag(outputProject, tagName, commit.getId());
      logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
      logService.createLogEntry(
          iocInstanceEntity, InstanceStatus.GIT_TAGGGED_AND_COMMITED, operationEntity);
      LOGGER.debug("Ioc {} has been given tag {}", iocInstanceEntity, tagName);

      iocInstanceEntity.setGitTag(tag.getName());
      iocInstanceEntity.setStatus(GenerationStatus.SUCCESSFUL.toString());
      iocInstanceRepository.updateInstance(iocInstanceEntity);
      logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
      logService.createLogEntry(iocInstanceEntity, InstanceStatus.SUCCESSFUL, operationEntity);

    } catch (final Exception exception) {
      iocInstanceEntity.setStatus(GenerationStatus.FAILED.toString());
      logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
      logService.createLogEntry(
          iocInstanceEntity, exception.getMessage(), InstanceStatus.FAILED, operationEntity);
      LOGGER.error("Failed to create {}", iocInstanceEntity, exception);
    } finally {
      ZonedDateTime finishedAt = ZonedDateTime.now();
      iocInstanceEntity.setFinishedAt(finishedAt);
      iocInstanceRepository.updateInstance(iocInstanceEntity);
      setOperationFinishedIfNeeded(iocInstanceEntity.getId(), finishedAt);
      logService.closeLastEntry(iocInstanceEntity.getId(), operationEntity.getId());
      LOGGER.debug("Ioc {} has been successfully completed", iocInstanceEntity);
    }
  }

  private void setOperationFinishedIfNeeded(final Long iocEntId, final ZonedDateTime finishedAt) {

    OperationEntity operation = iocInstanceRepository.fetchOperation(iocEntId);
    List<IocInstanceLogEntity> lastLogEntries =
        logRepository.findLastLogEntriesForOperation(operation.getId());

    if (isOperationFinished(lastLogEntries)) {

      operation.setFinishedAt(finishedAt);
      taskExecutorService.completeOperation(operation.getId());
    }
    // save anyway to store the updated config revision
    operationRepository.updateOperation(operation);
  }

  @NotNull
  private List<GitLabService.FileToCommit> getFilesToCommit(
      final IocInstanceEntity iocInstanceEntity,
      final Map<TreeItem, String> templates,
      final Map<TreeItem, String> filesToCopy,
      final String config,
      final String iocInstanceName)
      throws IOException, GitLabApiException {
    final List<GitLabService.FileToCommit> files = new ArrayList<>();
    files.add(new GitLabService.FileToCommit(config, Literals.CONFIG_OUTPUT_PATH));
    for (final Map.Entry<TreeItem, String> template : templates.entrySet()) {
      files.add(
          new GitLabService.FileToCommit(
              template.getValue(),
              Literals.TEMPLATE_OUTPUT_PATH_PREFIX + template.getKey().getPath()));
      files.add(
          new GitLabService.FileToCommit(
              substituteTemplate(template.getValue(), config),
              StringUtils.removeEnd(template.getKey().getPath(), Literals.TEMPLATE_EXTENSION)));
    }

    for (final Map.Entry<TreeItem, String> fileToCopy : filesToCopy.entrySet()) {
      files.add(
          new GitLabService.FileToCommit(fileToCopy.getValue(), fileToCopy.getKey().getPath()));
    }

    final String readmeTemplate = gitLabService.getReadmeTemplate();
    final String expectedReadmeContent =
        substituteTemplate(
            readmeTemplate,
            new JSONObject(
                    ImmutableMap.of(
                        "address",
                        hostAddress,
                        "instance_name",
                        iocInstanceName,
                        // "id", String.valueOf(iocInstanceEntity.getOperation().getId())))
                        "id",
                        ""))
                .toString());
    files.add(new GitLabService.FileToCommit(expectedReadmeContent, Literals.README_FILE));
    return files;
  }

  public void executeQueuedOperations() {
    List<IocInstanceEntity> queuedIocInstances = iocInstanceRepository.findQueuedIocInstances();
    final Map<OperationEntity, List<IocInstanceEntity>> queuedInstancesPerOperation =
        new HashMap<>();

    for (IocInstanceEntity i : queuedIocInstances) {
      OperationEntity operation = iocInstanceRepository.fetchOperation(i.getId());
      if (queuedInstancesPerOperation.containsKey(operation)) {
        queuedInstancesPerOperation.get(operation).add(i);
      } else {
        List<IocInstanceEntity> iocList = new ArrayList<>();
        iocList.add(i);
        queuedInstancesPerOperation.put(operation, iocList);
      }
    }

    for (final Map.Entry<OperationEntity, List<IocInstanceEntity>> entry :
        queuedInstancesPerOperation.entrySet()) {
      final OperationEntity operation = entry.getKey();
      final List<IocInstanceEntity> iocInstances = entry.getValue();
      try {
        executeQueuedIocInstances(operation, iocInstances);
      } catch (final Exception e) {
        failRelatedIocInstances(iocInstances, e.getMessage(), operation);
      }
    }
  }

  private void failRelatedIocInstances(
      final List<IocInstanceEntity> iocInstances, String errorMessage, OperationEntity operation) {
    for (final IocInstanceEntity iocInstanceEntity : iocInstances) {
      iocInstanceEntity.setStatus(GenerationStatus.FAILED.toString());
      iocInstanceEntity.setFinishedAt(ZonedDateTime.now());
      logService.createLogEntry(iocInstanceEntity, errorMessage, InstanceStatus.FAILED, operation);
      LOGGER.error("Failed {} for {}", errorMessage, iocInstanceEntity);
    }
  }

  private void executeQueuedIocInstances(
      final OperationEntity operation, final List<IocInstanceEntity> instances)
      throws GitLabApiException {
    final IocTypeEntity iocTypeEntity = operation.getIocType();

    final Project templateProject =
        gitLabService.checkTemplateExists(iocTypeEntity.getTemplateGitProjectId());

    final String templateRevision =
        Optional.ofNullable(operation.getTemplateRevision())
            .orElse(templateProject.getDefaultBranch());

    final List<TreeItem> projectFiles =
        fetchAndValidateTemplateProjectFiles(templateProject, templateRevision);

    final Project configProject =
        gitLabService.checkConfigExists(iocTypeEntity.getConfigGitProjectId());
    final Map<TreeItem, String> templateMap =
        createTemplateMap(projectFiles, templateProject.getId(), templateRevision);
    final Map<TreeItem, String> filesToCopy =
        collectFilesToCopy(projectFiles, templateProject.getId(), templateRevision);

    for (final IocInstanceEntity iocInstance : instances) {
      final String configName = iocInstance.getConfigurationName() + Literals.CONFIG_EXTENSION;
      taskExecutorService.executeTask(
          operation.getId(),
          () ->
              queueAndExecuteIocInstance(
                  iocInstance,
                  configName,
                  operation,
                  templateProject,
                  templateMap,
                  configProject,
                  filesToCopy,
                  operation.getGitTag()));
    }
  }

  public List<ConfigFile> getAndValidateConfigFiles(
      IocTypeEntity iocTypeEntity, String templateRevision, String configRevision)
      throws GitLabApiException {
    Project templateProject =
        gitLabService.checkTemplateExists(iocTypeEntity.getTemplateGitProjectId());
    String schema;
    try {
      GitLabService.GitFile templateSchema =
          gitLabService.getFile(templateProject.getId(), Literals.SCHEMA_NAME, templateRevision);
      schema = templateSchema.content();
    } catch (GitLabApiException e) {
      if (e.getHttpStatus() == 404) {
        throw new EntityNotFoundException("Template schema file not found");
      }
      throw e;
    }

    try {
      JsonValidator.isValid(schema);
    } catch (InvalidJsonSyntaxException e) {
      throw new UnprocessableRequestException("Invalid " + Literals.SCHEMA_NAME, e.getMessage());
    }

    Project configProject = gitLabService.checkConfigExists(iocTypeEntity.getConfigGitProjectId());

    List<String> configNames = extractConfigs(configProject.getId(), configRevision);
    Map<String, ConfigValidationException> validationProblems = null;
    try {
      validateConfigs(configProject, schema, configRevision, configNames);
    } catch (ConfigListValidationException e) {
      validationProblems = e.getErrorMap();
    }

    Map<String, ConfigValidationException> finalValidationProblems = validationProblems;
    return configNames.stream()
        .map(
            c ->
                new ConfigFile(
                    c,
                    finalValidationProblems == null || !finalValidationProblems.containsKey(c),
                    finalValidationProblems != null && finalValidationProblems.get(c) != null
                        ? finalValidationProblems.get(c).getDescription()
                        : null))
        .collect(Collectors.toList());
  }

  public List<TypeFile> getTemplateFiles(IocTypeEntity iocType, String templateRevision)
      throws GitLabApiException {
    Project templateProject = gitLabService.checkTemplateExists(iocType.getTemplateGitProjectId());
    List<TreeItem> projectFiles =
        fetchAndValidateTemplateProjectFiles(templateProject, templateRevision);
    Map<TreeItem, String> templates =
        createTemplateMap(projectFiles, templateProject.getId(), templateRevision);
    return templates.entrySet().stream()
        .map(
            t ->
                new TypeFile(iocType.getId(), templateRevision, t.getKey().getPath(), t.getValue()))
        .collect(Collectors.toList());
  }

  /**
   * Lists all template files in the project that can be found under the certain revision
   *
   * @param iocType The template project entity
   * @param templateRevision The git revision (tag/commitId) for the template files
   * @return List of template file-names
   * @throws GitLabApiException If error occurs during Gitlab communication
   */
  public TypeFilesResponse getTemplateFileNames(IocTypeEntity iocType, String templateRevision)
      throws GitLabApiException {
    List<String> templateFiles = new ArrayList<>();
    List<String> filesToCopy = new ArrayList<>();
    List<String> ignoredFiles = new ArrayList<>();

    Project templateProject = gitLabService.checkTemplateExists(iocType.getTemplateGitProjectId());
    List<TreeItem> projectFiles =
        fetchAndValidateTemplateProjectFiles(templateProject, templateRevision).stream()
            .filter(f -> f.getType().equals(TreeItem.Type.BLOB))
            .toList();

    for (TreeItem item : projectFiles) {
      if (Utils.checkTemplateFileNameConvention(item.getName())) {
        templateFiles.add(item.getPath());
      } else if (Utils.isFileToCopy(item.getName())) {
        filesToCopy.add((item.getPath()));
      } else if (!StringUtils.equals(item.getName(), Literals.SCHEMA_NAME)) {
        ignoredFiles.add(item.getPath());
      }
    }

    return new TypeFilesResponse(
        iocType.getId(), templateRevision, templateFiles, filesToCopy, ignoredFiles);
  }

  /**
   * Getting a template file content from Gitlab. The template file name has to be from the allowed
   * template-name list
   *
   * @param iocType The template project entity
   * @param templateFileName The name of the template file
   * @param templateRevision The git revision (tag/commitId) for the file
   * @return The Content of the template file that has to be retrieved - or null
   * @throws GitLabApiException If error occurs during Gitlab communication
   */
  public TypeFile getTemplateFileContent(
      IocTypeEntity iocType, String templateFileName, String templateRevision)
      throws GitLabApiException {
    Project templateProject = gitLabService.checkTemplateExists(iocType.getTemplateGitProjectId());

    if (Utils.checkTemplateFileNameConvention(templateFileName)) {
      GitLabService.GitFile templateFile =
          gitLabService.getFile(templateProject.getId(), templateFileName, templateRevision);
      return new TypeFile(
          iocType.getId(), templateRevision, templateFileName, templateFile.content());
    }

    throw new UnprocessableRequestException(
        INVALID_TEMPLATE_PROJECT,
        "Template file-name invalid, or can not be found in the repository");
  }

  private List<TreeItem> fetchAndValidateTemplateProjectFiles(Project project, String reference)
      throws GitLabApiException {
    List<TreeItem> allFiles = gitLabService.allFilesAndFolders(project, reference);
    List<String> rootLevelFiles =
        allFiles.stream()
            .filter(
                f -> f.getType().equals(TreeItem.Type.BLOB) && f.getName().equals((f.getPath())))
            .map(TreeItem::getName)
            .toList();
    if (rootLevelFiles.stream().noneMatch(f -> f.equals(Literals.ST_CMD_TEMPLATE_NAME))) {
      throw new UnprocessableRequestException(
          INVALID_TEMPLATE_PROJECT, "No template files in repository");
    }
    if (rootLevelFiles.stream().noneMatch(f -> f.equals(Literals.SCHEMA_NAME))) {
      throw new UnprocessableRequestException(
          INVALID_TEMPLATE_PROJECT, Literals.SCHEMA_NAME + " is missing");
    }
    GitLabService.GitFile templateSchema =
        gitLabService.getFile(project.getId(), Literals.SCHEMA_NAME, reference);
    String schema = templateSchema.content();
    try {
      JsonValidator.isValid(schema);
    } catch (InvalidJsonSyntaxException e) {
      throw new UnprocessableRequestException(
          INVALID_TEMPLATE_PROJECT,
          Literals.SCHEMA_NAME + " is an invalid JSON: " + e.getMessage());
    }
    if (rootLevelFiles.stream().noneMatch(f -> f.equals(Literals.IOC_JSON))) {
      throw new UnprocessableRequestException(
          INVALID_TEMPLATE_PROJECT, Literals.IOC_JSON + " is missing");
    }

    return allFiles;
  }

  private Map<TreeItem, String> createTemplateMap(
      List<TreeItem> projectFiles, long projectId, String templateReference)
      throws GitLabApiException {
    Map<TreeItem, String> templates = new HashMap<>();
    for (TreeItem file : projectFiles) {
      if (Utils.checkTemplateFileNameConvention(file.getName())) {
        GitLabService.GitFile templateFile =
            gitLabService.getFile(projectId, file.getPath(), templateReference);
        templates.put(file, templateFile.content());
      }
    }
    return templates;
  }

  private Map<TreeItem, String> collectFilesToCopy(
      List<TreeItem> filePath, long projectId, String templateReference) throws GitLabApiException {
    Map<TreeItem, String> files = new HashMap<>();
    for (TreeItem file : filePath) {
      if (Utils.isFileToCopy(file.getName())) {
        GitLabService.GitFile gitFile =
            gitLabService.getFile(projectId, file.getPath(), templateReference);
        files.put(file, gitFile.content());
      }
    }
    return files;
  }

  /**
   * Validates the JSON schema file. If the schema doesn't contain 'properties', or 'required' list,
   * will throw UnprocessableRequestException. If the 'properties', or 'required' list doesn't
   * contain the st.cmd entry, or the repo contains *iocsh.mustache, *template.mustache,
   * *db.mustache, but these files are not listed in the lists -> UnprocessableRequestException will
   * be thrown.
   *
   * @param schemaContent the JSON schema content
   * @param projectFiles the file-names that are in the template repository
   */
  private void validateSchemaContent(String schemaContent, List<String> projectFiles) {
    try {
      Map<String, Object> schema = new ObjectMapper().readValue(schemaContent, HashMap.class);

      Object requiredObject = schema.get("required");
      List<String> required;
      if (requiredObject instanceof List) {
        required =
            ((List<String>) requiredObject)
                .stream().filter(f -> f.equals(Literals.CMD_FILE)).toList();
      } else {
        throw new UnprocessableRequestException(INVALID_CONFIG_SCHEMA, "No 'required'");
      }

      List<String> keys = new ArrayList<>();
      keys.add(Literals.CMD_FILE);
      for (String file : projectFiles) {
        if (Literals.iocshTemplatePattern.matcher(file).matches()
            || Literals.dbTemplatePattern.matcher(file).matches()
            || Literals.templateTemplatePattern.matcher(file).matches()) {
          keys.add(StringUtils.removeEnd(file, Literals.TEMPLATE_EXTENSION));
        }
      }

      if (!required.isEmpty() && CollectionUtils.intersection(required, keys).isEmpty()) {
        throw new UnprocessableRequestException(
            INVALID_CONFIG_SCHEMA, "Keys: [" + Strings.join(required, ',') + "] must be required");
      }
    } catch (JsonProcessingException e) {
      throw new UnprocessableRequestException(INVALID_CONFIG_SCHEMA, e.getMessage());
    }
  }

  private void validateConfigRevision(
      String schema, Project configProject, String configRevision, List<String> configFileNames)
      throws GitLabApiException {
    List<String> configNames = configFileNames;
    // getting configs if request part was empty
    if ((configNames == null) || (configNames.isEmpty())) {
      configNames = extractConfigs(configProject.getId(), configRevision);
    }

    validateConfigs(configProject, schema, configRevision, configNames);
  }

  public PagedIocResponse findAll(
      Long templateId,
      String query,
      String user,
      Integer page,
      Integer limit,
      IocFetchOption iocFetchOption,
      boolean listAll) {
    List<Ioc> listResult = new ArrayList<>();

    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit, listAll);

    String queryString = StringUtils.isEmpty(query) ? null : query;
    String userString = StringUtils.isEmpty(user) ? null : user;

    List<IocInstanceEntity> iocInstances;

    if (IocFetchOption.ALL.equals(iocFetchOption)) {
      iocInstances =
          iocInstanceRepository.findAll(
              templateId,
              queryString,
              userString,
              pagingLimitDto.getPage(),
              pagingLimitDto.getLimit());
    } else {
      iocInstances =
          iocInstanceRepository.findLatestInstances(
              templateId, queryString, pagingLimitDto.getPage(), pagingLimitDto.getLimit());
    }

    if (iocInstances != null) {

      for (IocInstanceEntity iocInstance : iocInstances) {
        listResult.add(convertIoc(iocInstance));
      }
    }

    long totalCount;

    if (IocFetchOption.ALL.equals(iocFetchOption)) {
      totalCount = iocInstanceRepository.countForPaging(templateId, queryString, userString);
    } else {
      totalCount = iocInstanceRepository.countInstancesForPaging(templateId, queryString);
    }

    return new PagedIocResponse(
        templateId,
        listResult,
        totalCount,
        listResult.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit());
  }

  @NotNull
  private Long getIocTypeNextVersionNum(IocTypeEntity iocTypeEntity) {
    Long maxIocTypeVersion = iocTypeEntity.getMaxVersion();
    return maxIocTypeVersion == null ? 1 : maxIocTypeVersion + 1;
  }

  /**
   * Determines which files are part of the generated project, and which can be deleted
   *
   * @param neededFiles List of the generated files
   * @param project The output git project
   * @param reference The reference for the output git project
   * @return List of filePath that could be deleted (because are not part of the generated project)
   */
  private List<String> filesToDelete(
      List<GitLabService.FileToCommit> neededFiles, Project project, String reference) {
    List<String> result = new ArrayList<>();
    try {
      List<TreeItem> projectFiles = gitLabService.allFilesAndFolders(project, reference);

      for (int i = projectFiles.size() - 1; i >= 0; i--) {
        if (!isFileNeededInCommit(neededFiles, projectFiles.get(i))) {
          result.add(projectFiles.get(i).getPath());
        }
      }
    } catch (GitLabApiException e) {
      LOGGER.error("Error while trying to determine which files to delete", e);
    }

    return result;
  }

  /**
   * Checks if a file is needed for the new output-version creation Check is done by the filePath
   *
   * @param neededFiles the file list that should be kept (a.k.a. newly generated files)
   * @param fileToCheck the file that should be checked
   * @return <code>true</code> if the file/directory is needed <code>false</code> if the
   *     file/directory is not needed anymore, and it can be deleted
   */
  private boolean isFileNeededInCommit(
      List<GitLabService.FileToCommit> neededFiles, TreeItem fileToCheck) {

    for (GitLabService.FileToCommit f : neededFiles) {
      // fileToCheck is a file
      if ((fileToCheck.getType().equals(TreeItem.Type.BLOB))
          && (f.getFilePath().equals(fileToCheck.getPath()))) {
        return true;
      }

      // directory type - leave as it is, empty dirs will be deleted by git automatically
      if ((fileToCheck.getType().equals(TreeItem.Type.TREE))) {
        return true;
      }
    }

    return false;
  }

  private String substituteTemplate(String template, String config) throws IOException {
    Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
    Map<String, String[]> scopes = new Gson().fromJson(config, mapType);

    MustacheFactory mf = new DefaultMustacheFactory();
    Mustache mustache = mf.compile(new StringReader(template), "ioc_template");

    Writer writer = new StringWriter();
    mustache.execute(writer, scopes);
    writer.flush();

    return writer.toString();
  }

  private void validateConfigs(
      Project configProject, String schema, String configReference, List<String> configNames)
      throws GitLabApiException {
    Map<String, ConfigValidationException> errorMap = new HashMap<>();
    for (String actConfig : configNames) {
      String config =
          gitLabService.getFile(configProject.getId(), actConfig, configReference).content();

      try {
        validateConfig(schema, config, true);
      } catch (ConfigValidationException e) {
        errorMap.put(actConfig, e);
      }
    }
    if (!errorMap.isEmpty()) {
      throw new ConfigListValidationException(errorMap);
    }
  }

  private List<String> extractConfigs(Long configProjectId, String configReference)
      throws GitLabApiException {
    List<String> fileList = gitLabService.listFilesInRepo(configProjectId, configReference);

    return fileList.stream()
        .filter(f -> StringUtils.endsWithIgnoreCase(f, Literals.CONFIG_EXTENSION))
        .toList();
  }

  /**
   * Validates the schema file, and the config file (as JSON), and validates the config against the
   * schema
   *
   * @param schemaJson the schema file content
   * @param configJson the config file content
   * @param shouldValidateConfig set <code>true</code> if the config file should be validated, or
   *     set <code>false</code> if only the schema content should be validated (if it is a valid
   *     JSON)
   * @throws ConfigValidationException if the schema, or config file is not valid
   */
  private void validateConfig(String schemaJson, String configJson, boolean shouldValidateConfig)
      throws ConfigValidationException {
    try {
      JsonValidator.isValid(schemaJson);
    } catch (InvalidJsonSyntaxException e) {
      LOGGER.error("Invalid template JSON schema", e);
      throw new ConfigValidationException("Invalid template JSON schema", e.getMessage());
    }

    if (shouldValidateConfig) {
      try {
        JsonValidator.isValid(configJson);
      } catch (InvalidJsonSyntaxException e) {
        LOGGER.error("Invalid Config JSON", e);
        throw new ConfigValidationException("Invalid Config JSON", e.getMessage());
      }

      try {
        Schema schema = SchemaLoader.load(new JSONObject(schemaJson));
        schema.validate(new JSONObject(configJson));
      } catch (ValidationException e) {
        LOGGER.error("Config JSON validation error", e);
        throw new ConfigValidationException(
            "Config JSON validation error",
            StringUtils.join(
                e.getCausingExceptions() != null && !e.getCausingExceptions().isEmpty()
                    ? ImmutableList.of(
                        e.getMessage(),
                        StringUtils.join(
                            e.getCausingExceptions().stream()
                                .map(Exception::getMessage)
                                .collect(Collectors.toList()),
                            ", "))
                    : ImmutableList.of(e.getMessage()),
                ", "));
      }
    }
  }

  /**
   * Calculates how many output repositories will be created based on the IOC type ID, and the
   * config-name.
   *
   * @param typeId the IOC type ID
   * @param configNames the configuration names that should be checked
   * @return a number about how many repositories will be created based on the IOC type Id and
   *     config name
   * @throws UnprocessableRequestException if the config name list is null, or empty
   * @throws EmptyResultDataAccessException if IOC type cannot be found by ID
   */
  public CalculateNewIocsResponse calculateNewInstances(
      Long typeId, CalculateNewIocsRequest configNames) {
    // checking config-name list
    if (configNames.configNames() == null || (configNames.configNames().isEmpty())) {
      throw new UnprocessableRequestException("Calculation error", "Config-name list is empty");
    }

    // check if the IOC type ID exists
    iocTypeService.findTemplateById(typeId, true);

    // calculate
    int instanceNotFoundCount = 0;
    for (String config : configNames.configNames()) {
      if (!iocInstanceRepository.checkInstanceIsCreated(
          typeId, StringUtils.removeEndIgnoreCase(config, Literals.CONFIG_EXTENSION))) {
        instanceNotFoundCount++;
      }
    }

    return new CalculateNewIocsResponse(instanceNotFoundCount);
  }

  public GeneratedIocNames listInstanceNames(long typeId) {
    // check if type ID exits
    iocTypeService.findTemplateById(typeId, false);

    List<IocInstanceEntity> instanceList =
        iocInstanceRepository.findAll(typeId, null, null, 0, null);

    return new GeneratedIocNames(
        instanceList.size(),
        instanceList.stream()
            .map(i -> new NameAndGitRepo(i.getConfigurationName(), i.getGitProjectId()))
            .toList(),
        typeId);
  }

  public List<NameAndGitRepoWithMessage> deleteInstances(
      long typeId, DeleteIocRequest request, boolean forceDelete) {

    // check if type ID exits
    iocTypeService.findTemplateById(typeId, false);

    List<NameAndGitRepoWithMessage> result = new ArrayList<>();

    for (NameAndGitRepo n : request.iocsToDelete()) {
      // check if the instance exist in DB
      if (iocInstanceRepository.instanceExists(typeId, n.getGitRepoId(), n.getConfigName())) {
        boolean failedToDelete = false;
        // delete from git
        try {
          if (n.getGitRepoId() != null) {
            gitLabService.deleteProjectById(n.getGitRepoId());
          }
        } catch (Exception e) {
          LOGGER.error(
              "Error while trying to delete instance Name: {}, TypeId: {}, git repo ID: {}",
              n.getConfigName(),
              n.getGitRepoId(),
              typeId,
              e);
          failedToDelete = true;

          result.add(
              new NameAndGitRepoWithMessage(
                  n.getConfigName(),
                  n.getGitRepoId(),
                  "Failed to delete Git repo!",
                  OperationResult.ERROR));
        }

        // delete from DB
        if (forceDelete || !failedToDelete) {
          logRepository.deleteLogsRelatedToInstance(typeId, n.getGitRepoId(), n.getConfigName());
          iocInstanceRepository.deleteInstances(typeId, n.getGitRepoId(), n.getConfigName());
          operationRepository.deleteOrphanOperations();
        }

        // if nothing failed
        if (!failedToDelete) {
          result.add(
              new NameAndGitRepoWithMessage(
                  n.getConfigName(),
                  n.getGitRepoId(),
                  "Instance deleted",
                  OperationResult.SUCCESS));
        }
      } else {
        // instance doesn't exist in DB -> error
        result.add(
            new NameAndGitRepoWithMessage(
                n.getConfigName(),
                n.getGitRepoId(),
                "Instance doesn't exist!",
                OperationResult.ERROR));
      }
    }

    return result;
  }

  public JobInstanceLogResponse getInstanceLog(Long operationId, Long instanceId) {
    iocInstanceRepository.findById(instanceId);
    Map<InstanceStatus, IocInstanceLogEntity> instanceLogs =
        logService.getIocInstanceGenerationProgress(operationId, instanceId);
    InstanceLogEntry currentStepInfo;
    List<InstanceLogEntry> logs;

    OperationEntity operation = operationRepository.findOperationById(operationId);

    if (JobType.ATTACH.name().equals(operation.getJobType())
        || (JobType.MOVE.name().equals(operation.getJobType()))) {
      IocInstanceLogEntity lastLogEntry = logRepository.findLastLogEntry(instanceId, operationId);
      currentStepInfo = convertMoveOrAttach(lastLogEntry);
      logs = List.of(currentStepInfo);
    } else {
      InstanceLogEntry creationEntity = getCreationEntity(instanceLogs);
      currentStepInfo = creationEntity;

      InstanceLogEntry processingEntity =
          getProcessingEntity(instanceLogs, currentStepInfo.status());
      if (currentStepInfo.status() == InstanceGenerationStatus.SUCCESSFUL
          || currentStepInfo.status() == InstanceGenerationStatus.SKIPPED) {
        currentStepInfo = processingEntity;
      }

      logs = List.of(creationEntity, processingEntity);
    }

    return new JobInstanceLogResponse(
        instanceId, currentStepInfo.getStep(), currentStepInfo.status(), logs);
  }

  private static InstanceLogEntry convertMoveOrAttach(IocInstanceLogEntity log) {
    InstanceGenerationStep step;
    if (JobType.ATTACH.name().equals(log.getType())) {
      step = InstanceGenerationStep.ATTACH_INSTANCE;
    } else {
      step = InstanceGenerationStep.MOVE_INSTANCE;
    }

    return new InstanceLogEntry(
        step,
        0,
        log.getCreatedAt(),
        log.getFinishedAt(),
        InstanceGenerationStatus.valueOf(log.getStatus()),
        log.getDescription());
  }

  private static @NotNull InstanceLogEntry getCreationEntity(
      Map<InstanceStatus, IocInstanceLogEntity> instanceLogs) {

    if (instanceLogs.containsKey(InstanceStatus.GIT_PROJECT_CREATING)) {
      IocInstanceLogEntity creatingLog = instanceLogs.get(InstanceStatus.GIT_PROJECT_CREATING);
      IocInstanceLogEntity createdLog = instanceLogs.get(InstanceStatus.GIT_PROJECT_CREATED);
      if (createdLog == null) {
        createdLog = instanceLogs.get(InstanceStatus.FAILED);
      }
      return getInstanceLogEntry(
          creatingLog, createdLog, InstanceGenerationStep.CREATE_GIT_PROJECT);
    } else if (instanceLogs.containsKey(InstanceStatus.GIT_PROJECT_FETCHING)) {
      return new InstanceLogEntry(
          InstanceGenerationStep.CREATE_GIT_PROJECT,
          InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
          null,
          null,
          InstanceGenerationStatus.SKIPPED,
          null);
    } else {
      return new InstanceLogEntry(
          InstanceGenerationStep.CREATE_GIT_PROJECT,
          InstanceGenerationStep.CREATE_GIT_PROJECT.ordinal(),
          null,
          null,
          InstanceGenerationStatus.QUEUED,
          null);
    }
  }

  private static @NotNull InstanceLogEntry getProcessingEntity(
      Map<InstanceStatus, IocInstanceLogEntity> instanceLogs,
      InstanceGenerationStatus currentStatus) {
    InstanceGenerationStep instanceGenerationStep =
        InstanceGenerationStep.PROCESSING_TEMPLATE_AND_COMMIT;
    if (instanceLogs.containsKey(InstanceStatus.GIT_TAGGING_AND_COMMITING)) {
      IocInstanceLogEntity processingLog =
          instanceLogs.get(InstanceStatus.GIT_TAGGING_AND_COMMITING);
      IocInstanceLogEntity processedLog = instanceLogs.get(InstanceStatus.GIT_TAGGGED_AND_COMMITED);
      if (processedLog == null) {
        processedLog = instanceLogs.get(InstanceStatus.FAILED);
      }
      return getInstanceLogEntry(processingLog, processedLog, instanceGenerationStep);
    } else if (instanceLogs.containsKey(InstanceStatus.GIT_PROJECT_FETCHING)
        && instanceLogs.containsKey(InstanceStatus.FAILED)) {
      IocInstanceLogEntity fetchingLog = instanceLogs.get(InstanceStatus.GIT_PROJECT_FETCHING);
      IocInstanceLogEntity fetchedLog = instanceLogs.get(InstanceStatus.FAILED);
      return getInstanceLogEntry(fetchingLog, fetchedLog, instanceGenerationStep);
    }
    return new InstanceLogEntry(
        instanceGenerationStep,
        instanceGenerationStep.ordinal(),
        null,
        null,
        currentStatus == InstanceGenerationStatus.FAILED
            ? InstanceGenerationStatus.SKIPPED
            : InstanceGenerationStatus.QUEUED,
        null);
  }

  private static @NotNull InstanceLogEntry getInstanceLogEntry(
      IocInstanceLogEntity processingLog,
      IocInstanceLogEntity processedLog,
      InstanceGenerationStep instanceGenerationStep) {
    if (processedLog != null) {
      return new InstanceLogEntry(
          instanceGenerationStep,
          instanceGenerationStep.ordinal(),
          processingLog.getCreatedAt(),
          processedLog.getCreatedAt(),
          processedLog.getStatus().equals(InstanceStatus.FAILED.toString())
              ? InstanceGenerationStatus.FAILED
              : InstanceGenerationStatus.SUCCESSFUL,
          processedLog.getDescription());
    }
    return new InstanceLogEntry(
        instanceGenerationStep,
        instanceGenerationStep.ordinal(),
        processingLog.getCreatedAt(),
        null,
        InstanceGenerationStatus.RUNNING,
        processingLog.getDescription());
  }

  public void moveInstance(long newTypeId, long instanceId, String createdBy) {
    IocTypeEntity iocType = iocTypeService.findTemplateById(newTypeId, false);
    IocInstanceEntity instance = iocInstanceRepository.findById(instanceId);
    OperationEntity oldOperation = iocInstanceRepository.fetchOperation(instanceId);

    final OperationEntity operationEntity = new OperationEntity();

    long newVersion =
        Utils.calculateVerNum(
            gitLabService, iocType, Collections.singletonList(instance.getGitProjectId()));

    operationEntity.setPredecessorType(instance.getIocType());
    operationEntity.setPredecessorTypeName(instance.getIocType().getName());
    operationEntity.setIocType(iocType);
    operationEntity.setTypeName(iocType.getName());
    operationEntity.setCreatedBy(createdBy);
    operationEntity.setCreatedAt(ZonedDateTime.now());
    operationEntity.setGitTag(oldOperation.getGitTag());
    operationEntity.setConfigurationRevision(oldOperation.getConfigurationRevision());
    // Tag should be filled to show latest version on type list
    operationEntity.setGitTag("");
    operationEntity.setTemplateRevision(oldOperation.getTemplateRevision());
    operationEntity.setJobType(JobType.MOVE.name());
    operationEntity.setVersion(newVersion);
    operationEntity.setFinishedAt(ZonedDateTime.now());
    operationRepository.createOperation(operationEntity);

    String instanceName =
        Utils.generateProjectName(iocType.getName(), instance.getConfigurationName());

    instance.setIocTypeName(iocType.getName());
    instance.setIocType(iocType);
    instance.setInstanceName(instanceName);
    instance.setGitTag(null);

    iocInstanceRepository.updateInstance(instance);

    try {
      gitLabService.renameProjectById(instance.getGitProjectId(), instanceName);
      logMovingInstance(
          true, iocType, instance, operationEntity, "IOC Moved", newVersion, createdBy);
    } catch (GitLabApiException e) {
      LOGGER.debug(
          "Failed to rename git project when trying to move, new name: {}", instanceName, e);
      logMovingInstance(
          false,
          iocType,
          instance,
          operationEntity,
          "IOC Repository rename filed",
          newVersion,
          createdBy);
    }

    iocType.setMaxVersion(newVersion);
    templateRepository.updateIocType(iocType);
    operationRepository.deleteOrphanOperations();
  }

  private void logMovingInstance(
      boolean wasSuccessful,
      IocTypeEntity iocType,
      IocInstanceEntity instance,
      OperationEntity operationEntity,
      String resultText,
      long newVersion,
      String createdBy) {
    IocInstanceLogEntity log = new IocInstanceLogEntity();
    log.setIocInstance(instance);
    log.setCreatedAt(ZonedDateTime.now());
    log.setOperation(operationEntity);
    log.setType(JobType.MOVE.name());
    log.setTypeName(iocType.getName());
    log.setDescription(resultText);

    if (wasSuccessful) {
      log.setStatus(GenerationStatus.SUCCESSFUL.toString());
    } else {
      log.setStatus(GenerationStatus.FAILED.toString());
    }

    log.setFinishedAt(ZonedDateTime.now());
    log.setVersion(newVersion);
    log.setCreatedBy(createdBy);
    logRepository.create(log);
  }

  private static Ioc convertIoc(final IocInstanceEntity iocInstanceEntity) {

    return new Ioc(
        iocInstanceEntity.getId(),
        iocInstanceEntity.getIocType().getId(),
        iocInstanceEntity.getIocTypeName(),
        iocInstanceEntity.getCreatedBy(),
        iocInstanceEntity.getCreatedAt(),
        iocInstanceEntity.getGitProjectId(),
        iocInstanceEntity.getGitTag(),
        iocInstanceEntity.getConfigurationName(),
        iocInstanceEntity.getVersion());
  }

  private static boolean isOperationFinished(
      final List<IocInstanceLogEntity> instancesOfOperation) {
    return instancesOfOperation.stream()
        .allMatch(
            iocInstanceLogEntity -> FINISHED_STATUSES.contains(iocInstanceLogEntity.getStatus()));
  }
}
