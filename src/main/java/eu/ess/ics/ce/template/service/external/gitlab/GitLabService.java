/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.external.gitlab;

import eu.ess.ics.ce.template.common.Literals;
import eu.ess.ics.ce.template.common.Utils;
import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.GitRepoNotFoundException;
import eu.ess.ics.ce.template.exceptions.UnprocessableRequestException;
import eu.ess.ics.ce.template.rest.model.authentication.response.BaseUserInfo;
import eu.ess.ics.ce.template.rest.model.git.response.GitProject;
import eu.ess.ics.ce.template.rest.model.git.response.GitReference;
import eu.ess.ics.ce.template.rest.model.git.response.ReferenceType;
import eu.ess.ics.ce.template.service.external.gitlab.dto.GitRef;
import java.util.*;
import org.apache.commons.lang3.StringUtils;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.GroupApi;
import org.gitlab4j.api.TagsApi;
import org.gitlab4j.api.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class GitLabService {
  private static final String EXAMPLE_CONFIG_PATH = "configs";
  private static final String EXAMPLE_TEMPLATE_PATH = "template";
  private static final String ROOT_PATH = "";
  private static final String GIT_PROJECT_EXISTS = "Gitlab project exists";
  private static final String FAILED_TO_CREATE_PROJECT = "Failed to create Gitlab project";

  private static final String GIT_TAG_EXISTS = "Git tag already exists";
  private static final String MASTER_CONFIG_FILE_NAME = "_" + Literals.CONFIG_EXTENSION;

  public static final class FileToCommit {
    private String fileContent;
    private String filePath;

    public FileToCommit(String fileContent, String filePath) {
      this.fileContent = fileContent;
      this.filePath = filePath;
    }

    public String getFileContent() {
      return fileContent;
    }

    public String getFilePath() {
      return filePath;
    }
  }

  private static final class GitProjectDetails {
    private Long groupId;
    private String projectName;

    public Long getGroupId() {
      return groupId;
    }

    public String getProjectName() {
      return projectName;
    }

    public GitProjectDetails(Long groupId, String projectName) {
      this.groupId = groupId;
      this.projectName = projectName;
    }
  }

  public record GitFile(String content, String commitId) {}

  private static final Logger LOGGER = LoggerFactory.getLogger(GitLabService.class);

  private static final int TAG_COMMIT_LIMIT = 4;

  @Value("${gitlab.server.address}")
  private String gitServerAddress;

  @Value("${gitlab.tech.user.token}")
  private String token;

  @Value("${gitlab.allowed.output.group.id}")
  private Long allowedGitOutputGroup;

  @Value("${gitlab.allowed.template.group.id}")
  private Long allowedGitTemplateGroup;

  @Value("${gitlab.allowed.config.group.id}")
  private Long allowedGitConfigGroup;

  @Value("${gitlab.allowed.example.group.id}")
  private Long allowedGitExampleGroup;

  @Value("${gitlab.master.template.id}")
  private Long masterTemplateId;

  @Value("${gitlab.master.config.id}")
  private long masterConfigId;

  @Value("${gitlab.master.instance.id}")
  private long masterInstanceId;

  public GitProject getGitProject(final long projectId) throws GitLabApiException {
    final Project project = projectFromId(projectId);
    return new GitProject(
        project.getId(), project.getName(), project.getWebUrl(), project.getDescription());
  }

  public BaseUserInfo findUser(String userName) {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getUserApi().findUsers(userName).stream()
          .filter(u -> "active".equalsIgnoreCase(u.getState()))
          .findFirst()
          .map(
              u -> {
                BaseUserInfo tmpUsr = new BaseUserInfo();
                tmpUsr.setAvatar(u.getAvatarUrl());
                tmpUsr.setEmail(u.getEmail());
                tmpUsr.setFullName(u.getName());
                /*
                "normal" LDAP user is usually disabled by IT, and the other username
                (with index postfix) is active, which will cause unwanted behaviour on the UI
                */
                tmpUsr.setLoginName(userName);
                tmpUsr.setGitlabUserName(u.getUsername());

                return tmpUsr;
              })
          .orElseThrow(() -> new EntityNotFoundException("Active Gitlab user", userName));
    } catch (GitLabApiException e) {
      throw new EntityNotFoundException("Gitlab user", userName);
    }
  }

  @Cacheable("gitlab_file_content")
  public GitFile getFile(Long projectId, String fileName, String ref)
      throws GitLabApiException, GitRepoNotFoundException {

    if (StringUtils.isEmpty(fileName)) {
      throw new GitRepoNotFoundException("Requested file-name cannot be empty!");
    }

    RepositoryFile file;

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      file = gitLabApi.getRepositoryFileApi().getFile(projectId, fileName, ref);
    }

    return new GitFile(file.getDecodedContentAsString(), file.getCommitId());
  }

  public String getOptionalFileContent(Long projectId, String fileName, String ref) {
    Optional<RepositoryFile> file;

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      file = gitLabApi.getRepositoryFileApi().getOptionalFile(projectId, fileName, ref);
    }

    return file.map(RepositoryFile::getDecodedContentAsString).orElse(null);
  }

  @Cacheable("gitlab_template_project")
  public Project checkTemplateExists(Long projectId) {
    return checkRepoIdExists(projectId);
  }

  public boolean checkTemplateExists(String projectName) {
    return checkRepoExists(projectName, allowedGitTemplateGroup) != null;
  }

  @Cacheable("gitlab_config_project")
  public Project checkConfigExists(Long projectId) {
    return checkRepoIdExists(projectId);
  }

  public boolean checkConfigExists(String projectName) {
    return checkRepoExists(projectName, allowedGitConfigGroup) != null;
  }

  public Project createTemplateProject(String projectName) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload = createPayloadForInitialCommit(gitLabApi, ROOT_PATH, masterTemplateId);

      return createProjectWithInitialCommit(
          gitLabApi, projectName, allowedGitTemplateGroup, payload);
    }
  }

  public Project createTemplateProjectFromExample(
      final String projectName, final long exampleProjectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload =
          createPayloadForInitialCommit(gitLabApi, EXAMPLE_TEMPLATE_PATH, exampleProjectId);

      return createProjectWithInitialCommit(
          gitLabApi, projectName, allowedGitTemplateGroup, payload);
    }
  }

  public Project createConfigProjectFromExample(
      final String projectName, final long exampleProjectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload =
          createPayloadForInitialCommit(gitLabApi, EXAMPLE_CONFIG_PATH, exampleProjectId);
      if (payload != null) {

        return createProjectWithInitialCommit(
            gitLabApi, projectName, allowedGitConfigGroup, payload);
      }
      return createProject(gitLabApi, projectName, allowedGitConfigGroup);
    }
  }

  public Project createConfigProject(String projectName, List<String> configNames)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload = createConfigPayload(gitLabApi, masterConfigId, configNames);
      if (payload != null) {

        return createProjectWithInitialCommit(
            gitLabApi, projectName, allowedGitConfigGroup, payload);
      }
      return createProject(gitLabApi, projectName, allowedGitConfigGroup);
    }
  }

  public List<String> allTopLevelFiles(Project project, String reference)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      List<TreeItem> tree =
          gitLabApi.getRepositoryApi().getTree(project.getId(), ROOT_PATH, reference);
      return tree.stream().map(TreeItem::getName).toList();
    }
  }

  /**
   * Lists all files and directories within a git project
   *
   * @param project the project that has to be fetched
   * @param reference the tag/commitId that has to be examined
   * @return List of TreeItems containing information about the stored files/directories
   * @throws GitLabApiException If error occurs during the fetching
   */
  public List<TreeItem> allFilesAndFolders(Project project, String reference)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getRepositoryApi().getTree(project.getId(), ROOT_PATH, reference, true);
    }
  }

  public String getReadmeTemplate() throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      Project masterProject = gitLabApi.getProjectApi().getProject(masterInstanceId);
      return getFile(
              masterInstanceId, Literals.README_FILE_TEMPLATE, masterProject.getDefaultBranch())
          .content;
    }
  }

  private Project createProjectWithInitialCommit(
      GitLabApi gitLabApi, String projectName, long groupId, CommitPayload payload)
      throws GitLabApiException {
    Project gitProject = checkRepoExists(projectName, groupId);
    if (gitProject != null) {
      throw new UnprocessableRequestException(
          GIT_PROJECT_EXISTS,
          "Project \"" + projectName + "\" already exists in group (ID: " + groupId + ")");
    }
    if (payload == null) {
      throw new UnprocessableRequestException(
          FAILED_TO_CREATE_PROJECT, "Initial commit creation failed");
    }

    Project projectSpec =
        new Project()
            .withPublic(true)
            .withNamespaceId(groupId)
            // Minimum of one approval
            .withApprovalsBeforeMerge(1)
            // Merge checks: All threads resolved
            .withOnlyAllowMergeIfAllDiscussionsAreResolved(true)
            .withName(projectName);

    Project created = gitLabApi.getProjectApi().createProject(projectSpec);

    ProjectApprovalsConfig approvalsConfig = new ProjectApprovalsConfig();
    // Approval settings: Prevent approvals by users who add commits
    approvalsConfig.withMergeRequestsDisableCommittersApproval(true);
    // Prevent editing approval rules in merge requests
    approvalsConfig.withDisableOverridingApproversPerMergeRequest(true);
    // Approval settings: Prevent approval by author
    approvalsConfig.setMergeRequestsAuthorApproval(false);

    gitLabApi.getProjectApi().setApprovalsConfiguration(created.getId(), approvalsConfig);

    payload.setBranch(created.getDefaultBranch());
    gitLabApi.getCommitsApi().createCommit(created.getId(), payload);
    return created;
  }

  Project createProject(GitLabApi gitLabApi, String projectName, long groupId)
      throws GitLabApiException {
    Project gitProject = checkRepoExists(projectName, groupId);
    if (gitProject != null) {
      throw new UnprocessableRequestException(
          GIT_PROJECT_EXISTS,
          "Project \"" + projectName + "\" already exists in group (ID: " + groupId + ")");
    }

    Project projectSpec =
        new Project().withPublic(true).withNamespaceId(groupId).withName(projectName);

    return gitLabApi.getProjectApi().createProject(projectSpec);
  }

  private CommitPayload createPayloadForInitialCommit(
      final GitLabApi gitLabApi, final String filePath, final long sourceProjectId) {
    try {
      final Project sourceProject = gitLabApi.getProjectApi().getProject(sourceProjectId);

      final List<TreeItem> tree =
          gitLabApi
              .getRepositoryApi()
              .getTree(sourceProject.getId(), filePath, sourceProject.getDefaultBranch());
      final List<CommitAction> commitActions = new ArrayList<>();
      for (final TreeItem item : tree) {
        final CommitAction action = new CommitAction();
        action.setAction(CommitAction.Action.CREATE);
        action.setContent(
            getFile(sourceProject.getId(), item.getPath(), sourceProject.getDefaultBranch())
                .content());
        action.setFilePath(calculateDestinationPath(filePath, item));
        commitActions.add(action);
      }
      return new CommitPayload().withCommitMessage("Init").withActions(commitActions);
    } catch (final Exception e) {
      LOGGER.error(
          "Unable to collect initial commit information during new template/config creation", e);
      throw new UnprocessableRequestException(
          "Error during new template/config project creation",
          "Unable to collect initial commit information");
    }
  }

  private String calculateDestinationPath(final String filePath, final TreeItem item) {
    return filePath.isEmpty() ? item.getPath() : item.getName();
  }

  private CommitPayload createConfigPayload(
      GitLabApi gitLabApi, long masterProjectId, List<String> configNames) {

    if (configNames == null || configNames.isEmpty()) {
      return createInitCommitPayloadWithGitignoreFile();
    }

    try {
      Project masterProject = gitLabApi.getProjectApi().getProject(masterProjectId);
      List<TreeItem> tree =
          gitLabApi
              .getRepositoryApi()
              .getTree(masterProject.getId(), ROOT_PATH, masterProject.getDefaultBranch());
      List<CommitAction> commitActions = new ArrayList<>();
      for (TreeItem item : tree) {
        // If at least one configuration file name (IOC instance name postfix) was defined by API
        // user,
        // configuration file names must be iterated and files must be added into initial commit
        // (with file content defined in configuration project master template)
        if (item.getName().equals(MASTER_CONFIG_FILE_NAME)
            && configNames != null
            && !configNames.isEmpty()) {
          for (String configName : configNames) {
            CommitAction action = new CommitAction();
            action.setAction(CommitAction.Action.CREATE);
            action.setContent(
                getFile(masterProject.getId(), item.getName(), masterProject.getDefaultBranch())
                    .content());
            action.setFilePath(
                item.getPath()
                    .replace(MASTER_CONFIG_FILE_NAME, configName + Literals.CONFIG_EXTENSION));
            commitActions.add(action);
          }
        } else {
          return null;
        }
      }
      commitActions.add(createGitignoreFileAction());
      return new CommitPayload().withCommitMessage("Init").withActions(commitActions);
    } catch (Exception e) {
      LOGGER.error(
          "Unable to collect initial commit information during new template/config creation", e);
      throw new UnprocessableRequestException(
          "Error during new template/config project creation",
          "Unable to collect initial commit information");
    }
  }

  private CommitAction createGitignoreFileAction() {
    final CommitAction commitAction = new CommitAction();
    commitAction.setAction(CommitAction.Action.CREATE);
    commitAction.setContent(Literals.GITIGNORE_FILE_CONTENT);
    commitAction.setFilePath(Literals.GITIGNORE_FILE);
    return commitAction;
  }

  private CommitPayload createInitCommitPayloadWithGitignoreFile() {
    final CommitAction createGitignoreAction = createGitignoreFileAction();
    return new CommitPayload()
        .withCommitMessage("Init")
        .withActions(List.of(createGitignoreAction));
  }

  private Project checkRepoIdExists(Long projectId) {

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      Optional<Project> project = gitLabApi.getProjectApi().getOptionalProject(projectId);

      if (project.isEmpty()) {
        throw new EntityNotFoundException("Gitlab project with id", projectId);
      }
      return project.get();
    }
  }

  private Project checkRepoExists(String project, Long groupId) {

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      Optional<Group> projectGroup = gitLabApi.getGroupApi().getOptionalGroup(groupId);

      if (projectGroup.isPresent()) {
        Optional<Project> gitProject =
            gitLabApi
                .getProjectApi()
                .getOptionalProject(Utils.concatUrl(projectGroup.get().getFullPath(), project));

        if (gitProject.isPresent()) {
          Project gitResult = gitProject.get();
          return gitResult.getArchived() ? null : gitResult;
        }
      }

      return null;
    }
  }

  public Project createOutputProject(String project) throws GitLabApiException {

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      GitProjectDetails projectDetails = parseGitProject(project);

      Project projectSpec =
          new Project()
              .withPublic(true)
              .withNamespaceId(projectDetails.getGroupId())
              .withName(projectDetails.getProjectName());

      return gitLabApi.getProjectApi().createProject(projectSpec);
    }
  }

  public boolean isNewProject(String projectName, Long projectId) {
    if (projectId != null) {
      return false;
    } else {
      Project instanceProject = checkRepoExists(projectName, allowedGitOutputGroup);
      return instanceProject == null;
    }
  }

  public Project getOutputProject(String projectName, Long projectId) throws GitLabApiException {
    if (projectId != null) {
      return projectFromId(projectId);
    } else {
      return checkRepoExists(projectName, allowedGitOutputGroup);
    }
  }

  /**
   * Protecting project's tag(s)
   *
   * @param projectId The Git project Id
   * @param tagName Name of the tag that should be protected (wildcard '*' is allowed)
   * @param protectionLevel Protection level who can override the tag
   * @throws GitLabApiException If exception happens during creating tag protection on GitLab
   */
  public void protectTag(Long projectId, String tagName, AccessLevel protectionLevel)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      TagsApi tagsApi = gitLabApi.getTagsApi();
      tagsApi.protectTag(projectId, tagName, protectionLevel);
    }
  }

  public List<GitProject> listTemplateProjects() throws GitLabApiException {
    return listProjectsInGroup(allowedGitTemplateGroup);
  }

  public List<GitProject> listConfigProjects() throws GitLabApiException {
    return listProjectsInGroup(allowedGitConfigGroup);
  }

  public List<GitProject> listExampleProjects() throws GitLabApiException {
    return listProjectsInGroup(allowedGitExampleGroup);
  }

  private List<GitProject> listProjectsInGroup(Long groupId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getGroupApi().getProjects(groupId).stream()
          .map(p -> new GitProject(p.getId(), p.getName(), p.getWebUrl(), p.getDescription()))
          .toList();
    }
  }

  private boolean checkFileExists(Long projectId, String branchName, String filePath) {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getRepositoryFileApi().getFile(projectId, filePath, branchName) != null;
    } catch (GitLabApiException e) {
      return false;
    }
  }

  public Tag getTag(Long projectId, String tagName) {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getTagsApi().getTag(projectId, tagName);
    } catch (GitLabApiException e) {
      return null;
    }
  }

  public Tag createTag(final Project project, final String tagName, final String commitId)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      if (gitLabApi.getTagsApi().getTags(project.getId()).stream()
          .noneMatch(t -> StringUtils.equals(t.getName(), tagName))) {
        return gitLabApi.getTagsApi().createTag(project.getId(), tagName, commitId);
      }
      throw new UnprocessableRequestException(
          GIT_TAG_EXISTS,
          "Tag named '"
              + tagName
              + "' already exists, "
              + "output project name: "
              + project.getName());
    }
  }

  public Commit createCommit(
      Project outputProject,
      String branchName,
      String message,
      List<FileToCommit> files,
      List<String> filesToDelete)
      throws GitLabApiException {
    Long projectId = outputProject.getId();

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      CommitPayload payload =
          new CommitPayload()
              .withBranch(branchName)
              .withCommitMessage(message)
              .withActions(
                  files.stream()
                      .map(
                          file -> {
                            CommitAction action = new CommitAction();
                            action.setAction(
                                checkFileExists(projectId, branchName, file.getFilePath())
                                    ? CommitAction.Action.UPDATE
                                    : CommitAction.Action.CREATE);
                            action.setContent(file.getFileContent());
                            action.setFilePath(file.getFilePath());
                            return action;
                          })
                      .toList());

      // files/dirs that will be deleted because they are not part of the generated files
      if (!filesToDelete.isEmpty()) {
        List<CommitAction> actions = new ArrayList<>(payload.getActions());
        for (String f : filesToDelete) {
          CommitAction action = new CommitAction();
          action.setAction(CommitAction.Action.DELETE);
          action.setFilePath(f);
          actions.add(action);
        }
        payload = payload.withActions(actions);
      }
      return gitLabApi.getCommitsApi().createCommit(projectId, payload);
    }
  }

  public List<GitReference> tagsAndCommits(long projectId, String reference)
      throws GitLabApiException {
    List<GitReference> result = new ArrayList<>();

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      Project gitProject = gitLabApi.getProjectApi().getProject(projectId);

      result.addAll(
          gitLabApi.getTagsApi().getTags(projectId).stream()
              .filter(t -> StringUtils.isEmpty(reference) || t.getName().contains(reference))
              .sorted(
                  (d1, d2) ->
                      d2.getCommit()
                          .getCommittedDate()
                          .compareTo(d1.getCommit().getCommittedDate()))
              .limit(TAG_COMMIT_LIMIT)
              .map(
                  p ->
                      new GitReference(
                          p.getName(),
                          p.getCommit().getShortId(),
                          p.getMessage(),
                          p.getCommit().getCommittedDate(),
                          ReferenceType.TAG))
              .toList());

      result.addAll(
          gitLabApi
              .getCommitsApi()
              .getCommits(projectId, gitProject.getDefaultBranch(), null)
              .stream()
              .filter(c -> StringUtils.isEmpty(reference) || c.getShortId().contains(reference))
              .sorted((d1, d2) -> d2.getCommittedDate().compareTo(d1.getCommittedDate()))
              .limit(TAG_COMMIT_LIMIT)
              .map(
                  p ->
                      new GitReference(
                          p.getId(),
                          p.getShortId(),
                          p.getMessage(),
                          p.getCommittedDate(),
                          ReferenceType.COMMIT))
              .toList());
      result.sort((o1, o2) -> o2.getCommitDate().compareTo(o1.getCommitDate()));
    }

    return result;
  }

  public List<String> listTagsForRepo(Long gitProjectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getTagsApi().getTags(gitProjectId).stream().map(Tag::getName).toList();
    }
  }

  public List<String> listFilesInRepo(Long projectId, String reference) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getRepositoryApi().getTree(projectId, null, reference).stream()
          .filter(f -> !f.getType().equals(TreeItem.Type.TREE))
          .map(TreeItem::getName)
          .toList();
    }
  }

  private GitProjectDetails parseGitProject(String project) throws GitLabApiException {
    String[] projectArr = project.split("/");

    int index = 0;
    Long projectGroup = allowedGitOutputGroup;

    while (index < (projectArr.length - 1)) {
      projectGroup = subGroupIdByName(projectGroup, projectArr[index]);
      index++;
    }

    return new GitProjectDetails(projectGroup, projectArr[projectArr.length - 1]);
  }

  private Long subGroupIdByName(Long parentGroupId, String groupName) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      GroupApi groupApi = gitLabApi.getGroupApi();
      return groupApi.getSubGroups(parentGroupId).stream()
          .filter(g -> g.getName().equals(groupName))
          .map(Group::getId)
          .findFirst()
          .orElse(null);
    }
  }

  public Project projectFromId(Long projectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      return gitLabApi.getProjectApi().getProject(projectId);
    }
  }

  public Commit getCommit(Long projectId, String revision) throws GitLabApiException {
    if (StringUtils.isEmpty(revision)) {
      return null;
    }

    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      return gitLabApi.getCommitsApi().getCommit(projectId, revision);
    }
  }

  public void deleteFile(Long projectId, String revision, String fileName, String commitMessage)
      throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {
      gitLabApi.getRepositoryFileApi().deleteFile(projectId, fileName, revision, commitMessage);
    }
  }

  public void renameProjectById(Long projectId, String newName) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      Project project = gitLabApi.getProjectApi().getProject(projectId);

      project.setName(newName);
      project.setPath(newName);

      gitLabApi.getProjectApi().updateProject(project);
    }
  }

  public void deleteProjectById(Long projectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      gitLabApi.getProjectApi().deleteProject(projectId);
    }
  }

  public void archiveProjectById(Long projectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      gitLabApi.getProjectApi().archiveProject(projectId);
    }
  }

  public void unarchiveProjectById(Long projectId) throws GitLabApiException {
    try (GitLabApi gitLabApi = new GitLabApi(gitServerAddress, token)) {

      gitLabApi.getProjectApi().unarchiveProject(projectId);
    }
  }

  /**
   * Resolving project URL from Git project ID. If the project couldn't be resolved the result will
   * be null!
   *
   * @param projectId the git project Id
   * @return the resolved web url, or null, if the project couldn't be resolved.
   */
  public String webUrlFromProjectId(Long projectId) {
    String resultUrl = null;
    try {
      Project project = this.projectFromId(projectId);
      resultUrl = project.getWebUrl();
    } catch (Exception e) {
    }

    return resultUrl;
  }

  public GitRef getRef(long projectId, String reference) {
    GitProject gitlabProject;
    try {
      gitlabProject = getGitProject(projectId);
    } catch (GitLabApiException e) {
      return new GitRef(ReferenceType.UNKNOWN, null, null);
    }

    try {
      Commit commit = getCommit(projectId, reference);
      if (commit != null) {
        return new GitRef(
            ReferenceType.COMMIT,
            commit.getShortId(),
            commit.getWebUrl().replace("-/commit/", "-/tree/"));
      }
    } catch (Exception e) {
      return new GitRef(ReferenceType.UNKNOWN, null, null);
    }

    try {
      Tag tag = getTag(projectId, reference);
      if (tag != null) {
        return new GitRef(
            ReferenceType.TAG,
            reference,
            tag.getCommit().getWebUrl().replace("-/commit/", "-/tree/"));
      }
    } catch (Exception e) {
      return new GitRef(ReferenceType.UNKNOWN, null, null);
    }

    return new GitRef(ReferenceType.UNKNOWN, null, null);
  }
}
