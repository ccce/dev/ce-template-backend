package eu.ess.ics.ce.template.common;

import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Application level utility functions
 *
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class Utils {
  // Util class, don't instantiate it!
  private Utils() {}

  private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

  /**
   * Clones a Date to make object immutable.
   *
   * @param dateToClone the date that has to be cloned.
   * @return <code>null</code>, if date was null <code>cloned date</code>, if date was not null
   */
  public static Date cloneDate(Date dateToClone) {
    return dateToClone == null ? null : new Date(dateToClone.getTime());
  }

  public static String generateProjectName(String templateProjectName, String configName) {
    String outputPostfix = StringUtils.removeEndIgnoreCase(configName, Literals.CONFIG_EXTENSION);

    return Literals.OUTPUT_PROJECT_PREFIX + templateProjectName + "-" + outputPostfix;
  }

  public static String concatUrl(String baseUrl, String segment) {

    if (StringUtils.isEmpty(baseUrl)) {
      if (!StringUtils.isEmpty(segment) && segment.startsWith("/")) {
        return segment;
      }

      return "/".concat(segment);
    }

    if (baseUrl.endsWith("/")) {
      return baseUrl.concat(segment);
    }

    return baseUrl.concat("/").concat(segment);
  }

  /**
   * Checks if fileName is valid for template file.
   *
   * @param templateName The name of the file that has to be checked
   * @return <code>true</code> if the fileName is valid to be template fileName <code>false</code>
   *     if fileName is not valid to be a template fileName
   */
  public static boolean checkTemplateFileNameConvention(String templateName) {
    return StringUtils.equals(templateName, Literals.ST_CMD_TEMPLATE_NAME)
        || Literals.iocshTemplatePattern.matcher(templateName).matches()
        || Literals.dbTemplatePattern.matcher(templateName).matches()
        || Literals.templateTemplatePattern.matcher(templateName).matches();
  }

  public static boolean isFileToCopy(String fileName) {
    // checking if filename-format is a VALID
    if (!Literals.allowedFileNamePattern.matcher(fileName).matches()) {
      return false;
    }

    return StringUtils.equals(fileName, Literals.IOC_JSON)
        || StringUtils.equals(fileName, Literals.ENVIRONMENT_FILE)
        || Literals.iocshPattern.matcher(fileName).matches()
        || Literals.dbPattern.matcher(fileName).matches()
        || Literals.templatePattern.matcher(fileName).matches()
        || Literals.substitutionPattern.matcher(fileName).matches()
        || Literals.plcPattern.matcher(fileName).matches();
  }

  public static long calculateVerNum(
      GitLabService gitLabService, IocTypeEntity iocType, List<Long> gitProjectIds) {
    long result = iocType.getMaxVersion() == null ? 1 : iocType.getMaxVersion();

    if ((gitProjectIds != null) && (!gitProjectIds.isEmpty())) {
      long maxVersion = 0;
      for (Long projectId : gitProjectIds) {
        try {
          List<String> gitTags = gitLabService.listTagsForRepo(projectId);
          long tmpMaxVersion = maxVerFromTags(gitTags);
          maxVersion = Math.max(maxVersion, tmpMaxVersion);
        } catch (Exception e) {
          LOGGER.debug("Could not fetch Tags for project");
        }
      }
      result = Math.max(result, maxVersion);
    }

    return result + 1;
  }

  private static long maxVerFromTags(List<String> gitTags) {
    if ((gitTags == null) || (gitTags.isEmpty())) {
      return 0;
    }

    long maxVersion = 0;
    for (String tag : gitTags) {
      if (tag.startsWith(Literals.VERSION_PREFIX)) {
        String tmpVer = tag.replace(Literals.VERSION_PREFIX, "");

        try {
          long currVer = Long.parseLong(tmpVer);
          maxVersion = Math.max(maxVersion, currVer);
        } catch (NumberFormatException e) {
          LOGGER.debug("Version number can not be converted", e);
        }
      }
    }

    return maxVersion;
  }
}
