/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package eu.ess.ics.ce.template.service.internal;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
@Service
public class TaskExecutorService {

  private final Map<Long, Executor> operationExecutors = new ConcurrentHashMap<>();

  @Transactional
  public void executeTask(Long operationId, Runnable task) {
    if (operationExecutors.containsKey(operationId)) {
      operationExecutors.get(operationId).execute(task);
    } else {
      ExecutorService executor = Executors.newSingleThreadExecutor();
      executor.execute(task);
      operationExecutors.put(operationId, executor);
    }
  }

  @Transactional
  public void completeOperation(Long operationId) {
    operationExecutors.remove(operationId);
  }
}
