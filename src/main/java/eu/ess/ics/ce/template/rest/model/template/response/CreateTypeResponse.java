package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record CreateTypeResponse(
    @NotNull String name,
    @NotNull @JsonProperty("type_project_id") long typeProjectId,
    @NotNull @JsonProperty("type_git_id") long typeGitId,
    @NotNull @JsonProperty("config_git_id") long configGitId) {}
