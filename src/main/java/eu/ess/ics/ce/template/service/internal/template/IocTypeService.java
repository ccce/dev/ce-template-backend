package eu.ess.ics.ce.template.service.internal.template;

import eu.ess.ics.ce.template.common.Utils;
import eu.ess.ics.ce.template.common.conversion.PagingUtil;
import eu.ess.ics.ce.template.exceptions.EntityNotFoundException;
import eu.ess.ics.ce.template.exceptions.InputValidationException;
import eu.ess.ics.ce.template.exceptions.PreconditionFailedException;
import eu.ess.ics.ce.template.exceptions.UnprocessableRequestException;
import eu.ess.ics.ce.template.repository.api.IInstanceLogRepository;
import eu.ess.ics.ce.template.repository.api.IIocInstanceRepository;
import eu.ess.ics.ce.template.repository.api.IOperationRepository;
import eu.ess.ics.ce.template.repository.api.ITemplateRepository;
import eu.ess.ics.ce.template.repository.entity.IocInstanceEntity;
import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import eu.ess.ics.ce.template.repository.entity.IocTypeEntity;
import eu.ess.ics.ce.template.repository.entity.OperationEntity;
import eu.ess.ics.ce.template.rest.model.jobs.response.JobType;
import eu.ess.ics.ce.template.rest.model.template.request.CreateTypeRequest;
import eu.ess.ics.ce.template.rest.model.template.request.ValidationConstants;
import eu.ess.ics.ce.template.rest.model.template.response.*;
import eu.ess.ics.ce.template.rest.model.types.request.InstancesToAttach;
import eu.ess.ics.ce.template.rest.model.types.response.AttachInstanceResponse;
import eu.ess.ics.ce.template.service.external.gitlab.GitLabService;
import eu.ess.ics.ce.template.service.internal.PagingLimitDto;
import eu.ess.ics.ce.template.service.internal.audit.AuditLogService;
import eu.ess.ics.ce.template.service.internal.jobs.dto.GenerationStatus;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class IocTypeService {
  private static final Logger LOGGER = LoggerFactory.getLogger(IocTypeService.class);

  private final GitLabService gitLabService;
  private final ITemplateRepository templateRepository;
  private final IIocInstanceRepository instanceRepository;
  private final IInstanceLogRepository logRepository;
  private final IOperationRepository operationRepository;
  private final AuditLogService auditLogService;
  private final PagingUtil utils;

  private static final String CONFIG_POSTFIX = "-config";

  public IocTypeService(
      GitLabService gitLabService,
      ITemplateRepository templateRepository,
      IIocInstanceRepository instanceRepository,
      IInstanceLogRepository logRepository,
      IOperationRepository operationRepository,
      AuditLogService auditLogService,
      PagingUtil utils) {
    this.gitLabService = gitLabService;
    this.templateRepository = templateRepository;
    this.utils = utils;
    this.instanceRepository = instanceRepository;
    this.logRepository = logRepository;
    this.operationRepository = operationRepository;
    this.auditLogService = auditLogService;
  }

  public IocTypeEntity storeIocType(
      String name,
      String description,
      String createdBy,
      long templateProjectId,
      long configurationProjectId) {

    IocTypeEntity iocType = new IocTypeEntity();
    iocType.setName(name);
    iocType.setCreatedBy(createdBy);
    iocType.setCreatedAt(ZonedDateTime.now());
    iocType.setTemplateGitProjectId(templateProjectId);
    iocType.setConfigGitProjectId(configurationProjectId);
    iocType.setDescription(description);
    iocType.setMaxVersion(0L);
    templateRepository.createIocType(iocType);
    auditLogService.logIocTypeCreation(iocType, createdBy);

    return iocType;
  }

  public IocTypeEntity findTemplateById(long templateId, boolean errorIfDeleted) {
    try {
      IocTypeEntity iocTypeEntity = templateRepository.findById(templateId);
      if (iocTypeEntity.isDeleted() && errorIfDeleted) {
        throw new EntityNotFoundException("Template", templateId);
      }
      return iocTypeEntity;
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("Template", templateId);
    }
  }

  public CreateTypeResponse createTemplateAndConfiguration(
      final CreateTypeRequest createRequest, final Long exampleProjectId, final String user)
      throws GitLabApiException, UnprocessableRequestException {

    if (exampleProjectId != null) {
      return createTypeAndConfigurationFromExample(
          createRequest.getTemplateName(), exampleProjectId, createRequest.getDescription(), user);
    }

    validateSimpleCreateRequest(createRequest);
    checkTemplateProjectIfExists(createRequest.getTemplateName());

    final String configName = calculateConfigRepoName(createRequest.getTemplateName());

    checkConfigProjectIfExists(configName);

    final Project templateProject =
        gitLabService.createTemplateProject(createRequest.getTemplateName());
    final Project configProject =
        gitLabService.createConfigProject(configName, createRequest.getIocs());
    return createIocType(
        createRequest.getTemplateName(),
        createRequest.getDescription(),
        user,
        templateProject,
        configProject);
  }

  public CreateTypeResponse createTypeAndConfigurationFromExample(
      final String templateName, long exampleProjectId, final String description, final String user)
      throws GitLabApiException, UnprocessableRequestException {
    validateCreateFromExampleRequest(templateName, exampleProjectId);
    checkTemplateProjectIfExists(templateName);

    final String configName = calculateConfigRepoName(templateName);

    checkConfigProjectIfExists(configName);

    final Project templateProject =
        gitLabService.createTemplateProjectFromExample(templateName, exampleProjectId);

    final Project configProject =
        gitLabService.createConfigProjectFromExample(configName, exampleProjectId);

    return createIocType(templateName, description, user, templateProject, configProject);
  }

  @NotNull
  private CreateTypeResponse createIocType(
      final String templateName,
      final String description,
      final String user,
      final Project templateProject,
      final Project configProject) {
    try {
      final IocTypeEntity iocTypeEntity =
          storeIocType(
              templateName, description, user, templateProject.getId(), configProject.getId());

      return new CreateTypeResponse(
          templateProject.getName(),
          iocTypeEntity.getId(),
          templateProject.getId(),
          configProject.getId());

    } catch (DataIntegrityViolationException e) {
      throw new UnprocessableRequestException(
          "Uniqueness violation",
          Objects.requireNonNull(e.getMessage()).contains("template_name_unique")
              ? "Template named '" + templateName + "' already exists in the database"
              : e.getMessage());
    }
  }

  private void checkConfigProjectIfExists(final String configName) throws GitLabApiException {
    if (gitLabService.checkConfigExists(configName)) {
      throw new UnprocessableRequestException(
          "Cannot create config project",
          "Configuration project named '" + configName + "' already exists");
    }
  }

  private void checkTemplateProjectIfExists(final String templateName) throws GitLabApiException {
    if (gitLabService.checkTemplateExists(templateName)) {
      throw new UnprocessableRequestException(
          "Cannot create template project",
          "Template project named '" + templateName + "' already exists");
    }
  }

  private String calculateConfigRepoName(String templateName) {
    return templateName + CONFIG_POSTFIX;
  }

  public PagedTypeResponse findAll(
      String createdBy,
      String query,
      boolean includeDeleted,
      Integer page,
      Integer limit,
      boolean listAll) {
    List<TypeBase> listResult = new ArrayList<>();

    PagingLimitDto pagingLimitDto = utils.pageLimitConverter(page, limit, listAll);

    String createdByString = StringUtils.isEmpty(createdBy) ? null : createdBy;
    String queryString = StringUtils.isEmpty(query) ? null : query;

    List<IocTypeEntity> templates =
        templateRepository.findAll(
            createdByString,
            queryString,
            includeDeleted,
            pagingLimitDto.getPage(),
            pagingLimitDto.getLimit());

    if (templates != null) {

      for (IocTypeEntity template : templates) {

        OperationEntity latestOperation = operationRepository.findLatestOperation(template.getId());

        listResult.add(
            convertToTemplateBase(
                template,
                instanceRepository.countInstancesForPaging(template.getId(), null),
                latestOperation == null ? null : latestOperation.getGitTag(),
                latestOperation == null ? null : latestOperation.getId()));
      }
    }

    long totalCount =
        templateRepository.countForPaging(createdByString, queryString, includeDeleted);

    return new PagedTypeResponse(
        listResult,
        totalCount,
        listResult.size(),
        pagingLimitDto.getPage(),
        pagingLimitDto.getLimit());
  }

  public Type getIocTypeById(long id) {
    IocTypeEntity iocTypeEntity = findTemplateById(id, false);
    return convertType(iocTypeEntity);
  }

  public Type updateTemplate(
      OAuth2User user, long iocTypeId, String templateNewName, String description)
      throws GitLabApiException {
    IocTypeEntity iocTypeEntity = findTemplateById(iocTypeId, true);
    try {

      if (StringUtils.isNotEmpty(templateNewName)
          && (!StringUtils.equals(templateNewName, iocTypeEntity.getName()))) {
        renameIocType(user, iocTypeId, templateNewName);
      }

      if (StringUtils.isNotEmpty(description)
          && (!StringUtils.equals(description, iocTypeEntity.getDescription()))) {
        iocTypeEntity.setDescription(description);
        templateRepository.updateIocType(iocTypeEntity);
        auditLogService.logIocTypeModification(iocTypeEntity, user.getName());
      }

    } catch (DataIntegrityViolationException e) {
      throw new UnprocessableRequestException(
          "Uniqueness violation",
          Objects.requireNonNull(e.getMessage()).contains("configuration_git_project_unique")
              ? "Config Git project is already bound to a template"
              : "Template Git project is already registered");
    }
    return convertType(iocTypeEntity);
  }

  public static void validateInstanceNames(List<String> instanceNames) {
    Pattern instanceNamePattern = Pattern.compile(ValidationConstants.IOC_INSTANCE_NAME_REGEX);
    for (String instanceName : instanceNames) {
      Matcher matcher = instanceNamePattern.matcher(instanceName);
      if (!matcher.matches()) {
        throw new InputValidationException(
            "'" + instanceName + "': " + ValidationConstants.IOC_INSTANCE_NAME_ALLOWED_FORMAT);
      }
    }
  }

  private void validateCreateFromExampleRequest(
      final String templateName, final long exampleProjectId) throws GitLabApiException {
    validateTemplateName(templateName);
    validateExampleProject(exampleProjectId);
  }

  private void validateSimpleCreateRequest(CreateTypeRequest request) {
    validateTemplateName(request.getTemplateName());
    if (request.getIocs() != null && !request.getIocs().isEmpty()) {
      validateInstanceNames(request.getIocs());
    }
  }

  private void validateExampleProject(long exampleProjectId) throws GitLabApiException {
    if (gitLabService.listExampleProjects().stream()
        .noneMatch(gitProject -> gitProject.getProjectId() == exampleProjectId)) {
      throw new InputValidationException(ValidationConstants.INVALID_EXAMPLE_PROJECT);
    }
  }

  private void validateTemplateName(final String templateName) {
    if (StringUtils.isEmpty(templateName)) {
      throw new InputValidationException(ValidationConstants.TEMPLATE_NAME_REQUIRED);
    }
    Pattern templateNamePattern = Pattern.compile(ValidationConstants.TYPE_NAME_REGEX);
    Matcher matcher = templateNamePattern.matcher(templateName);
    if (!matcher.matches()) {
      throw new InputValidationException(ValidationConstants.TEMPLATE_NAME_ALLOWED_FORMAT);
    }
  }

  public Type unarchiveIocType(OAuth2User user, long iocTypeId) throws GitLabApiException {
    LOGGER.debug("{} tries to unarchive IOC type with ID: {}", user.getName(), iocTypeId);

    LOGGER.debug("Unarchiving IOC type, iocTypeId: {}", iocTypeId);
    IocTypeEntity iocTypeEntity = templateRepository.findById(iocTypeId);

    if (!iocTypeEntity.isDeleted()) {
      throw new PreconditionFailedException("The IOC type is not archived.");
    }

    iocTypeEntity.setDeleted(false);
    templateRepository.updateIocType(iocTypeEntity);
    auditLogService.logIocTypeRestoration(iocTypeEntity, user.getName());

    LOGGER.debug("Unarchiving template from GitLab, iocTypeId: {}", iocTypeEntity);
    gitLabService.unarchiveProjectById(iocTypeEntity.getTemplateGitProjectId());

    LOGGER.debug("Unarchiving config from GitLab, iocTypeId: {}", iocTypeEntity);
    gitLabService.unarchiveProjectById(iocTypeEntity.getConfigGitProjectId());

    List<IocInstanceEntity> generatedIocInstances =
        instanceRepository.findAll(iocTypeId, null, null, 0, null);

    for (IocInstanceEntity iocInstance : generatedIocInstances) {
      LOGGER.debug(
          "Unarchiving IOC instance project from GitLab, iocInstanceId: {}", iocInstance.getId());
      gitLabService.unarchiveProjectById(iocInstance.getGitProjectId());
    }

    return convertType(iocTypeEntity);
  }

  public Type archiveIocType(OAuth2User user, long iocTypeId) throws GitLabApiException {
    LOGGER.debug("{} tries to archive IOC type with ID: {}", user.getName(), iocTypeId);

    LOGGER.debug("Archiving IOC type, and config from DB, iocTypeId: {}", iocTypeId);
    IocTypeEntity iocTypeEntity = findTemplateById(iocTypeId, true);

    iocTypeEntity.setDeleted(true);
    templateRepository.updateIocType(iocTypeEntity);
    auditLogService.logIocTypeDeletion(iocTypeEntity, user.getName());

    LOGGER.debug("Archiving template project from GitLab, iocTypeId: {}", iocTypeEntity);
    gitLabService.archiveProjectById(iocTypeEntity.getTemplateGitProjectId());

    LOGGER.debug("Archiving config from GitLab, iocTypeId: {}", iocTypeEntity);
    gitLabService.archiveProjectById(iocTypeEntity.getConfigGitProjectId());

    List<IocInstanceEntity> generatedIocInstances =
        instanceRepository.findAll(iocTypeId, null, null, 0, null);

    for (IocInstanceEntity iocInstance : generatedIocInstances) {
      LOGGER.debug(
          "Archiving IOC instance project from GitLab, iocInstanceId: {}", iocInstance.getId());
      gitLabService.archiveProjectById(iocInstance.getGitProjectId());
    }

    return convertType(iocTypeEntity);
  }

  public void deleteIocType(OAuth2User user, long iocTypeId) throws GitLabApiException {
    LOGGER.debug("{} tries to delete IOC type with ID: {}", user.getName(), iocTypeId);

    IocTypeEntity iocTypeEntity = templateRepository.findById(iocTypeId);

    LOGGER.debug("Deleting template from GitLab, iocTypeId: {}", iocTypeId);
    gitLabService.deleteProjectById(iocTypeEntity.getTemplateGitProjectId());

    if (iocTypeEntity.getConfigGitProjectId() != null) {
      LOGGER.debug("Deleting config from GitLab, iocTypeId: {}", iocTypeId);
      gitLabService.deleteProjectById(iocTypeEntity.getConfigGitProjectId());
    }

    LOGGER.debug("Deleting IOC instances, iocTypeId: {}", iocTypeId);
    deleteIocInstancesAndOperationsByType(iocTypeId);

    LOGGER.debug("Deleting IOC type, and config from DB, iocTypeId: {}", iocTypeId);
    auditLogService.deleteIocTypeHistory(iocTypeId);
    templateRepository.deleteIocTypeAndConfigById(iocTypeId);

    LOGGER.debug("Template and config are deleted, iocTypeId: {}", iocTypeId);
  }

  private void deleteIocInstancesAndOperationsByType(final long iocTypeId)
      throws GitLabApiException {
    final List<IocInstanceEntity> iocInstances =
        instanceRepository.findAll(iocTypeId, null, null, 0, null);

    for (final IocInstanceEntity instance : iocInstances) {
      if (instance.getGitProjectId() != null) {
        gitLabService.deleteProjectById(instance.getGitProjectId());
      }
      logRepository.deleteLogsForIocInstance(instance.getId());
      instanceRepository.deleteInstance(instance.getId());
    }

    List<OperationEntity> operations =
        operationRepository.findOperations(null, iocTypeId, null, null, null);
    for (OperationEntity operation : operations) {
      logRepository.deleteLogsForOperation(operation.getId());
      operationRepository.deleteOperation(operation.getId());
    }

    operationRepository.removePredecessorLinkForType(iocTypeId);
    operationRepository.deleteOrphanOperations();
  }

  public void renameIocType(OAuth2User user, long iocTypeId, String newTemplateName)
      throws GitLabApiException {
    LOGGER.debug("{} tries to rename IOC type with ID: {}", user.getName(), iocTypeId);

    IocTypeEntity iocTypeEntity = templateRepository.findById(iocTypeId);

    gitLabService.renameProjectById(iocTypeEntity.getTemplateGitProjectId(), newTemplateName);

    iocTypeEntity.setName(newTemplateName);
    templateRepository.updateIocType(iocTypeEntity);
    auditLogService.logIocTypeModification(iocTypeEntity, user.getName());

    if (iocTypeEntity.getConfigGitProjectId() != null) {
      LOGGER.debug("Template renamed, trying to rename configs, iocTypeId: {}", iocTypeId);
      gitLabService.renameProjectById(
          iocTypeEntity.getConfigGitProjectId(), calculateConfigRepoName(newTemplateName));
    } else {
      LOGGER.debug("No config found to rename, iocTypeId: {}", iocTypeId);
    }

    List<IocInstanceEntity> instances = instanceRepository.findAll(iocTypeId, null, null, 0, null);

    if ((instances != null) && (!instances.isEmpty())) {
      LOGGER.debug("Renaming instance repositories");

      for (IocInstanceEntity instance : instances) {
        gitLabService.renameProjectById(
            instance.getGitProjectId(),
            Utils.generateProjectName(newTemplateName, instance.getConfigurationName()));
        instance.setIocTypeName(newTemplateName);
        instance.setInstanceName(
            Utils.generateProjectName(newTemplateName, instance.getConfigurationName()));
        instanceRepository.updateInstance(instance);
      }
    }
  }

  public PagedTypeHistoryResponse getIocTypeHistory(long typeId, Integer page, Integer limit) {
    try {
      templateRepository.findById(typeId);
      return auditLogService.getIocTypeHistory(typeId, page, limit);
    } catch (EmptyResultDataAccessException e) {
      throw new EntityNotFoundException("Template", typeId);
    }
  }

  public List<AttachInstanceResponse> attachInstances(
      long typeId, List<InstancesToAttach> request, String createdBy) {

    List<AttachInstanceResponse> result = new ArrayList<>();

    if (!request.isEmpty()) {
      IocTypeEntity iocType = templateRepository.findById(typeId);

      checkAttachReq(request);

      final OperationEntity operationEntity = new OperationEntity();

      long newVersion =
          Utils.calculateVerNum(
              gitLabService,
              iocType,
              request.stream().map(InstancesToAttach::gitProjectId).toList());

      operationEntity.setIocType(iocType);
      operationEntity.setTypeName(iocType.getName());
      operationEntity.setCreatedBy(createdBy);
      operationEntity.setCreatedAt(ZonedDateTime.now());
      operationEntity.setGitTag("");
      operationEntity.setConfigurationRevision("");
      // Tag should be filled to show latest version on type list
      operationEntity.setGitTag("");
      operationEntity.setTemplateRevision("");
      operationEntity.setJobType(JobType.ATTACH.name());
      operationEntity.setVersion(newVersion);
      operationRepository.createOperation(operationEntity);

      for (InstancesToAttach i : request) {

        try {

          IocInstanceEntity inst = new IocInstanceEntity();
          inst.setCreatedAt(ZonedDateTime.now());

          String instanceName = Utils.generateProjectName(iocType.getName(), i.newName());
          gitLabService.renameProjectById(i.gitProjectId(), instanceName);

          inst.setIocTypeName(iocType.getName());
          inst.setConfigurationName(i.newName());
          inst.setIocType(iocType);
          inst.setGitProjectId(i.gitProjectId());
          // TAG should be filled to show link for details
          inst.setGitTag("");
          inst.setGitRevision("");
          inst.setCreatedBy(createdBy);
          inst.setStatus(GenerationStatus.SUCCESSFUL.toString());
          inst.setVersion(newVersion);
          inst.setFinishedAt(ZonedDateTime.now());
          inst.setInstanceName(instanceName);

          instanceRepository.create(inst);

          IocInstanceLogEntity log = new IocInstanceLogEntity();
          log.setIocInstance(inst);
          log.setCreatedAt(ZonedDateTime.now());
          log.setOperation(operationEntity);
          log.setType(JobType.ATTACH.name());
          log.setTypeName(iocType.getName());
          log.setStatus(GenerationStatus.SUCCESSFUL.toString());
          log.setDescription("IOC Attached");
          log.setFinishedAt(ZonedDateTime.now());
          log.setVersion(newVersion);
          log.setCreatedBy(createdBy);
          logRepository.create(log);

          iocType.setMaxVersion(newVersion);
          templateRepository.updateIocType(iocType);

          result.add(
              new AttachInstanceResponse(i.gitProjectId(), i.newName(), i.gitProjectId(), "Ok"));

        } catch (Exception e) {
          LOGGER.error("Error while trying to attach instance: {}", i.newName(), e);
          result.add(
              new AttachInstanceResponse(
                  i.gitProjectId(), i.newName(), null, "Error: " + e.getMessage()));
        }
      }

      operationEntity.setFinishedAt(ZonedDateTime.now());
      operationRepository.updateOperation(operationEntity);
      operationRepository.deleteOrphanOperations();
    }
    return result;
  }

  private void checkAttachReq(List<InstancesToAttach> request) {
    for (InstancesToAttach inst : request) {
      if (instanceRepository.countInstancesWithRepoId(inst.gitProjectId()) > 0L) {
        throw new InputValidationException(
            String.format("Git repository with ID (%s) already exists", inst.gitProjectId()));
      }
    }
  }

  private Type convertType(IocTypeEntity iocTypeEntity) {
    Project templateProject = null;
    try {
      templateProject = gitLabService.projectFromId(iocTypeEntity.getTemplateGitProjectId());
    } catch (GitLabApiException e) {
      LOGGER.error(
          "Cannot find on gitlab the template by ID: {}",
          iocTypeEntity.getTemplateGitProjectId(),
          e);
    }
    Project configProject = null;
    try {
      configProject = gitLabService.projectFromId(iocTypeEntity.getConfigGitProjectId());
    } catch (GitLabApiException e) {
      LOGGER.error(
          "Cannot find on gitlab the config project by ID: {}",
          iocTypeEntity.getConfigGitProjectId(),
          e);
    }

    OperationEntity latestOperation =
        operationRepository.findLatestOperation(iocTypeEntity.getId());

    return new Type(
        iocTypeEntity.getId(),

        // TODO Discuss which name must be retrieved: stored or actual from Git
        //  (decision may affect template search as well!)
        iocTypeEntity.getName(),
        iocTypeEntity.getDescription(),
        templateProject == null ? iocTypeEntity.getTemplateGitProjectId() : templateProject.getId(),
        templateProject == null ? null : templateProject.getWebUrl(),
        configProject == null ? iocTypeEntity.getConfigGitProjectId() : configProject.getId(),
        configProject == null ? null : configProject.getWebUrl(),
        iocTypeEntity.getCreatedBy(),
        iocTypeEntity.getCreatedAt(),
        instanceRepository.countInstancesForPaging(iocTypeEntity.getId(), null),
        latestOperation == null ? null : latestOperation.getGitTag(),
        latestOperation == null ? null : latestOperation.getId(),
        iocTypeEntity.isDeleted());
  }

  public TypeBase convertToTemplateBase(
      IocTypeEntity iocTypeEntity,
      long numberOfInstances,
      String latestInstanceRef,
      Long latestOperationId) {

    return new TypeBase(
        iocTypeEntity.getId(),

        // TODO Discuss which name must be retrieved: stored or actual from Git
        //  (decision may affect template search as well!)
        // templateProject != null ? templateProject.getName() : iocTypeEntity.getName(),
        iocTypeEntity.getName(),
        iocTypeEntity.getDescription(),
        iocTypeEntity.getTemplateGitProjectId(),
        iocTypeEntity.getConfigGitProjectId(),
        iocTypeEntity.getCreatedBy(),
        iocTypeEntity.getCreatedAt(),
        numberOfInstances,
        latestInstanceRef,
        latestOperationId,
        iocTypeEntity.isDeleted());
  }
}
