package eu.ess.ics.ce.template.rest.model.types.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record IocPreview(
    @JsonProperty("project_name") String repositoryName,
    @JsonProperty("configuration_schema") String configSchema,
    @JsonProperty("substituted_content") String substitutedContent) {
  public IocPreview(String repositoryName, String configSchema, String substitutedContent) {
    this.repositoryName = repositoryName;
    this.configSchema = configSchema;
    this.substitutedContent = substitutedContent;
  }

  @Override
  public String repositoryName() {
    return repositoryName;
  }

  @Override
  public String configSchema() {
    return configSchema;
  }
}
