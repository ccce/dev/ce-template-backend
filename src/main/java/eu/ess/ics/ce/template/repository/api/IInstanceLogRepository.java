package eu.ess.ics.ce.template.repository.api;

import eu.ess.ics.ce.template.repository.entity.IocInstanceLogEntity;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public interface IInstanceLogRepository {

  @Transactional
  IocInstanceLogEntity create(IocInstanceLogEntity iocInstanceLogEntity);

  @Transactional
  void deleteLogsRelatedToInstance(long typeId, long gitRepoId, String configName);

  List<IocInstanceLogEntity> findByOperationId(long operationId, Integer page, Integer limit);

  long countForPaging(long operationId);

  List<IocInstanceLogEntity> findLogsForIocInstance(long operationId, long instanceId);

  @Transactional
  void deleteLogsForIocInstance(long instanceId);

  @Transactional
  void deleteLogsForOperation(long operationId);

  @Transactional
  void updateLogEntry(IocInstanceLogEntity iocInstanceLogEntity);

  IocInstanceLogEntity findLastLogEntry(long instanceId, long operationId);

  List<IocInstanceLogEntity> findLastLogEntriesForOperation(long operationId);

  List<IocInstanceLogEntity> findLastLogEntries();
}
