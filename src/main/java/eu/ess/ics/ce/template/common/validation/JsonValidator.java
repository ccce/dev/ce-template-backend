package eu.ess.ics.ce.template.common.validation;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.ess.ics.ce.template.exceptions.InvalidJsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class JsonValidator {
  public static void isValid(String json) throws InvalidJsonSyntaxException {
    try {
      new ObjectMapper().readTree(json);
    } catch (JacksonException e) {
      try {
        new JSONObject(json);
      } catch (JSONException je) {
        throw new InvalidJsonSyntaxException(je.getMessage());
      }
      throw new InvalidJsonSyntaxException(e.getMessage());
    }
  }
}
