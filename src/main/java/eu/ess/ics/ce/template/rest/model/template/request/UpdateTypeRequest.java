package eu.ess.ics.ce.template.rest.model.template.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record UpdateTypeRequest(
    @JsonProperty("type_new_name") String typeNewName, String description) {}
