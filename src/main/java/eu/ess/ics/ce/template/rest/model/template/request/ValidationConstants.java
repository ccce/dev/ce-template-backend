package eu.ess.ics.ce.template.rest.model.template.request;

/**
 * @author <a href="mailto:domonkos.gulyas@ess.eu">Domonkos Gulyas</a>
 */
public final class ValidationConstants {
  public static final String TYPE_NAME_REGEX = "^(?=.{1,20}$)[a-z0-9]+(_[a-z0-9]+)*$";
  public static final String IOC_INSTANCE_NAME_REGEX = "^(?=.{1,20}$)[a-z0-9]+(-[a-z0-9]+)*$";
  public static final String TEMPLATE_NAME_REQUIRED = "'type_name' parameter is required";
  public static final String TEMPLATE_NAME_ALLOWED_FORMAT =
      "Only lowercase alphanumeric chars and underscores are allowed in template name (max 20 chars)";
  public static final String IOC_INSTANCE_NAME_ALLOWED_FORMAT =
      "Only lowercase alphanumeric chars and hyphens are allowed in instance name (max 20 chars)";

  public static final String INVALID_EXAMPLE_PROJECT =
      "The given Gitlab project is not an example project.";

  private ValidationConstants() {}
}
