package eu.ess.ics.ce.template.exceptions;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class InputValidationException extends CcceException {
  public InputValidationException(String description) {
    super("Invalid request", description);
  }
}
