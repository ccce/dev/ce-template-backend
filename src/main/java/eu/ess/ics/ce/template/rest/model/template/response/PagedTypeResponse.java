package eu.ess.ics.ce.template.rest.model.template.response;

import eu.ess.ics.ce.template.rest.model.PagedResponse;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public class PagedTypeResponse extends PagedResponse {
  private final List<TypeBase> types;

  public PagedTypeResponse(
      List<TypeBase> types,
      @NotNull long totalCount,
      @NotNull int listSize,
      @NotNull int pageNumber,
      Integer limit) {
    super(totalCount, listSize, pageNumber, limit);
    this.types = types;
  }

  public List<TypeBase> getTypes() {
    return types;
  }
}
