package eu.ess.ics.ce.template;

import com.google.common.collect.ImmutableList;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(
    exclude = {SecurityAutoConfiguration.class, UserDetailsServiceAutoConfiguration.class})
public class CeTemplatingBackendApplication {

  private static final String OPENAPI_SERVER = "openapi.server";
  private final Environment env;

  public CeTemplatingBackendApplication(Environment env) {
    this.env = env;
  }

  public static void main(String[] args) {
    SpringApplication.run(CeTemplatingBackendApplication.class, args);
  }

  @Bean
  OpenAPI customOpenAPI(@Value("${api.version}") String appVersion) {
    return new OpenAPI()
        .info(
            new Info()
                .title("CE template API")
                .version(appVersion)
                .description("CE template backend"))
        .servers(
            ImmutableList.of(
                new Server()
                    .url(env.getProperty(OPENAPI_SERVER))
                    .description("CE template backend")));
  }

  @Bean
  WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry
            .addMapping("/**")
            .allowedOriginPatterns("*")
            .allowCredentials(true)
            .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE");
      }
    };
  }
}
