package eu.ess.ics.ce.template.rest.model.template.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
public record TypeFile(
    @NotNull @JsonProperty("type_id") Long typeId,
    @NotNull @JsonProperty("type_revision") String typeRevision,
    @NotNull String name,
    String content) {}
