ALTER TABLE ioc_instance ADD status text NULL;
ALTER TABLE ioc_instance ADD finished_at timestamp NULL;
ALTER TABLE ioc_instance ADD error_message text NULL;

AlTER TABLE ioc_instance ALTER COLUMN git_revision DROP NOT NULL;
AlTER TABLE ioc_instance ALTER COLUMN git_tag DROP NOT NULL;
AlTER TABLE ioc_instance ALTER COLUMN git_project_id DROP NOT NULL;

UPDATE ioc_instance inst
SET finished_at = (SELECT finished_at FROM operation op
                   WHERE inst.operation_id = op.id);

UPDATE ioc_instance
SET status = 'SUCCESSFUL';
