ALTER TABLE ioc_type ADD max_version BIGINT NOT NULL DEFAULT 0;

UPDATE ioc_type type
SET max_version = COALESCE((SELECT MAX(operation.version) FROM operation WHERE operation.ioc_type_id = type.id), 0);

ALTER TABLE operation DROP COLUMN version;
