ALTER TABLE ioc_type RENAME COLUMN git_project_id TO template_git_project_id;

ALTER TABLE ioc_type
    ADD config_git_project_id text NULL;

ALTER TABLE ioc_instance
    ADD ioc_type_name text NULL;


UPDATE ioc_type it
SET config_git_project_id = (SELECT git_project_id FROM configuration config WHERE config.template_id = it.id)
WHERE EXISTS
              (SELECT * FROM configuration config WHERE config.template_id = it.id);

CREATE TABLE IF NOT EXISTS operation
(
    id
    bigserial
    NOT
    NULL,
    ioc_type_id
    bigint
    NOT
    NULL,
    created_at
    timestamp
    NOT
    NULL
    DEFAULT
    now
(
),
    ioc_type_name text NOT NULL,
    version bigint NOT NULL,
    git_tag text NOT NULL,
    created_by text NOT NULL,
    finished_at timestamp NULL,
    CONSTRAINT operation_pk PRIMARY KEY
(
    id
),
    CONSTRAINT operation_fk FOREIGN KEY
(
    ioc_type_id
) REFERENCES ioc_type
(
    id
)
    );


INSERT INTO operation (ioc_type_id, ioc_type_name, version, git_tag, created_by, created_at, finished_at)
SELECT ii.type_id, '', ii.version, 0, '', now(), now()
FROM ioc_instance ii
GROUP BY ii.type_id, ii.version;


UPDATE operation op
SET created_by = (SELECT inst.created_by
                  FROM ioc_instance inst
                  WHERE inst.type_id = op.ioc_type_id
                    AND inst.version = op.version
    LIMIT 1)
  , created_at = (
SELECT inst.created_at
FROM ioc_instance inst
WHERE inst.type_id = op.ioc_type_id
  AND inst.version = op.version
    LIMIT 1)
    , finished_at = (
SELECT inst.created_at
FROM ioc_instance inst
WHERE inst.type_id = op.ioc_type_id
  AND inst.version = op.version
    LIMIT 1)
    , git_tag = (
SELECT inst.git_tag
FROM ioc_instance inst
WHERE inst.type_id = op.ioc_type_id
  AND inst.version = op.version
    LIMIT 1)
WHERE EXISTS
    (SELECT *
    FROM ioc_instance inst
    WHERE inst.type_id = op.ioc_type_id
  AND inst.version = op.version);


UPDATE operation op
SET ioc_type_name = (SELECT it.name FROM ioc_type it WHERE it.id = op.ioc_type_id)
WHERE EXISTS
              (SELECT * FROM ioc_type it WHERE it.id = op.ioc_type_id);


UPDATE ioc_instance ii
SET ioc_type_name = (SELECT it.name FROM ioc_type it WHERE it.id = ii.type_id)
WHERE EXISTS
              (SELECT * FROM ioc_type it WHERE it.id = ii.type_id);


UPDATE ioc_instance inst
SET ioc_type_name = (SELECT it.name FROM ioc_type it WHERE it.id = inst.type_id)
WHERE EXISTS
              (SELECT * FROM ioc_type it WHERE it.id = inst.type_id);


ALTER TABLE ioc_instance
    ADD operation_id bigint NULL;
ALTER TABLE ioc_instance
    ADD CONSTRAINT ioc_instance_fk FOREIGN KEY (operation_id) REFERENCES operation (id);


UPDATE ioc_instance inst
SET operation_id = (SELECT op.id
                    FROM operation op
                    WHERE op.ioc_type_id = inst.type_id
                      AND op.version = inst.version)
WHERE EXISTS
          (SELECT *
           FROM operation op
           WHERE op.ioc_type_id = inst.type_id
             AND op.version = inst.version);

ALTER TABLE ioc_instance DROP CONSTRAINT ioc_instance_template_fk;
ALTER TABLE ioc_instance DROP COLUMN type_id;

DROP TABLE IF EXISTS configuration;
