/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

CREATE TABLE IF NOT EXISTS log
(
    id bigserial NOT NULL,
    ioc_instance_id bigint NOT NULL,
    description text NOT NULL,
    created_at ${timetamp_with_timezone_type} NOT NULL,
    type text NOT NULL,
    status text NOT NULL,
    CONSTRAINT operation_log_pk PRIMARY KEY (id),
    CONSTRAINT instance_fk FOREIGN KEY (ioc_instance_id) REFERENCES ioc_instance (id)
);


INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, 'IOC has been added to the queue', created_at, 'CREATE_IOC', 'QUEUED'
FROM ioc_instance;

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, 'Started processing IOC', created_at, 'CREATE_IOC', 'STARTED'
FROM ioc_instance WHERE status != 'QUEUED';

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, CONCAT('Git repository has been created with ID ', git_project_id), created_at, 'CREATE_IOC', 'GIT_PROJECT_CREATED'
FROM ioc_instance WHERE git_project_id IS NOT NULL;

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, 'Files have been processed and commited', created_at, 'CREATE_IOC', 'TEMPLATE_PROCESSING_AND_COMMIT'
FROM ioc_instance WHERE git_revision IS NOT NULL;

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, CONCAT('Git repository has been tagged with ', git_tag), created_at, 'CREATE_IOC', 'TAG_CREATED'
FROM ioc_instance WHERE git_tag IS NOT NULL;

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, 'IOC has been processed successfully', finished_at, 'CREATE_IOC', 'SUCCESSFUL'
FROM ioc_instance WHERE status = 'SUCCESSFUL';

INSERT INTO log
(ioc_instance_id,
 description,
 created_at,
 type,
 status)
SELECT id, error_message, finished_at, 'CREATE_IOC', 'FAILED'
FROM ioc_instance WHERE status = 'FAILED';

ALTER TABLE ioc_instance DROP COLUMN error_message;
