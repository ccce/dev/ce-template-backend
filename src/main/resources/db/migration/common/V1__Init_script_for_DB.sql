CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE IF NOT EXISTS template (
                                   id bigserial NOT NULL,
                                   name text NOT NULL,
                                   created_at timestamp(3) NOT NULL DEFAULT now(),
                                   owner text NOT NULL,
                                   git_project_id int8 NOT NULL,
                                   CONSTRAINT template_name_unique UNIQUE (name),
                                   CONSTRAINT template_pk PRIMARY KEY (id),
                                   CONSTRAINT template_project_id_unique UNIQUE (git_project_id)
);

CREATE TABLE IF NOT EXISTS configuration (
                                        id bigserial NOT NULL,
                                        git_project_id int8 NOT NULL,
                                        created_at timestamp(3) NOT NULL DEFAULT now(),
                                        template_id int8 NULL,
                                        CONSTRAINT configuration_git_project_unique UNIQUE (git_project_id),
                                        CONSTRAINT configuration_pk PRIMARY KEY (id),
                                        CONSTRAINT configuration_template_fk FOREIGN KEY (template_id) REFERENCES template(id)
);


CREATE TABLE IF NOT EXISTS ioc_instance (
                                     id bigserial NOT NULL,
                                     template_id int8 NOT NULL,
                                     template_git_project_id int8 NOT NULL,
                                     template_revision text NOT NULL,
                                     configuration_git_project_id int8 NOT NULL,
                                     configuration_revision text NOT NULL,
                                     created_by text NOT NULL,
                                     created_at timestamp(3) NOT NULL DEFAULT now(),
                                     git_project_id int8 NOT NULL,
                                     git_revision text NOT NULL,
                                     git_tag text NOT NULL,
                                     configuration_name text NOT NULL,
                                     version int8 NOT NULL DEFAULT 1,
                                     CONSTRAINT ioc_instance_pk PRIMARY KEY (id),
                                     CONSTRAINT ioc_instance_template_fk FOREIGN KEY (template_id) REFERENCES template(id)
);
