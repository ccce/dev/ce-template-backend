/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

ALTER TABLE ioc_instance DROP COLUMN template_git_project_id;
ALTER TABLE ioc_instance DROP COLUMN configuration_git_project_id;
ALTER TABLE ioc_instance ADD ioc_type_id bigint NULL;
ALTER TABLE ioc_instance ADD CONSTRAINT ioc_instance_ioc_type_fk FOREIGN KEY (ioc_type_id) REFERENCES ioc_type(id);
ALTER TABLE ioc_instance ADD instance_name text NULL;

UPDATE ioc_instance SET ioc_type_id = (SELECT opr.ioc_type_id FROM operation opr WHERE ioc_instance.operation_id = opr.id);

ALTER TABLE log RENAME TO ioc_instance_log;
ALTER TABLE ioc_instance_log ADD operation_id bigint NULL;
ALTER TABLE ioc_instance_log ADD CONSTRAINT ioc_instance_log_operation_fk FOREIGN KEY (operation_id) REFERENCES operation(id);
ALTER TABLE operation ADD job_type text NULL;
ALTER TABLE operation ADD "version" bigint NULL;
ALTER TABLE ioc_instance_log ADD created_by text NULL;
ALTER TABLE ioc_instance_log ADD finished_at ${timetamp_with_timezone_type} NULL;
ALTER TABLE ioc_instance_log ADD "version" bigint NULL;
ALTER TABLE ioc_instance_log ADD ioc_type_name text NULL;

UPDATE ioc_instance_log SET operation_id = (SELECT ii.operation_id from ioc_instance ii WHERE ii.id = ioc_instance_log.ioc_instance_id);
UPDATE ioc_instance_log SET created_by = (SELECT ii.created_by from ioc_instance ii WHERE ii.id = ioc_instance_log.ioc_instance_id);
UPDATE ioc_instance_log SET finished_at = created_at;
UPDATE ioc_instance_log SET "version" = (SELECT ii.version from ioc_instance ii WHERE ii.id = ioc_instance_log.ioc_instance_id);
UPDATE ioc_instance_log SET ioc_type_name = (SELECT ii.ioc_type_name from ioc_instance ii WHERE ii.id = ioc_instance_log.ioc_instance_id);

UPDATE operation o SET "version" = (SELECT cast(REPLACE(git_tag, 'Version-', '') as bigint ) FROM operation where id = o.id);
UPDATE ioc_instance SET instance_name = 'e3-ioc-' || ioc_type_name || '-' || configuration_name ;

WITH latest_instance AS (
    SELECT
        i.ID AS inst_id,
        i.ioc_type_name,
        i.configuration_name
    FROM ioc_instance i
             INNER JOIN (
        SELECT
            ioc_type_name,
            configuration_name,
            MAX(created_at) AS max_created_at
        FROM ioc_instance
        GROUP BY ioc_type_name, configuration_name
    ) latest
                        ON i.ioc_type_name = latest.ioc_type_name
                            AND i.configuration_name = latest.configuration_name
                            AND i.created_at = latest.max_created_at
)
UPDATE ioc_instance_log
SET ioc_instance_id = latest_instance.inst_id FROM latest_instance
WHERE exists (select io.id from ioc_instance io where ioc_instance_log.ioc_instance_id = io.id and io.ioc_type_name = latest_instance.ioc_type_name and io.configuration_name = latest_instance.configuration_name);

ALTER TABLE ioc_instance DROP operation_id;

DELETE FROM ioc_instance ie
WHERE (ie.ioc_type_id, ie.configuration_name, ie.version) NOT IN
  (SELECT f.ioc_type_id, f.configuration_name, MAX(f.version) FROM ioc_instance f GROUP BY f.ioc_type_id, f.configuration_name);

ALTER TABLE operation ALTER COLUMN "version" SET NOT NULL;

UPDATE operation SET job_type = 'GENERATE_INSTANCE';

ALTER TABLE ioc_instance ALTER COLUMN ioc_type_id SET NOT NULL;
ALTER TABLE ioc_instance ALTER COLUMN instance_name SET NOT NULL;
ALTER TABLE ioc_instance ALTER COLUMN ioc_type_name SET NOT NULL;

ALTER TABLE ioc_instance_log ALTER COLUMN operation_id SET NOT NULL;
ALTER TABLE ioc_instance_log ALTER COLUMN "version" SET NOT NULL;
ALTER TABLE ioc_instance_log ALTER COLUMN ioc_type_name SET NOT NULL;
