/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

INSERT INTO log
    (ioc_instance_id, description, created_at, type, status)
SELECT ioc_instance_id, 'Fetched Project', created_at, 'CREATE_IOC', 'GIT_PROJECT_FETCHED'
FROM log
WHERE (status = 'TEMPLATE_PROCESSING_AND_COMMIT'
    AND NOT EXISTS(SELECT ioc_instance_id
                   FROM log as in_log
                   WHERE status = 'GIT_PROJECT_CREATED'
                     AND in_log.ioc_instance_id = log.ioc_instance_id))
   OR (status = 'STARTED'
    AND NOT EXISTS(SELECT ioc_instance_id
                   FROM log as in_log
                   WHERE status = 'GIT_PROJECT_CREATED'
                     AND in_log.ioc_instance_id = log.ioc_instance_id)
    AND NOT EXISTS(SELECT ioc_instance_id
                   FROM log as in_log
                   WHERE status = 'TEMPLATE_PROCESSING_AND_COMMIT'
                     AND in_log.ioc_instance_id = log.ioc_instance_id));

INSERT INTO log
    (ioc_instance_id, description, created_at, type, status)
SELECT ioc_instance_id, 'Fetching Project', created_at, 'CREATE_IOC', 'GIT_PROJECT_FETCHING'
FROM log
WHERE status = 'STARTED'
  AND EXISTS(SELECT ioc_instance_id
             FROM log as in_log
             WHERE status = 'GIT_PROJECT_FETCHED'
               AND in_log.ioc_instance_id = log.ioc_instance_id);

INSERT INTO log
    (ioc_instance_id, description, created_at, type, status)
SELECT ioc_instance_id, 'Creating Project', created_at, 'CREATE_IOC', 'GIT_PROJECT_CREATING'
FROM log
WHERE status = 'STARTED'
  AND EXISTS(SELECT ioc_instance_id
             FROM log as in_log
             WHERE status = 'GIT_PROJECT_CREATED'
               AND in_log.ioc_instance_id = log.ioc_instance_id);

UPDATE log
set status = 'GIT_TAGGING_AND_COMMITING'
WHERE status = 'TEMPLATE_PROCESSING_AND_COMMIT';

UPDATE log
SET status = 'GIT_TAGGGED_AND_COMMITED'
WHERE status = 'TAG_CREATED';
