/*
 * Copyright (C) 2025 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

DELETE FROM ioc_instance_log iil WHERE iil.status = 'SUCCESSFUL' AND exists
    (SELECT * FROM ioc_instance_log iil2 WHERE iil2.ioc_instance_id = iil.ioc_instance_id
                                           AND iil2.operation_id = iil.operation_id
                                           AND iil2.status='FAILED')
