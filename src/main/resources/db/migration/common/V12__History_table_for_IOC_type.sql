/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

CREATE TABLE IF NOT EXISTS ioc_type_history
(
    id bigserial NOT NULL,
    type_id bigint NOT NULL,
    type_name text NOT NULL,
    created_by text NOT NULL,
    template_git_project_id bigint NOT NULL,
    config_git_project_id bigint,
    updated_by text NOT NULL,
    updated_at ${timetamp_with_timezone_type} NOT NULL,
    event text NOT NULL,
    CONSTRAINT ioc_type_history_pk PRIMARY KEY (id),
    CONSTRAINT type_fk FOREIGN KEY (type_id) REFERENCES ioc_type (id)
);

INSERT INTO ioc_type_history
(type_id,
 type_name,
 created_by,
 template_git_project_id,
 config_git_project_id,
 updated_by,
 updated_at,
 event)
SELECT id, name, created_by, template_git_project_id, config_git_project_id, created_by, created_at, 'CREATE'
FROM ioc_type;
