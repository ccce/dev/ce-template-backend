# syntax=docker/dockerfile:1

### Build stage
FROM maven:3.8.5-openjdk-17 AS builder

COPY docker/settings.xml /usr/share/maven/conf/settings.xml

RUN microdnf install Xvfb

# Make sure that anyone can write to .m2 because
# the image might not always be used with the maven user
RUN useradd -m -s /bin/bash -u 1000 maven \
  && mkdir /home/maven/.m2 \
  && chmod 0777 /home/maven/.m2 \
  && chown maven:maven /home/maven/.m2

ENV MAVEN_CONFIG "/home/maven/.m2"

USER maven

WORKDIR /build
COPY pom.xml .

# Leverage Docker build cache for dependencies
RUN --mount=type=cache,target=/root/.m2 \
    mvn dependency:resolve dependency:resolve-plugins

COPY src src/
COPY styles styles/

RUN --mount=type=cache,target=/root/.m2 \
    mvn clean package -Dmaven.checkstyle.skip=true -Dfmt.skip=true

### Runtime stage
FROM eclipse-temurin:17-jre

# Add ESS CA certificate to system-wide trust store
ADD https://artifactory.esss.lu.se/artifactory/certificates/CA/ess-primary-ca-root.crt /usr/local/share/ca-certificates/ess-primary-ca-root.crt
RUN update-ca-certificates

# Add ESS CA certificate to Java trust store
RUN keytool -importcert -trustcacerts -keystore /opt/java/openjdk/lib/security/cacerts \
    -storepass changeit -noprompt -alias ess_root_ca -file /usr/local/share/ca-certificates/ess-primary-ca-root.crt

WORKDIR /app
COPY --from=builder /build/target/*.jar app.jar

EXPOSE 8080

CMD ["java", "-jar", "/app/app.jar"]
