# CE template

This project hosts the back-end for the template functionality in the Controls Ecosystem (CE).

## Quickstart (Development)

First, create and configure your environment file:
```bash
cp .env.example .env
```
You will need to add your development tokens to the `.env` file.

Then choose your preferred way to run the application:

Option 1: Docker Compose (full stack)
```bash
docker compose up -d
```

Option 2: Spring Boot with hot-reload
```bash
docker compose up -d db
mvn spring-boot:run
```

The application will be available at `http://localhost:8080`.

## Requirements

- Docker and Docker Compose
- PostgreSQL 15 (for non-containerized use)

## Production Deployment

1. Build the application:
   ```bash
   mvn package
   ```

2. Build the container:
   ```bash
   docker build -t ce-deploy-backend .
   ```

3. Required configuration:
   - PostgreSQL database access
   - GitLab OAuth2 credentials
   - NetBox integration settings
   - Graylog logging configuration
   - AWX automation setup
   - OIDC configuration

Configuration should be managed through the deployment infrastructure (e.g., Ansible, Kubernetes secrets). The `.env` file is for development only.

## Development

See [DEVELOPMENT.md](DEVELOPMENT.md) for further information.
